package com.swcguild.fibonacci.dao;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class FibonacciDAOImplTest {

    public FibonacciDAOImplTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void Get4TermsTest() {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        FibonacciDAO dao = (FibonacciDAOImpl) ctx.getBean("daoBean");
        int[] testArray = {1, 1, 2, 3};

        Assert.assertArrayEquals(testArray, dao.getNTerms(4));

    }

    @Test
    public void Get2TermsTest() {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        FibonacciDAO dao = (FibonacciDAOImpl) ctx.getBean("daoBean");
        int[] testArray = {1, 1};

        Assert.assertArrayEquals(testArray, dao.getNTerms(2));

    }

    @Test
    public void Get1TermTest() {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        FibonacciDAO dao = (FibonacciDAOImpl) ctx.getBean("daoBean");
        int[] testArray = {1};

        Assert.assertArrayEquals(testArray, dao.getNTerms(1));

    }

    @Test
    public void Get0TermsTest() {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        FibonacciDAO dao = (FibonacciDAOImpl) ctx.getBean("daoBean");
        int[] testArray = {};

        Assert.assertArrayEquals(testArray, dao.getNTerms(0));

    }

}
