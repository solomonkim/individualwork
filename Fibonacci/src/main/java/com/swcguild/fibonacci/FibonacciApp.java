/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.fibonacci;

import com.swcguild.fibonacci.ops.FibonacciControllerImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class FibonacciApp {

    public static void main(String[] args) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        FibonacciControllerImpl controller = ctx.getBean("controllerBean", FibonacciControllerImpl.class);
        controller.run();

    }
}
