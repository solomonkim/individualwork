/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.fibonacci.dao;

/**
 *
 * @author apprentice
 */
public class FibonacciDAOImpl implements FibonacciDAO {

    @Override
    public int[] getNTerms(int numberOfTerms) {
        int[] firstNTerms = new int[numberOfTerms];
        int termNMinusTwo = 0;
        int termNMinusOne = 1;
        int termN;
        int n = numberOfTerms;

        for (int i = 0; i < n; i++) {
            if (i < 2) {
                termN = 1;
            } else {
                termN = termNMinusTwo + termNMinusOne;
            }
            firstNTerms[i] = termN;
            termNMinusTwo = termNMinusOne;
            termNMinusOne = termN;
        }
        return firstNTerms;
    }
}
