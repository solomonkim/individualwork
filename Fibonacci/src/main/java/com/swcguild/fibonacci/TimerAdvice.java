/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.fibonacci;

import com.swcguild.fibonacci.ui.ConsoleIO;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class TimerAdvice {

    public Object timeMethod(ProceedingJoinPoint jp) {

        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        ConsoleIO console = (ConsoleIO) ctx.getBean("consoleBean");
        Object ret = null;

        try {
            long start = System.currentTimeMillis();
            ret = jp.proceed();
            long end = System.currentTimeMillis();
            String timedMethod = jp.getSignature().getName();
            console.print(timedMethod + ": " + (end - start) + " ms\n");

        } catch (Throwable ex) {
            console.print("An exception occurred.");
        }
        return ret;
    }
}
