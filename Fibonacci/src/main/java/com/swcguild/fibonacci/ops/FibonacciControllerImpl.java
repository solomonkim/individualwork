/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.fibonacci.ops;

import com.swcguild.fibonacci.dao.FibonacciDAO;
import com.swcguild.fibonacci.ui.ConsoleIO;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class FibonacciControllerImpl implements FibonacciController {

//    ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext();
    private FibonacciDAO dao;
    private ConsoleIO console;

    public FibonacciControllerImpl(FibonacciDAO dao, ConsoleIO console) {
        this.dao = dao;
        this.console = console;
    }

    @Override
    public void run() {

        int n;
        int[] firstNTerms;

        do {
            n = console.readInt("How many terms of the Fibonacci sequence do you want to print?");
            if (n < 0) {
                console.print("Choose a non-negative number of terms to display.\n");
            }
        } while (n < 0);
        if (n == 1) {
            console.print("Here is the first term of the Fibonnaci sequence:\n");
        } else {
            console.print("Here are the first " + n + " terms of the Fibonacci sequence:\n");
        }
        firstNTerms = dao.getNTerms(n);

        for (int i = 0; i < n; i++) {
            if (i < n - 1) {
                console.print(firstNTerms[i] + ", ");
            } else {
                console.print(Integer.toString(firstNTerms[i]));
            }
        }
    }

    public FibonacciDAO getFibonacciDAO() {
        return dao;
    }

}
