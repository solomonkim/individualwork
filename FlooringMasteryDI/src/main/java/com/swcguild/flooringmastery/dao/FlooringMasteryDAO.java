/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmastery.dao;

import com.swcguild.flooringmastery.dto.Order;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public interface FlooringMasteryDAO {

    void addOrder(String date);

    void createNewOrder(String mmddyyyy, String customerName, String stateAbbreviation, String productType, Double area);

    void deleteOrder(int orderNumber);

    void editOrder(String orderNumber, String date);

    ArrayList<Order> fetchOrdersForDate(String date);

    Order fetchSingleOrder(int orderNumber);

    boolean loadConfig();

    void loadFiles() throws FileNotFoundException;

    void loadFlooringData() throws FileNotFoundException;

    void loadStateTax() throws FileNotFoundException;

    int orderNumber();

    Order removeOrder(String orderNumber, String date);

    void writeFiles() throws IOException;
    
}
