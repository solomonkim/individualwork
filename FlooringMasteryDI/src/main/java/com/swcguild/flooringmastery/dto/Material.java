/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmastery.dto;

/**
 *
 * @author apprentice
 */
public class Material {

    String productType;
    double matRate;
    double labRate;

    public Material(String productType, double matRate, double labRate) {
        this.productType = productType;
        this.matRate = matRate;
        this.labRate = labRate;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String materialType) {
        this.productType = materialType;
    }

    public double getMatRate() {
        return matRate;
    }

    public void setMatRate(double matRate) {
        this.matRate = matRate;
    }

    public double getLabRate() {
        return labRate;
    }

    public void setLabRate(double labRate) {
        this.labRate = labRate;
    }

}
