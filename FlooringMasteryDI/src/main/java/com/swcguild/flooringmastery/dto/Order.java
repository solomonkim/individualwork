/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.flooringmastery.dto;

/**
 *
 * @author apprentice
 */
public class Order {

    int orderNumber;
    String customerName;
    String state;
    Double taxRate;
    String productType;
    Double area;
    Double costPerSquareFoot;
    Double laborCostPerSquareFoot;
    Double materialCost;
    Double laborCost;
    Double tax;
    Double total;
    String mmddyyyy;

    public Order() {
    }

    public Order(int orderNumber, String customerName, String state, Double taxRate, String productType, Double area, Double costPerSquareFoot, Double laborCostPerSquareFoot, Double materialCost, Double laborCost, Double tax, Double total, String mmddyyyy) {
        this.orderNumber = orderNumber;
        this.customerName = customerName;
        this.state = state;
        this.taxRate = taxRate;
        this.productType = productType;
        this.area = area;
        this.costPerSquareFoot = costPerSquareFoot;
        this.laborCostPerSquareFoot = laborCostPerSquareFoot;
        this.materialCost = materialCost;
        this.laborCost = laborCost;
        this.tax = tax;
        this.total = total;
        this.mmddyyyy = mmddyyyy;
    }

    public int getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(int orderNumber) {
        this.orderNumber = orderNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Double getTaxRate() {
        return taxRate;
    }

    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Double getArea() {
        return area;
    }

    public void setArea(Double area) {
        this.area = area;
    }

    public Double getMatCostPerSquareFoot() {
        return costPerSquareFoot;
    }

    public void setMatCostPerSquareFoot(Double costPerSquareFoot) {
        this.costPerSquareFoot = costPerSquareFoot;
    }

    public Double getLabCostPerSquareFoot() {
        return laborCostPerSquareFoot;
    }

    public void setLabCostPerSquareFoot(Double laborCostPerSquareFoot) {
        this.laborCostPerSquareFoot = laborCostPerSquareFoot;
    }

    public Double getMatCost() {
        return materialCost;
    }

    public void setMatCost(Double materialCost) {
        this.materialCost = materialCost;
    }

    public Double getLabCost() {
        return laborCost;
    }

    public void setLabCost(Double laborCost) {
        this.laborCost = laborCost;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public String getMmddyyyy() {
        return mmddyyyy;
    }

    public void setMmddyyyy(String mmddyyyy) {
        this.mmddyyyy = mmddyyyy;
    }
}
