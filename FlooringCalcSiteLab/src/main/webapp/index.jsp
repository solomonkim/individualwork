<%-- 
    Document   : index
    Created on : Mar 28, 2016, 11:11:04 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flooring Calculator</title>
    </head>
    <body>
        <h1>How much will your flooring cost?</h1>
        <form method="POST" action="FlooringCalcServlet">
            width (feet): <input type ="number" min="100" max="1999" step="50" name="width"><br />
            length (feet): <input type ="number" min="100" max="1999" step="50" name="length"><br />
            cost/square foot: <input type ="number" min="0" max="199" step="1" name="costSqFt"><br />
            <button type="submit" value="Submit">Submit</button>
        </form>
    </body>
</html>
