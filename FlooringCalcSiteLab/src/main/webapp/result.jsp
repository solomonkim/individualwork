<%-- 
    Document   : result
    Created on : Mar 28, 2016, 10:19:13 AM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Flooring Calculation Results</title>
    </head>
    <body>
        <h1>Here are the results of your flooring order...</h1>

        <c:if test="${badInput==true}">
            Please go back and provide actual correct input.
            <a href=index.jsp">Return to form</a>
        </c:if>
        <c:out value="${width}" default="n/a" />ft. *
        <c:out value="${length}" default="n/a" />ft. = 
        <c:out value="${flooringSqFt}" default="n/a" />sq. ft.<br />

        <c:out value="${flooringSqFt}" default="n/a" />sq. ft *
        <c:out value="${costSqFt}" default="n/a" />$/sq. ft = 
        <c:out value="${flooringCost}" default="n/a" />$<br />

        <c:out value="${flooringSqFt}" default="n/a" /> / 5 sq. ft./hr. = 
        <c:out value="${laborTime}" default="n/a" /> 15m increments<br />

        <c:out value="${laborTime}" default="n/a" /> 15m increments * $86/hr = 
        <c:out value="${laborCost}" default="n/a" /> 15m increments (rounded to the next 15m increment)<br />

    </body>
</html>
