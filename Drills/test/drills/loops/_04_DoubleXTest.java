package drills.loops;

/*
Given a string, return true if the first instance of "x" in the string is
immediately followed by another "x". 
 */
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _04_DoubleXTest {

    _04_DoubleX testObj = new _04_DoubleX();

    public _04_DoubleXTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void hello() { //DoubleX("axxbb") -> true
        Assert.assertTrue(testObj.DoubleX("axxbb"));
    }

    @Test
    public void hello1() { //DoubleX("axaxxax") -> false
        Assert.assertFalse(testObj.DoubleX("axaxxax"));
    }

    @Test
    public void hello2() { //DoubleX("xxxxx") -> true
        Assert.assertTrue(testObj.DoubleX("xxxxx"));
    }
}
