/*
Given a string, return the count of the number of times that a substring length
2 appears in the string and also as the last 2 chars of the string, so "hixxxhi"
yields 1 (we won't count the end substring). 
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _07_CountLast2Test {

    _07_CountLast2 testObj = new _07_CountLast2();

    public _07_CountLast2Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test() { //CountLast2("hixxhi") -> 1
        Assert.assertEquals(1, testObj.CountLast2("hixxhi"));
    }

    @Test
    public void test1() { //CountLast2("xaxxaxaxx") -> 1
        Assert.assertEquals(1, testObj.CountLast2("xaxxaxaxx"));
    }

    @Test
    public void test2() { //CountLast2("axxxaaxx") -> 2
        Assert.assertEquals(2, testObj.CountLast2("axxxaaxx"));
    }
}
