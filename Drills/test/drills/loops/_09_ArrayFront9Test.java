/*
Given an array of ints, return true if one of the first 4 elements in the array
is a 9. The array length may be less than 4. 
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _09_ArrayFront9Test {

    _09_ArrayFront9 testObj = new _09_ArrayFront9();

    public _09_ArrayFront9Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void hello() { //ArrayFront9({1, 2, 9, 3, 4}) -> true
        int[] testArray = {1, 2, 9, 3, 4};
        Assert.assertTrue(testObj.ArrayFront9(testArray));
    }

    @Test
    public void hel2lo() { //ArrayFront9({1, 2, 3, 4, 9}) -> false
        int[] testArray = {1, 2, 3, 4, 9};
        Assert.assertFalse(testObj.ArrayFront9(testArray));
    }

    @Test
    public void hel1lo() { //ArrayFront9({1, 2, 3, 4, 5}) -> false
        int[] testArray = {1, 2, 3, 4, 5};
        Assert.assertFalse(testObj.ArrayFront9(testArray));
    }
}
