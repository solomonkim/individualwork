package drills.loops;

/*
Given a string and a non-negative int n, return a larger string that is n copies
of the original string.
 */

import drills.loops._01_StringTimes;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _01_StringTimesTest {

    _01_StringTimes testObj = new _01_StringTimes();

    public _01_StringTimesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTwice() { //StringTimes("Hi", 2) -> "HiHi"
        Assert.assertEquals("HiHi", testObj.StringTimes("Hi", 2));
    }

    @Test
    public void testThrice() { //StringTimes("Hi", 3) -> "HiHiHi"
        Assert.assertEquals("HiHiHi", testObj.StringTimes("Hi", 3));
    }

    @Test
    public void testOnce() { //StringTimes("Hi", 1) -> "Hi"
        Assert.assertEquals("Hi", testObj.StringTimes("Hi", 1));
    }
}
