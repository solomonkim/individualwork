/*
Given a string and a non-negative int n, we'll say that the front of the string
is the first 3 chars, or whatever is there if the string is less than length 3.
Return n copies of the front; 
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _02_FrontTimesTest {

    _02_FrontTimes testObj = new _02_FrontTimes();

    public _02_FrontTimesTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLongWordThrice() { //FrontTimes("Chocolate", 2) -> "ChoCho"
        Assert.assertEquals("ChoCho", testObj.FrontTimes("Chocolate", 2));
    }

    @Test
    public void testLongWordZero() { //FrontTimes("Chocolate", 0) -> ""
        Assert.assertEquals("", testObj.FrontTimes("Chocolate", 0));
    }

    @Test
    public void testShortWordTwice() { //FrontTimes("Ab", 2) -> "AbAb"
        Assert.assertEquals("AbAb", testObj.FrontTimes("Ab", 2));
    }
}
