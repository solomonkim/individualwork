/*
Given a non-empty string like "Code" return a string like "CCoCodCode".  (first
char, first two, first 3, etc)
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _06_StringSplosionTest {

    _06_StringSplosion testObj = new _06_StringSplosion();

    public _06_StringSplosionTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCode() { //StringSplosion("Code") -> "CCoCodCode"
        Assert.assertEquals("CCoCodCode", testObj.StringSplosion("Code"));
    }

    @Test
    public void testAbc() { //StringSplosion("abc") -> "aababc"
        Assert.assertEquals("aababc", testObj.StringSplosion("abc"));
    }

    @Test
    public void testAb() { //StringSplosion("ab") -> "aab"
        Assert.assertEquals("aab", testObj.StringSplosion("ab"));
    }
}
