/*
Given a string, return a new string made of every other char starting with the
first, so "Hello" yields "Hlo".
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _05_EveryOtherTest {

    _05_EveryOther testObj = new _05_EveryOther();

    public _05_EveryOtherTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHlo() { //EveryOther("Hello") -> "Hlo"
        Assert.assertEquals("Hlo", testObj.EveryOther("Hello"));
    }

    @Test
    public void testHi() { //EveryOther("Hi") -> "H"
        Assert.assertEquals("H", testObj.EveryOther("Hi"));
    }

    @Test
    public void testHeeololeo() { //EveryOther("Heeololeo") -> "Hello"
        Assert.assertEquals("Hello", testObj.EveryOther("Heeololeo"));
    }
}
