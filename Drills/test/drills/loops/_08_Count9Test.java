/*
Given an array of ints, return the number of 9's in the array. 
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _08_Count9Test {

    _08_Count9 testObj = new _08_Count9();

    public _08_Count9Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test3() { //Count9({1, 2, 9}) -> 1
        int[] testArray = {1, 2, 9};
        Assert.assertEquals(1, testObj.Count9(testArray));
    }

    @Test
    public void test3with2() { //Count9({1, 9, 9}) -> 2
        int[] testArray = {1, 9, 9};
        Assert.assertEquals(2, testObj.Count9(testArray));
    }

    @Test
    public void test5With3() { //Count9({1, 9, 9, 3, 9}) -> 3
        int[] testArray = {1, 9, 9, 3, 9};
        Assert.assertEquals(3, testObj.Count9(testArray));
    }
}
