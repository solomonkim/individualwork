/*
Count the number of "xx" in the given string. We'll say that overlapping is
allowed, so "xxx" contains 2 "xx". 
 */
package drills.loops;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _03_CountXXTest {

    _03_CountXX testObj = new _03_CountXX();

    public _03_CountXXTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testabcxx() { //CountXX("abcxx") -> 1
        Assert.assertEquals(1, testObj.CountXX("abcxx"));
    }

    @Test
    public void testxxx() { //CountXX("xxx") -> 2
        Assert.assertEquals(2, testObj.CountXX("xxx"));
    }

    @Test
    public void testxxxx() { //CountXX("xxxx") -> 3
        Assert.assertEquals(3, testObj.CountXX("xxxx"));
    }
}
