/*
Given a string, take the last char and return a new string with the last char
added at the front and back, so "cat" yields "tcatt". The original string will
be length 1 or more. 
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _13_BackAroundTest {

    _13_BackAround testObj = new _13_BackAround();

    public _13_BackAroundTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSmallWord() { //BackAround("cat") -> "tcatt"
        Assert.assertEquals("tcatt", testObj.BackAround("cat"));
    }

    @Test
    public void testBigWord() { //BackAround("Hello") -> "oHelloo"
        Assert.assertEquals("oHelloo", testObj.BackAround("Hello"));
    }

    @Test
    public void testOneLetter() { //BackAround("a") -> "aaa"
        Assert.assertEquals("aaa", testObj.BackAround("a"));
    }
}
