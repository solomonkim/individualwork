/*
Given two ints, a and b, return true if one if them is 10 or if their sum is 10.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _06_Makes10Test {

    _06_Makes10 testObj = new _06_Makes10();

    public _06_Makes10Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testOneIsTen() { //Makes10(9, 10) -> true
        Assert.assertTrue(testObj.Makes10(9, 10));
    }

    @Test
    public void testNeitherIsTen() { //Makes10(9, 9) -> false
        Assert.assertFalse(testObj.Makes10(9, 9));
    }

    @Test
    public void testSumIsTen() { //Makes10(1, 9) -> true
        Assert.assertTrue(testObj.Makes10(1, 9));
    }
}
