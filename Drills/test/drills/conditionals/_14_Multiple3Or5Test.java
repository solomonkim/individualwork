/*
Return true if the given non-negative number is a multiple of 3 or a multiple of
5. Use the % "mod" operator
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _14_Multiple3Or5Test {

    _14_Multiple3Or5 testObj = new _14_Multiple3Or5();

    public _14_Multiple3Or5Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test3() { //Multiple3or5(3) -> true
        Assert.assertTrue(testObj.Multiple3Or5(3));
    }

    @Test
    public void test10() { //Multiple3or5(10) -> true
        Assert.assertTrue(testObj.Multiple3Or5(10));
    }

    @Test
    public void test8() { //Multiple3or5(8) -> false
        Assert.assertFalse(testObj.Multiple3Or5(8));
    }
}
