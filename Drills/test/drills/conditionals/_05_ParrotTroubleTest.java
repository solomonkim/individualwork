/*
We have a loud talking parrot. The "hour" parameter is the current hour time in
the range 0..23. We are in trouble if the parrot is talking and the hour is
before 7 or after 20. Return true if we are in trouble. 
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _05_ParrotTroubleTest {

    _05_ParrotTrouble testObj = new _05_ParrotTrouble();

    public _05_ParrotTroubleTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //ParrotTrouble(true, 6) -> true
        Assert.assertTrue(testObj.ParrotTrouble(true, 6));
    }

    @Test
    public void test2() { //ParrotTrouble(true, 7) -> false
        Assert.assertFalse(testObj.ParrotTrouble(true, 7));
    }

    @Test
    public void test3() { //ParrotTrouble(false, 6) -> false
        Assert.assertFalse(testObj.ParrotTrouble(false, 6));
    }
}
