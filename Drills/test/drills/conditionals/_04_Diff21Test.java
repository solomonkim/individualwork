/*
Given an int n, return the absolute value of the difference between n and 21,
except return double the absolute value of the difference if n is over 21. 
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _04_Diff21Test {

    _04_Diff21 testObj = new _04_Diff21();

    public _04_Diff21Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //Diff21(23) -> 4
        Assert.assertEquals(4, testObj.Diff21(23));
    }

    @Test
    public void test2() { //Diff21(10) -> 11
        Assert.assertEquals(11, testObj.Diff21(10));
    }

    @Test
    public void test3() { //Diff21(21) -> 0
        Assert.assertEquals(0, testObj.Diff21(21));
    }
}
