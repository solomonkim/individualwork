package drills.conditionals;

/*
Given a string, return a new string where "not " has been added to the front.
However, if the string already begins with "not", return the string unchanged.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _09_NotStringTest {

    _09_NotString testObj = new _09_NotString();

    public _09_NotStringTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCandy() { //NotString("candy") -> "not candy"
        Assert.assertEquals("not candy", testObj.NotString("candy"));
    }

    @Test
    public void testX() { //NotString("x") -> "not x"
        Assert.assertEquals("not x", testObj.NotString("x"));
    }

    @Test
    public void testNotBad() { //NotString("not bad") -> "not bad"
        Assert.assertEquals("not bad", testObj.NotString("not bad"));
    }
}
