/*
Given a string, return a new string where the first and last chars have been
exchanged. 
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _11_FrontBackTest {

    _11_FrontBack testObj = new _11_FrontBack();

    public _11_FrontBackTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFourChars() { //FrontBack("code") -> "eodc"
        Assert.assertEquals("eodc", testObj.FrontBack("code"));
    }

    @Test
    public void testOneChar() { //FrontBack("a") -> "a"
        Assert.assertEquals("a", testObj.FrontBack("a"));
    }

    @Test
    public void testTwoChars() { //FrontBack("ab") -> "ba"
        Assert.assertEquals("ba", testObj.FrontBack("ab"));
    }
}
