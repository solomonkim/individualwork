package drills.conditionals;

/*
Given a non-empty string and an int n, return a new string where the char at
index n has been removed. The value of n will be a valid index of a char in the
original string (Don't check for bad index). 
 */
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _10_MissingCharTest {

    _10_MissingChar testObj = new _10_MissingChar();

    public _10_MissingCharTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testPositionOne() { //MissingChar("kitten", 1) -> "ktten"
        Assert.assertEquals("ktten", testObj.MissingChar("kitten", 1));
    }

    @Test
    public void testPositionZero() { //MissingChar("kitten", 0) -> "itten"
        Assert.assertEquals("itten", testObj.MissingChar("kitten", 0));
    }

    @Test
    public void testPositionFour() { //MissingChar("kitten", 4) -> "kittn"
        Assert.assertEquals("kittn", testObj.MissingChar("kitten", 4));
    }
}
