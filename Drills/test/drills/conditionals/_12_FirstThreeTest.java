/*
Given a string, we'll say that the front is the first 3 chars of the string. If
the string length is less than 3, the front is whatever is there. Return a new
string which is 3 copies of the front. 
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _12_FirstThreeTest {

    _12_FirstThree testObj = new _12_FirstThree();

    public _12_FirstThreeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testThreeLetterWord() { //Front3("Mic") -> "MicMicMic"
        Assert.assertEquals("MicMicMic", testObj.Front3("Mic"));
    }

    @Test
    public void testLongWord() { //Front3("Chocolate") -> "ChoChoCho"
        Assert.assertEquals("ChoChoCho", testObj.Front3("Chocolate"));
    }

    @Test
    public void testTwoLetterWord() { //Front3("at") -> "atatat"
        Assert.assertEquals("atatat", testObj.Front3("at"));
    }
}
