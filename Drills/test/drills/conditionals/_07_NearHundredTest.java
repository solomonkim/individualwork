/*
Given an int n, return true if it is within 10 of 100 or 200.
Hint: Check out the C# Math class for absolute value
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _07_NearHundredTest {

    _07_NearHundred testObj = new _07_NearHundred();

    public _07_NearHundredTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testAbsValLessThanTen() { //NearHundred(103) -> true
        Assert.assertTrue(testObj.NearHundred(103));
    }

    @Test
    public void testAbsValIsTen() { //NearHundred(90) -> true
        Assert.assertTrue(testObj.NearHundred(90));
    }

    @Test
    public void testAbsValGreaterThanTen() { //NearHundred(89) -> false
        Assert.assertFalse(testObj.NearHundred(89));
    }
}
