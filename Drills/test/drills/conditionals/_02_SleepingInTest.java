/*
The parameter weekday is true if it is a weekday, and the parameter vacation is
true if we are on vacation. We sleep in if it is not a weekday or we're on
vacation. Return true if we sleep in.
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _02_SleepingInTest {

    _02_SleepingIn testObj = new _02_SleepingIn();

    public _02_SleepingInTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //CanSleepIn(false, false) -> true
        Assert.assertTrue(testObj.CanSleepIn(false, false));
    }

    @Test
    public void test2() { //CanSleepIn(true, false) -> false
        Assert.assertFalse(testObj.CanSleepIn(true, false));
    }

    @Test
    public void test3() { //CanSleepIn(false, true) -> true
        Assert.assertTrue(testObj.CanSleepIn(false, true));
    }
}
