/*
We have two children, a and b, and the parameters aSmile and bSmile indicate if
each is smiling. We are in trouble if they are both smiling or if neither of
them is smiling. Return true if we are in trouble. 
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _01_MischeviousChildrenTest {
    
    _01_MischeviousChildren testObj = new _01_MischeviousChildren();

    public _01_MischeviousChildrenTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //AreWeInTrouble(true, true) -> true
        Assert.assertTrue(testObj.AreWeInTrouble(true, true));
    }

    @Test
    public void test2() { //AreWeInTrouble(false, false) -> true
        Assert.assertTrue(testObj.AreWeInTrouble(false, false));
    }

    @Test
    public void test3() { //AreWeInTrouble(true, false) -> false
        Assert.assertFalse(testObj.AreWeInTrouble(true, false));
    }
}
