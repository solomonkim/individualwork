/*
Given two int values, return their sum. However, if the two values are the same,
then return double their sum. 
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Solomon Kim
 */
public class _03_SumDoubleTest {

    _03_SumDouble testObj = new _03_SumDouble();

    public _03_SumDoubleTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //SumDouble(1, 2) -> 3
        Assert.assertEquals(3, testObj.SumDouble(1, 2));
    }

    @Test
    public void test2() { //SumDouble(3, 2) -> 5
        Assert.assertEquals(5, testObj.SumDouble(3, 2));
    }

    @Test
    public void test3() { //SumDouble(2, 2) -> 8
        Assert.assertEquals(8, testObj.SumDouble(2, 2));
    }
}
