/*
Given two int values, return true if one is negative and one is positive. Except
if the parameter "negative" is true, then return true only if both are negative. 
 */
package drills.conditionals;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _08_PosNegTest {

    _08_PosNeg testObj = new _08_PosNeg();

    public _08_PosNegTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void oneAndOneFalse() { //PosNeg(1, -1, false) -> true
        Assert.assertTrue(testObj.PosNeg(1, -1, false));
    }

    @Test
    public void oneAndOneTrue() { //PosNeg(-1, 1, true) -> false
        Assert.assertFalse(testObj.PosNeg(-1, 1, true));
    }

    @Test
    public void bothNegativeTrue() { //PosNeg(-4, -5, true) -> true
        Assert.assertTrue(testObj.PosNeg(-4, -5, true));
    }
}
