/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class _08_HigherWinsTest {
    
    _08_HigherWins testObj = new _08_HigherWins();
    
    public _08_HigherWinsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //_08_HigherWins({1, 2, 3}) -> {3, 3, 3}
        int[] testArrayArgument = {1, 2, 3};
        int[] testArrayExpectedReturn = {3, 3, 3};
        
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._08_HigherWins(testArrayArgument));
    }
    
    @Test
    public void test2() { //_08_HigherWins({11, 5, 9}) -> {11, 11, 11}
        int[] testArrayArgument = {11, 5, 9};
        int[] testArrayExpectedReturn = {11, 11, 11};
        
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._08_HigherWins(testArrayArgument));
    }
    
    @Test
    public void test3() { //_08_HigherWins({2, 11, 3}) -> {3, 3, 3}
        int[] testArrayArgument = {2, 11, 3};
        int[] testArrayExpectedReturn = {3, 3, 3};
        
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._08_HigherWins(testArrayArgument));
    }
    
    @Test
    public void test4() { //_08_HigherWins({11, 4, 5, 11}) -> {11, 4, 5, 11}
        int[] testArrayArgument = {11, 4, 5, 11};
        int[] testArrayExpectedReturn = {11, 4, 5, 11};
        
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._08_HigherWins(testArrayArgument));
    }
}
