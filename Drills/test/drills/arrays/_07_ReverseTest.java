/*
Given an array of ints length 3, return a new array with the elements in reverse
order, so for example {1, 2, 3} becomes {3, 2, 1}.
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _07_ReverseTest {

    _07_Reverse testObj = new _07_Reverse();

    public _07_ReverseTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //_07_Reverse({1, 2, 3}) -> {3, 2, 1}
        int[] testArrayArgument = {1, 2, 3};
        int[] testArrayExpectedReturn = {3, 2, 1};
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._07_Reverse(testArrayArgument));
    }

    @Test
    public void test2() { //_07_Reverse({5, 11, 9}) -> {9, 11, 5}
        int[] testArrayArgument = {1, 2, 3};
        int[] testArrayExpectedReturn = {3, 2, 1};
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._07_Reverse(testArrayArgument));
    }

    @Test
    public void test3() { //_07_Reverse({7, 0, 0}) -> {0, 0, 7}
        int[] testArrayArgument = {1, 2, 3};
        int[] testArrayExpectedReturn = {3, 2, 1};
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._07_Reverse(testArrayArgument));
    }
}
