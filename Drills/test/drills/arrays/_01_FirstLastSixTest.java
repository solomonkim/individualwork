package drills.arrays;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class _01_FirstLastSixTest {

    _01_FirstLastSix testObj = new _01_FirstLastSix();

    public _01_FirstLastSixTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testWithThreeItemsInArray() {
        int[] testArray = {1, 2, 6};
        Assert.assertTrue(testObj.firstLastSix(testArray));
    }

    @Test
    public void testWithFourItemsInArray() {
        int[] testArray = {6, 1, 2, 3};
        Assert.assertTrue(testObj.firstLastSix(testArray));
    }

    @Test
    public void testWithFiveItemsInArray() {
        int[] testArray = {13, 6, 1, 2, 3};
        Assert.assertFalse(testObj.firstLastSix(testArray));
    }
    
    public void testWithNullArray() {
        int[] testArray;
        Assert.assertFalse(testObj.firstLastSix(null));
    }
}
