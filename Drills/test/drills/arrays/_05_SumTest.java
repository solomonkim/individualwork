/*
Given an array of ints, return the sum of all the elements. 
 */
package drills.arrays;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _05_SumTest {

    _05_Sum testObj = new _05_Sum();

    public _05_SumTest() {

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //_05_Sum({1, 2, 3}) -> 6
        int[] testArray = {1, 2, 3};
        Assert.assertEquals(6, testObj._05_Sum(testArray));

    }

    @Test
    public void test2() { //_05_Sum({5, 11, 2}) -> 18
        int[] testArray = {5, 11, 2};
        Assert.assertEquals(18, testObj._05_Sum(testArray));

    }

    @Test
    public void test3() { //_05_Sum({7, 0, 0}) -> 7
        int[] testArray = {7, 0, 0};
        Assert.assertEquals(7, testObj._05_Sum(testArray));

    }
}
