/*
Return an int array length n containing the first n digits of pi.
 */
package drills.arrays;

import java.util.Arrays;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _03_MakePiTest {

    _03_MakePi testObj = new _03_MakePi();

    public _03_MakePiTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { // _03_MakePi(3) -> {3, 1, 4]
        int[] testArray = {3, 1, 4};
        assertTrue(Arrays.equals(testObj._03_MakePi(3), testArray));
    }

    @Test
    public void test2() { // _03_MakePi(6) -> {3, 1, 4, 1, 5, 0}
        int[] testArray = {3, 1, 4, 1, 5, 0};
        assertFalse(Arrays.equals(testObj._03_MakePi(6), testArray));
    }

    @Test
    public void test3() { // _03_MakePi(12) -> {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 9}
        int[] testArray = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 9};
        assertTrue(Arrays.equals(testObj._03_MakePi(12), testArray));
    }
}
