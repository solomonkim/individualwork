/*
Given an array of ints, return true if the array is length 1 or more, and the
first element and the last element are equal.
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _02_SameFirstLastTest {

    _02_SameFirstLast testObj = new _02_SameFirstLast();

    public _02_SameFirstLastTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //_02_SameFirstLast({1, 2, 3}) -> false

        int[] testArray = {1, 2, 3};
        Assert.assertFalse(testObj._02_SameFirstLast(testArray));
    }

    @Test
    public void test2() { //_02_SameFirstLast({1, 2, 3, 1}) -> true

        int[] testArray = {1, 2, 3, 1};
        Assert.assertTrue(testObj._02_SameFirstLast(testArray));
    }

    @Test
    public void test3() { //SameFirstLast({1, 2, 1}) -> true

        int[] testArray = {1, 2, 1};
        assertTrue(testObj._02_SameFirstLast(testArray));
    }
}
