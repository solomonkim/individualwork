/*
Given an array of ints, return an array with the elements "rotated left" so
{1, 2, 3} yields {2, 3, 1}. 
 */
package drills.arrays;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _06_RotateLeftTest {

    _06_RotateLeft testObj = new _06_RotateLeft();

    public _06_RotateLeftTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //_06_RotateLeft({1, 2, 3}) -> {2, 3, 1}
        int[] testArrayArgument = {1, 2, 3};
        int[] testArrayExpectedReturn = {2, 3, 1};
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._06_RotateLeft(testArrayArgument));
    }

    @Test
    public void test2() { //_06_RotateLeft({5, 11, 9}) -> {11, 9, 5}
        int[] testArrayArgument = {5, 11, 9};
        int[] testArrayExpectedReturn = {11, 9, 5};
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._06_RotateLeft(testArrayArgument));
    }

    @Test
    public void test3() { //_06_RotateLeft({7, 0, 0}) -> {0, 0, 7}
        int[] testArrayArgument = {7, 0, 0};
        int[] testArrayExpectedReturn = {0, 0, 7};
        Assert.assertArrayEquals(testArrayExpectedReturn, testObj._06_RotateLeft(testArrayArgument));
    }
}
