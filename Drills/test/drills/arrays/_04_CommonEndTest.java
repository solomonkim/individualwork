/*
Given 2 arrays of ints, a and b, return true if they have the same first element
or they have the same last element. Both arrays will be length 1 or more.
 */
package drills.arrays;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Solomon Kim
 */
public class _04_CommonEndTest {

    _04_CommonEnd testObj = new _04_CommonEnd();

    public _04_CommonEndTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test1() { //_04_CommonEnd({1, 2, 3}, {7, 3}) -> true
        int[] testArrayA = {1, 2, 3};
        int[] testArrayB = {7, 3};

        Assert.assertTrue(testObj._04_commonEnd(testArrayA, testArrayB));
    }

    @Test
    public void test2() { //_04_CommonEnd({1, 2, 3}, {7, 3, 2}) -> false
        int[] testArrayA = {1, 2, 3};
        int[] testArrayB = {7, 3, 2};
        Assert.assertFalse(testObj._04_commonEnd(testArrayA, testArrayB));
    }

    @Test
    public void test3() { //_04_CommonEnd({1, 2, 3}, {1, 3}) -> true
        int[] testArrayA = {1, 2, 3};
        int[] testArrayB = {1, 3};
        Assert.assertTrue(testObj._04_commonEnd(testArrayA, testArrayB));
    }
}