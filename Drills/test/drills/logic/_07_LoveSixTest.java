/*
The number 6 is a truly great number. Given two int values, a and b, return true
if either one is 6. Or if their sum or difference is 6.
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _07_LoveSixTest {

    _07_LoveSix testObj = new _07_LoveSix();

    public _07_LoveSixTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testElementIsSix() { //LoveSix(6, 4) → true
        Assert.assertTrue(testObj.LoveSix(6, 4));
    }

    @Test
    public void testNoneAreSix() { //LoveSix(4, 5) → false
        Assert.assertFalse(testObj.LoveSix(4, 5));
    }

    @Test
    public void testSumIsSix() { //LoveSix(1, 5) → true
        Assert.assertTrue(testObj.LoveSix(1, 5));
    }
}
