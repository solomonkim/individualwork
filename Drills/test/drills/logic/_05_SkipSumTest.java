/*
Given 2 ints, a and b, return their sum. However, sums in the range 10..19
inclusive are forbidden, so in that case just return 20. 
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _05_SkipSumTest {

    _05_SkipSum testObj = new _05_SkipSum();

    public _05_SkipSumTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSmallSum() { //SkipSum(3, 4) → 7
        Assert.assertEquals(7, testObj.SkipSum(3, 4));
    }

    @Test
    public void testForbiddenSum() { //SkipSum(9, 4) → 20
        Assert.assertEquals(20, testObj.SkipSum(9, 4));
    }

    @Test
    public void testLargeSum() { //SkipSum(10, 11) → 21
        Assert.assertEquals(21, testObj.SkipSum(10, 11));
    }
}
