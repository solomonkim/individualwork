/*
You and your date are trying to get a table at a restaurant. The parameter "you"
is the stylishness of your clothes, in the range 0..10, and "date" is the
stylishness of your date's clothes. The result getting the table is encoded as
an int value with 0=no, 1=maybe, 2=yes. If either of you is very stylish, 8 or
more, then the result is 2 (yes). With the exception that if either of you has
style of 2 or less, then the result is 0 (no). Otherwise the result is 1 (maybe). 
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _02_CanHazTableTest {

    _02_CanHazTable testObj = new _02_CanHazTable();

    public _02_CanHazTableTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testStylishDate() { //CanHazTable(5, 10) → 2  
        Assert.assertEquals(2, testObj.CanHazTable(5, 10));
    }

    @Test
    public void testUnstylishDate() { //CanHazTable(5, 2) → 0
        Assert.assertEquals(0, testObj.CanHazTable(5, 2));
    }

    @Test
    public void testTwoMiddle() { //CanHazTable(5, 5) → 1
        Assert.assertEquals(1, testObj.CanHazTable(5, 5));
    }
}
