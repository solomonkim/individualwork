/*
You are driving a little too fast, and a police officer stops you. Write code to
compute the result, encoded as an int value: 0=no ticket, 1=small ticket, 2=big
ticket. If speed is 60 or less, the result is 0. If speed is between 61 and 80
inclusive, the result is 1. If speed is 81 or more, the result is 2. Unless it
is your birthday -- on that day, your speed can be 5 higher in all cases. 
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _04_CaughtSpeedingTest {

    _04_CaughtSpeeding testObj = new _04_CaughtSpeeding();

    public _04_CaughtSpeedingTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSlow() { //CaughtSpeeding(60, false) → 0
        Assert.assertEquals(0, testObj.CaughtSpeeding(60, false));
    }

    @Test
    public void testSmallTicketNotIsBirthday() { //CaughtSpeeding(65, false) → 1
        Assert.assertEquals(1, testObj.CaughtSpeeding(65, false));
    }

    @Test
    public void testSmallTicketIsBirthday() { //CaughtSpeeding(65, true) → 0
        Assert.assertEquals(0, testObj.CaughtSpeeding(60, true));
    }
}
