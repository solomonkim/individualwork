/*
The children in Cleveland spend most of the day playing outside. In particular,
they play if the temperature is between 60 and 90 (inclusive). Unless it is
summer, then the upper limit is 100 instead of 90. Given an int temperature and
a boolean isSummer, return true if the children play and false otherwise. 
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _03_PlayOutsideTest {

    _03_PlayOutside testObj = new _03_PlayOutside();

    public _03_PlayOutsideTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGoodTempNotSummer() { //PlayOutside(70, false) → true
        Assert.assertTrue(testObj.PlayOutside(70, false));
    }

    @Test
    public void testTooHotNotSummer() { //PlayOutside(95, false) → false
        Assert.assertFalse(testObj.PlayOutside(95, false));
    }

    @Test
    public void testWetHotAmericanSummer() { //PlayOutside(95, true) → true
        Assert.assertTrue(testObj.PlayOutside(95, true));
    }
}
