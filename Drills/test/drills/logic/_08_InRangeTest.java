/*
Given a number n, return true if n is in the range 1..10, inclusive. Unless
"outsideMode" is true, in which case return true if the number is less or equal
to 1, or greater or equal to 10. 
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _08_InRangeTest {

    _08_InRange testObj = new _08_InRange();

    public _08_InRangeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testInRangeNotOutside() { //InRange(5, false) → true
        Assert.assertTrue(testObj.InRange(5, false));
    }

    @Test
    public void testOutOfRangeNotOutside() { //InRange(11, false) → false
        Assert.assertFalse(testObj.InRange(11, false));
    }

    @Test
    public void testOutOfRangeOutside() { //InRange(11, true) → true
        Assert.assertTrue(testObj.InRange(11, true));
    }
}
