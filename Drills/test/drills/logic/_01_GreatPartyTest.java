/*
When squirrels get together for a party, they like to have cigars. A squirrel
party is successful when the number of cigars is between 40 and 60, inclusive.
Unless it is the weekend, in which case there is no upper bound on the number of
cigars. Return true if the party with the given values is successful, or false
otherwise. 
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _01_GreatPartyTest {

    _01_GreatParty testObj = new _01_GreatParty();

    public _01_GreatPartyTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testNotEnoughCigars() { //GreatParty(30, false) → false
        Assert.assertFalse(testObj.GreatParty(30, false));
    }

    @Test
    public void testEnoughCigars() { //GreatParty(50, false) → true
        Assert.assertTrue(testObj.GreatParty(50, false));
    }

    @Test
    public void testTooManyCigarsIsWeekend() { //GreatParty(70, true) → true
        Assert.assertTrue(testObj.GreatParty(70, true));
    }
}
