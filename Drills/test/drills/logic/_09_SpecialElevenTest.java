/*
We'll say a number is special if it is a multiple of 11 or if it is one more
than a multiple of 11. Return true if the given non-negative number is special.
Use the % "mod" operator
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _09_SpecialElevenTest {

    _09_SpecialEleven testObj = new _09_SpecialEleven();

    public _09_SpecialElevenTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void test22() { //SpecialEleven(22) → true
        Assert.assertTrue(testObj.SpecialEleven(22));
    }

    @Test
    public void test23() { //SpecialEleven(23) → true
        Assert.assertTrue(testObj.SpecialEleven(23));
    }

    @Test
    public void test24() { //SpecialEleven(24) → false
        Assert.assertFalse(testObj.SpecialEleven(24));
    }
}
