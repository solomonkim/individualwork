/*
Given a day of the week encoded as 0=Sun, 1=Mon, 2=Tue, ...6=Sat, and a
booleanean indicating if we are on vacation, return a string of the form "7:00"
indicating when the alarm clock should ring. Weekdays, the alarm should be
"7:00" and on the weekend it should be "10:00". Unless we are on vacation --
then on weekdays it should be "10:00" and weekends it should be "off". 
 */
package drills.logic;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _06_AlarmClockTest {

    _06_AlarmClock testObj = new _06_AlarmClock();

    public _06_AlarmClockTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testWeekdayNotVacation() { //AlarmClock(1, false) → "7:00"
        Assert.assertEquals("7:00", testObj.AlarmClock(1, false));
    }

    @Test
    public void testWeekdayVacation() { //AlarmClock(5, true) → "10:00"
        Assert.assertEquals("10:00", testObj.AlarmClock(5, true));
    }

    @Test
    public void testWeekendNotVacation() { //AlarmClock(0, false) → "10:00"
        Assert.assertEquals("10:00", testObj.AlarmClock(0, false));
    }
    
    @Test
    public void testWeekendVacation() { //AlarmClock(0, true) → "off"
        Assert.assertEquals("off", testObj.AlarmClock(0, true));
    }
}
