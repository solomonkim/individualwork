/*
Given 2 strings, a and b, return a string of the form short+long+short, with the
shorter string on the outside and the longer string on the inside. The strings
will not be the same length, but they may be empty (length 0). 
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _08_LongInMiddleTest {

    _08_LongInMiddle testObj = new _08_LongInMiddle();

    public _08_LongInMiddleTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testLongInMiddle() {
        Assert.assertEquals(testObj.LongInMiddle("Hello", "hi"), "hiHellohi");//LongInMiddle("Hello", "hi") -> "hiHellohi"
        Assert.assertEquals(testObj.LongInMiddle("hi", "Hello"), "hiHellohi");//LongInMiddle("hi", "Hello") -> "hiHellohi"
        Assert.assertEquals(testObj.LongInMiddle("aaa", "b"), "baaab");//LongInMiddle("aaa", "b") -> "baaab"
    }
}
