/*
Given two strings, a and b, return the result of putting them together in the
order abba, e.g. "Hi" and "Bye" returns "HiByeByeHi". 
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _02_AbbaTest {

    _02_Abba testObj = new _02_Abba();

    public _02_AbbaTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testHiBye() { //Abba("Hi", "Bye") -> "HiByeByeHi"
        Assert.assertEquals("HiByeByeHi", testObj.Abba("Hi", "Bye"));
    }

    @Test
    public void testYoAlice() { //Abba("Yo", "Alice") -> "YoAliceAliceYo"
        Assert.assertEquals("YoAliceAliceYo", testObj.Abba("Yo", "Alice"));
    }

    @Test
    public void testWhatUp() { //Abba("What", "Up") -> "WhatUpUpWhat"
        Assert.assertEquals("WhatUpUpWhat", testObj.Abba("What", "Up"));
    }
}
