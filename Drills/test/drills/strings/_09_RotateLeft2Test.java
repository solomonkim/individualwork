/*
Given a string, return a "rotated left 2" version where the first 2 chars are
moved to the end. The string length will be at least 2. 
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _09_RotateLeft2Test {

    _09_RotateLeft2 testObj = new _09_RotateLeft2();

    public _09_RotateLeft2Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRotateLeft2() {
        Assert.assertEquals(testObj.RotateLeft2("Hello"), "lloHe");//Rotateleft2("Hello") -> "lloHe"
        Assert.assertEquals(testObj.RotateLeft2("java"), "vaja");//Rotateleft2("java") -> "vaja"
        Assert.assertEquals(testObj.RotateLeft2("Hi"), "Hi");//Rotateleft2("Hi") -> "Hi"
    }
}
