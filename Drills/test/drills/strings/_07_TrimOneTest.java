/*
Given a string, return a version without the first and last char, so "Hello"
yields "ell". The string length will be at least 2. 
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _07_TrimOneTest {

    _07_TrimOne testObj = new _07_TrimOne();

    public _07_TrimOneTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTrimOne() {
        Assert.assertEquals(testObj.TrimOne("Hello"), "ell");//TrimOne("Hello") -> "ell"
        Assert.assertEquals(testObj.TrimOne("java"), "av");//TrimOne("java") -> "av"
        Assert.assertEquals(testObj.TrimOne("coding"), "odin");//TrimOne("coding") -> "odin"
    }
}
