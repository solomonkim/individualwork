/*
Given an "out" string length 4, such as "<<>>", and a word, return a new string
where the word is in the middle of the out string, e.g. "<<word>>".
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _04_InsertWordTest {

    _04_InsertWord testObj = new _04_InsertWord();

    public _04_InsertWordTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testInsertWord() {
        Assert.assertEquals(testObj.InsertWord("<<>>", "Yay"), "<<Yay>>");//InsertWord("<<>>", "Yay") -> "<<Yay>>"
        Assert.assertEquals(testObj.InsertWord("<<>>", "WooHoo"), "<<WooHoo>>");//InsertWord("<<>>", "WooHoo") -> "<<WooHoo>>"
        Assert.assertEquals(testObj.InsertWord("[[]]", "word"), "[[word]]");//InsertWord("[[]]", "word") -> "[[word]]"
    }
}
