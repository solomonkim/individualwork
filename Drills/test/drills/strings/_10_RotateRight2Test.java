/*
Given a string, return a "rotated right 2" version where the last 2 chars are
moved to the start. The string length will be at least 2. 
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _10_RotateRight2Test {

    _10_RotateRight2 testObj = new _10_RotateRight2();

    public _10_RotateRight2Test() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testRotateRight2() {
        Assert.assertEquals(testObj.RotateRight2("Hello"), "loHel");//RotateRight2("Hello") -> "loHel"
        Assert.assertEquals(testObj.RotateRight2("java"), "vaja");//RotateRight2("java") -> "vaja"
        Assert.assertEquals(testObj.RotateRight2("Hi"), "Hi");//RotateRight2("Hi") -> "Hi"}
    }
}
