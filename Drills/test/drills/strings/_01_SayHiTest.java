/*
Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!".
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author Solomon Kim
 */
public class _01_SayHiTest {

    _01_SayHi testObj = new _01_SayHi();

    public _01_SayHiTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testBob() {//SayHi("Bob") -> "Hello Bob!"
        Assert.assertEquals(testObj.SayHi("Bob"), "Hello Bob!");
    }

    @Test
    public void testAlice() {//SayHi("Alice") -> "Hello Alice!"
        Assert.assertEquals(testObj.SayHi("Alice"), "Hello Alice!");
    }

    @Test
    public void testX() {//SayHi("X") -> "Hello X!"
        Assert.assertEquals(testObj.SayHi("X"), "Hello X!");
    }
}
