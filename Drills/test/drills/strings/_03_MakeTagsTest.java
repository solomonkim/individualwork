/*
The web is built with HTML strings like "<i>Yay</i>" which draws Yay as italic
text. In this example, the "i" tag makes <i> and </i> which surround the word
"Yay". Given tag and word strings, create the HTML string with tags around the
word, e.g. "<i>Yay</i>". 
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _03_MakeTagsTest {

    _03_MakeTags testObj = new _03_MakeTags();

    public _03_MakeTagsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMakeTags() {
//MakeTags("i", "Yay") -> "<i>Yay</i>"
        Assert.assertEquals(testObj.MakeTags("i", "Yay"), "<i>Yay</i>");
//MakeTags("i", "Hello") -> "<i>Hello</i>"
        Assert.assertEquals(testObj.MakeTags("i", "Hello"), "<i>Hello</i>");
//MakeTags("cite", "Yay") -> "<cite>Yay</cite>"
        Assert.assertEquals(testObj.MakeTags("cite", "Yay"), "<cite>Yay</cite>");
    }
}
