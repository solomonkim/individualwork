/*
Given a string, return a new string made of 3 copies of the last 2 chars of the
original string. The string length will be at least 2. 
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @author Solomon Kim
 */
public class _05_MultipleEndingsTest {

    _05_MultipleEndings testObj = new _05_MultipleEndings();

    public _05_MultipleEndingsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMultipleEndings() {
        Assert.assertEquals(testObj.MultipleEndings("Hello"), "lololo");//MultipleEndings("Hello") -> "lololo"
        Assert.assertEquals(testObj.MultipleEndings("ab"), "ababab");//MultipleEndings("ab") -> "ababab"
        Assert.assertEquals(testObj.MultipleEndings("Hi"), "HiHiHi");//MultipleEndings("Hi") -> "HiHiHi"
    }
}
