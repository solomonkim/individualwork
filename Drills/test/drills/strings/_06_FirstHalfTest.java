/*
Given a string of even length, return the first half. So the string "WooHoo"
yields "Woo". 
 */
package drills.strings;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _06_FirstHalfTest {

    _06_FirstHalf testObj = new _06_FirstHalf();

    public _06_FirstHalfTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testFirstHalf() {
        Assert.assertEquals(testObj.FirstHalf("WooHoo"), "Woo");//FirstHalf("WooHoo") -> "Woo"
        Assert.assertEquals(testObj.FirstHalf("HelloThere"), "Hello");//FirstHalf("HelloThere") -> "Hello"
        Assert.assertEquals(testObj.FirstHalf("abcdef"), "abc");//FirstHalf("abcdef") -> "abc"
    }
}
