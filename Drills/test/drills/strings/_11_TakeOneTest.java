/*
Given a string, return a string length 1 from its front, unless front is false,
in which case return a string length 1 from its back. The string will be non-
empty. 
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _11_TakeOneTest {

    _11_TakeOne testObj = new _11_TakeOne();

    public _11_TakeOneTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testTakeOne() {
        Assert.assertEquals(testObj.TakeOne("Hello", true), "H");//TakeOne("Hello", true) -> "H"
        Assert.assertEquals(testObj.TakeOne("Hello", false), "o");//TakeOne("Hello", false) -> "o"
        Assert.assertEquals(testObj.TakeOne("oh", true), "o");//TakeOne("oh", true) -> "o"
    }
}
