/*
Given a string of even length, return a string made of the middle two chars, so
the string "string" yields "ri". The string length will be at least 2. 
 */
package drills.strings;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * @author Solomon Kim
 */
public class _12_MiddleTwoTest {

    _12_MiddleTwo testObj = new _12_MiddleTwo();

    public _12_MiddleTwoTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testMiddleTwo() {
        Assert.assertEquals(testObj.MiddleTwo("string"), "ri");//MiddleTwo("string") -> "ri"
        Assert.assertEquals(testObj.MiddleTwo("code"), "od");//MiddleTwo("code") -> "od"
        Assert.assertEquals(testObj.MiddleTwo("Practice"), "ct");//MiddleTwo("Practice") -> "ct"
    }
}
