/*
Given an array of ints, return the sum of all the elements. 
 */
package drills.arrays;

/**
 * @author Solomon Kim
 */
public class _05_Sum {

    public int _05_Sum(int[] numbers) {
        int sum = 0;
        for (int i = 0; i < numbers.length; i++) {
            sum += numbers[i];
        }
        return sum;
    }
}
