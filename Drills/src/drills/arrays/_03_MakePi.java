/*
Return an int array length n containing the first n digits of pi.
 */
package drills.arrays;

import java.util.Arrays;

/**
 * @author Solomon Kim
 */
public class _03_MakePi {

    public int[] _03_MakePi(int n) {
        int[] pi = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5, 9};
        int[] _03_makePi = Arrays.copyOf(pi, n);
        return _03_makePi;
    }

}
