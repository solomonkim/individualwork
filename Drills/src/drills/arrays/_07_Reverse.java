/*
Given an array of ints length 3, return a new array with the elements in reverse
order, so for example {1, 2, 3} becomes {3, 2, 1}.
 */
package drills.arrays;

/**
 * @author Solomon Kim
 */
public class _07_Reverse {

    public int[] _07_Reverse(int[] numbers) {

        int[] reverseThisArray = numbers;
        int temp = reverseThisArray[0];
        
        reverseThisArray[0] = reverseThisArray[2];
        reverseThisArray[2] = temp;

        return reverseThisArray;

    }
}
