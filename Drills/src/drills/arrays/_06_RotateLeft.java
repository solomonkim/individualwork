/*
Given an array of ints, return an array with the elements "rotated left" so
{1, 2, 3} yields {2, 3, 1}. 
 */
package drills.arrays;

/**
 * @author Solomon Kim
 */
public class _06_RotateLeft {

    public int[] _06_RotateLeft(int[] numbers) {

        int[] rotateThisArrayLeft = numbers;
        int temp = rotateThisArrayLeft[0];

        rotateThisArrayLeft[0] = rotateThisArrayLeft[1];
        rotateThisArrayLeft[1] = rotateThisArrayLeft[2];
        rotateThisArrayLeft[2] = temp;
        return rotateThisArrayLeft;

    }
}
