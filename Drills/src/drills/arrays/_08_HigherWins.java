/*
Given an array of ints, figure out which is larger between the first and last
elements in the array, and set all the other elements to be that value. Return 
the changed array. 
 */
package drills.arrays;

/**
 * @author Solomon Kim
 */
public class _08_HigherWins {

    public int[] _08_HigherWins(int[] numbers) {

        if (numbers[0] > numbers[numbers.length - 1]) {
            for (int i = 0; i < numbers.length; i++) {
                numbers[i] = numbers[0];
            }
        } else if (numbers[0] < numbers[numbers.length - 1]) {
            for (int i = 0; i < numbers.length; i++) {
                numbers[i] = numbers[numbers.length - 1];
            }
        }
        return numbers;
    }
}
