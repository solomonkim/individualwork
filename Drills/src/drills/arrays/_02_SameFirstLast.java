/*
Given an array of ints, return true if the array is length 1 or more, and the
first element and the last element are equal.
 */
package drills.arrays;

/**
 * @author Solomon Kim
 */
public class _02_SameFirstLast {

    public boolean _02_SameFirstLast(int[] numbers) {
        if (numbers[0] == numbers[numbers.length - 1]) {
            return true;
        } else {
            return false;
        }
    }
}
