package drills.arrays;

public class _01_FirstLastSix {
    public boolean firstLastSix(int[] numbers) {
        if(numbers[0] == 6)
            return true;
        if(numbers[numbers.length - 1] == 6)
            return true;
        return false;
    }
}
