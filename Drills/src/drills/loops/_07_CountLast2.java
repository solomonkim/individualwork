/*
Given a string, return the count of the number of times that a substring length
2 appears in the string and also as the last 2 chars of the string, so "hixxxhi"
yields 1 (we won't count the end substring). 
 */
package drills.loops;

/**
 * @author Solomon Kim
 */
public class _07_CountLast2 {

    public int CountLast2(String str) {
        String searchTerm = str.substring(0, 1) + str.substring(str.length() - 1, str.length());
        int count = 0;

        for (int i = 0; i < str.length() - 2; i++) {
            if ((str.substring(i, i + 2).equals(searchTerm))) {
                count++;
            }
        }
        return count;
    }
}
