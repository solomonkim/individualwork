/*
Given a non-empty string like "Code" return a string like "CCoCodCode".  (first
char, first two, first 3, etc)
 */
package drills.loops;

/**
 * @author Solomon Kim
 */
public class _06_StringSplosion {

    public String StringSplosion(String str) {

        String returnString = "";

        for (int i = 0; i <= str.length(); i++) {
            returnString = returnString + str.substring(0, i);
        }
        return returnString;
    }
}
