/*
Given a string and a non-negative int n, we'll say that the front of the string
is the first 3 chars, or whatever is there if the string is less than length 3.
Return n copies of the front; 
 */
package drills.loops;

/**
 * @author Solomon Kim
 */
public class _02_FrontTimes {

    public String FrontTimes(String str, int n) {

        String returnString = "";
        String stringToRepeat;
        
        if (str.length() < 3) {
            stringToRepeat = str.substring(0, str.length());
        } else {
            stringToRepeat = str.substring(0, 3);
        }

        if (str.length() < 3) {
            for (int i = 0; i < n; i++) {
                returnString = returnString + str;
            }
        } else {
            for (int i = 0; i < n; i++) {
                returnString = returnString + stringToRepeat;
            }
        }

        return returnString;

    }
}
