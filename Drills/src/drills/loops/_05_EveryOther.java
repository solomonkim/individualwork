/*
Given a string, return a new string made of every other char starting with the
first, so "Hello" yields "Hlo".
 */
package drills.loops;

/**
 * @author Solomon Kim
 */
public class _05_EveryOther {

    public String EveryOther(String str) {

        String returnString = "";

        for (int i = 0; i < str.length(); i += 2) {
            returnString = returnString + str.substring(i, i + 1);
        }
        return returnString;
    }
}
