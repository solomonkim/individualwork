/*
Given an array of ints, return the number of 9's in the array. 
 */
package drills.loops;

/**
 * @author Solomon Kim
 */
public class _08_Count9 {

    public int Count9(int[] numbers) {

        int count = 0;

        for (int i = 0; i < numbers.length; i++) {
            if (numbers[i] == 9) {
                count++;
            }
        }

        return count;
    }
}
