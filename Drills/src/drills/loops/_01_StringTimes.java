/*
Given a string and a non-negative int n, return a larger string that is n copies
of the original string.
 */
package drills.loops;

/**
 * @author Solomon Kim
 */
public class _01_StringTimes {

    public String StringTimes(String str, int n) {
        String returnString = "";
        for (int i = 0; i < n; i++) {
            returnString = returnString + str;
        }
        return returnString;
    }
}
