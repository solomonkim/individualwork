/*
Given an array of ints, return true if one of the first 4 elements in the array
is a 9. The array length may be less than 4. 
 */
package drills.loops;

/**
 * @author Solomon Kim
 */
public class _09_ArrayFront9 {

    public boolean ArrayFront9(int[] numbers) {

        for (int i = 0; i < numbers.length; i++) {
            if (i < 4 && numbers[i] == 9) {
                return true;
            }
        }
        return false;
    }
}
