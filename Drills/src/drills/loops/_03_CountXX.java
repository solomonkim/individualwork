/*
Count the number of "xx" in the given string. We'll say that overlapping is
allowed, so "xxx" contains 2 "xx". 
 */
package drills.loops;

/**
 * @author Solomon Kim
 */
public class _03_CountXX {

    public int CountXX(String str) {
        
        int xxCount = 0;
        
        for (int i = 0; i < str.length() -1; i++) {
           if (str.substring(i, i + 1).equals("x") && str.substring(i + 1, i + 2).equals("x")) {
               xxCount++;
           }
        }
        
        return xxCount;
    }
}