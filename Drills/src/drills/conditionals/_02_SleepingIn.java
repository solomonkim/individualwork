/*
The parameter weekday is true if it is a weekday, and the parameter vacation is
true if we are on vacation. We sleep in if it is not a weekday or we're on
vacation. Return true if we sleep in.
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _02_SleepingIn {

    public boolean CanSleepIn(boolean isWeekday, boolean isVacation) {

        if (!isWeekday || isVacation) {
            return true;
        } else {
            return false;
        }

    }
}
