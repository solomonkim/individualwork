/*
Given an int n, return true if it is within 10 of 100 or 200.
Hint: Check out the C# Math class for absolute value
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _07_NearHundred {

    public boolean NearHundred(int n) {
        return ((Math.abs(100 - n) <= 10) || (Math.abs(200 - n) <= 10));
    }
}
