/*
Given two ints, a and b, return true if one if them is 10 or if their sum is 10. 
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _06_Makes10 {

    public boolean Makes10(int a, int b) {
        return (a == 10) || (b == 10) || (a + b == 10);
    }
}
