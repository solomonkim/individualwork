/*
Given two int values, return their sum. However, if the two values are the same,
then return double their sum. 
 */
package drills.conditionals;

/**
 *
 * @author Solomon Kim
 */
public class _03_SumDouble {

    public int SumDouble(int a, int b) {
        if (a == b) {
            return (2 * (a + b));
        } else {
            return (a + b);
        }
    }
}
