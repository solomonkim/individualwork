/*
Given an int n, return the absolute value of the difference between n and 21,
except return double the absolute value of the difference if n is over 21. 
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _04_Diff21 {

    public int Diff21(int n) {

        if (n <= 21) {
            return (21 - n);
        } else {
            return (2 * (n - 21));
        }

    }
}
