/*
Given a string, take the last char and return a new string with the last char
added at the front and back, so "cat" yields "tcatt". The original string will
be length 1 or more. 
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _13_BackAround {

    public String BackAround(String str) {
        String lastLetter = str.substring(str.length() - 1);
        return lastLetter + str + lastLetter;
    }
}
