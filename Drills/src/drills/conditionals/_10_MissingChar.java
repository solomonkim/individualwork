/*
Given a non-empty string and an int n, return a new string where the char at
index n has been removed. The value of n will be a valid index of a char in the
original string (Don't check for bad index). 
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _10_MissingChar {

    public String MissingChar(String str, int n) {
        return (str.substring(0, n) + str.substring(n + 1, str.length()));
    }
}
