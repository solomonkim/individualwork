/*
Given a string, return a new string where "not " has been added to the front.
However, if the string already begins with "not", return the string unchanged.
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _09_NotString {

    public String NotString(String s) {
        if (s.startsWith("not")) {
            return s;
        } else {
            return ("not " + s);
        }
    }
}
