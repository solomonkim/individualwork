/*
Return true if the given non-negative number is a multiple of 3 or a multiple of
5. Use the % "mod" operator
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _14_Multiple3Or5 {

    public boolean Multiple3Or5(int number) {
        return (number % 3 == 0 || number % 5 == 0);
    }
}
