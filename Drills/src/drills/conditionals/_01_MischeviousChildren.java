/*
We have two children, a and b, and the parameters aSmile and bSmile indicate if
each is smiling. We are in trouble if they are both smiling or if neither of
them is smiling. Return true if we are in trouble. 
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _01_MischeviousChildren {

    public boolean AreWeInTrouble(boolean aSmile, boolean bSmile) {
        if ((aSmile && bSmile) || (!aSmile && !bSmile)) {
            return true;
        } else {
            return false;
        }
    }
}
