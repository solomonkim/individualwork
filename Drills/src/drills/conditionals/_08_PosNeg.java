/*
Given two int values, return true if one is negative and one is positive. Except
if the parameter "negative" is true, then return true only if both are negative. 
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _08_PosNeg {

    public boolean PosNeg(int a, int b, boolean negative) {

        return (((a < 0 && b > 0) || (a > 0 && b < 0)) && !negative) || ((a < 0) && (b < 0) && negative);

    }
}
