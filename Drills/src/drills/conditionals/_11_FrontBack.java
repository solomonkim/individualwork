/*
Given a string, return a new string where the first and last chars have been
exchanged. 
 */
package drills.conditionals;

/**
 * @author Solomon Kim
 */
public class _11_FrontBack {

    public String FrontBack(String str) {

        char[] charArray = str.toCharArray();
        char temp = charArray[0];
        charArray[0] = charArray[charArray.length - 1];
        charArray[charArray.length - 1] = temp;
        return new String(charArray);

//        return (str.substring(str.length() - 1, str.length()) + str.substring(1, str.length() -1) + str.subbstring(0, 1));
    }
}
