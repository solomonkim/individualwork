/*
The children in Cleveland spend most of the day playing outside. In particular,
they play if the temperature is between 60 and 90 (inclusive). Unless it is
summer, then the upper limit is 100 instead of 90. Given an int temperature and
a boolean isSummer, return true if the children play and false otherwise. 
 */
package drills.logic;

/**
 * @author Solomon Kim
 */
public class _03_PlayOutside {

    public boolean PlayOutside(int temp, boolean isSummer) {
        return ((temp > 59) && ((temp < 91) || ((temp < 101) && isSummer)));
    }
}
