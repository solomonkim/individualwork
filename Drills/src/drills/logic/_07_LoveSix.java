/*
The number 6 is a truly great number. Given two int values, a and b, return true
if either one is 6. Or if their sum or difference is 6.
 */
package drills.logic;

/**
 * @author Solomon Kim
 */
public class _07_LoveSix {

    public boolean LoveSix(int a, int b) {
        return ((a == 6) || (b == 6) || (a + b == 6) || (a - b == 6) || (b - a == 6));
    }
}
