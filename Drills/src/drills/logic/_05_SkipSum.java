/*
Given 2 ints, a and b, return their sum. However, sums in the range 10..19
inclusive are forbidden, so in that case just return 20. 
 */
package drills.logic;

/**
 * @author Solomon Kim
 */
public class _05_SkipSum {

    public int SkipSum(int a, int b) {
        int sum;

        if (a + b > 9 && a + b < 20) {
            return 20;
        } else {
            return a + b;
        }
    }
}
