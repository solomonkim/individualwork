/*
Given a number n, return true if n is in the range 1..10, inclusive. Unless
"outsideMode" is true, in which case return true if the number is less or equal
to 1, or greater or equal to 10. 
 */
package drills.logic;

/**
 * @author Solomon Kim
 */
public class _08_InRange {

    public boolean InRange(int n, boolean outsideMode) {
        return (n > 0 && n < 11) || (outsideMode && (n < 1 || n > 10));
    }
}
