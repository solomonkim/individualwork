/*
We'll say a number is special if it is a multiple of 11 or if it is one more
than a multiple of 11. Return true if the given non-negative number is special.
Use the % "mod" operator
 */
package drills.logic;

/**
 * @author Solomon Kim
 */
public class _09_SpecialEleven {

    public boolean SpecialEleven(int n) {
        return ((((n - 1) % 11) == 0) || (n % 11 == 0));
    }
}
