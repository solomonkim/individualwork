/*
Given a string, return a string length 1 from its front, unless front is false,
in which case return a string length 1 from its back. The string will be non-
empty. 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _11_TakeOne {

    public String TakeOne(String str, boolean fromFront) {
        if (fromFront) {
            return str.substring(0, 1);
        } else {
            return str.substring(str.length() - 1, str.length());
        }
    }
}
