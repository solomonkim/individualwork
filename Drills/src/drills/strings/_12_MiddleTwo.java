/*
Given a string of even length, return a string made of the middle two chars, so
the string "string" yields "ri". The string length will be at least 2. 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _12_MiddleTwo {

    public String MiddleTwo(String str) {
        String first = str.substring((str.length() / 2) - 1, str.length() / 2);
        String second = str.substring(str.length() / 2, (str.length() / 2) + 1);
        return first + second;
    }

}
