/*
Given a string, return a version without the first and last char, so "Hello"
yields "ell". The string length will be at least 2. 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _07_TrimOne {

    public String TrimOne(String str) {
        return str.substring(1, str.length() - 1);
    }
}
