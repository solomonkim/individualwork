/*
Given a string of even length, return the first half. So the string "WooHoo"
yields "Woo". 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _06_FirstHalf {

    public String FirstHalf(String str) {

        return str.substring(0, (str.length() / 2));

    }
}
