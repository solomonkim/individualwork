/*
Given a string, return a "rotated right 2" version where the last 2 chars are
moved to the start. The string length will be at least 2. 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _10_RotateRight2 {

    public String RotateRight2(String str) {
        String lastTwo, remainder;

        lastTwo = str.substring(str.length() - 2, str.length());
        remainder = str.substring(0, str.length() - 2);

        return lastTwo + remainder;
    }
}
