/*
Given two strings, a and b, return the result of putting them together in the
order abba, e.g. "Hi" and "Bye" returns "HiByeByeHi". 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _02_Abba {

    public String Abba(String a, String b) {
        return a + b + b + a;
    }

}
