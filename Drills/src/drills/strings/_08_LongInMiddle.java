/*
Given 2 strings, a and b, return a string of the form short+long+short, with the
shorter string on the outside and the longer string on the inside. The strings
will not be the same length, but they may be empty (length 0). 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _08_LongInMiddle {

    public String LongInMiddle(String a, String b) {

        String longWord, shortWord;

        if (a.length() > b.length()) {
            longWord = a;
            shortWord = b;
        } else {
            longWord = b;
            shortWord = a;
        }

        return shortWord + longWord + shortWord;

    }
}
