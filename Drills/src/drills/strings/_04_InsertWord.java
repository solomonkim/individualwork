/*
Given an "out" string length 4, such as "<<>>", and a word, return a new string
where the word is in the middle of the out string, e.g. "<<word>>".
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _04_InsertWord {

    public String InsertWord(String container, String word) {
        String prefix = container.substring(0, 2);
        String suffix = container.substring(2, 4);
        return prefix + word + suffix;
    }
}
