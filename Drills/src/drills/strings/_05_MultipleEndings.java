/*
Given a string, return a new string made of 3 copies of the last 2 chars of the
original string. The string length will be at least 2. 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _05_MultipleEndings {

    public String MultipleEndings(String str) {
        String lastTwo = str.substring(str.length() - 2, str.length());
        return lastTwo + lastTwo + lastTwo;
    }
}
