/*
Given a string name, e.g. "Bob", return a greeting of the form "Hello Bob!".
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _01_SayHi {

    public String SayHi(String name) {
        return "Hello " + name + "!";
    }

}
