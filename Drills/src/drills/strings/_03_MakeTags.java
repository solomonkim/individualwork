/*
The web is built with HTML strings like "<i>Yay</i>" which draws Yay as italic
text. In this example, the "i" tag makes <i> and </i> which surround the word
"Yay". Given tag and word strings, create the HTML string with tags around the
word, e.g. "<i>Yay</i>". 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _03_MakeTags {

    public String MakeTags(String tag, String content) {
        return "<" + tag + ">" + content + "</" + tag + ">";
    }

}
