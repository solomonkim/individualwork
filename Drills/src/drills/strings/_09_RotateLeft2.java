/*
Given a string, return a "rotated left 2" version where the first 2 chars are
moved to the end. The string length will be at least 2. 
 */
package drills.strings;

/**
 * @author Solomon Kim
 */
public class _09_RotateLeft2 {

    public String RotateLeft2(String str) {
        String firstTwo, remainder;

        firstTwo = str.substring(0, 2);
        remainder = str.substring(2, str.length());

        return remainder + firstTwo;
    }
}
