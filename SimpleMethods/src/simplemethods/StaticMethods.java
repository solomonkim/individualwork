package simplemethods;

import java.util.Scanner;

public class StaticMethods {

    public static void main(String[] args) {
        boolean something;
        Scanner sc = new Scanner(System.in);
        System.out.println("true or false?");
        something = sc.nextBoolean();
        something = not(something);
        System.out.println(something);
        System.out.println(excite("give me exclamation marks"));
    }

    public static boolean not(boolean somethingSomething) {
        return !somethingSomething;
        }
    
    public static String excite(String exclamationMarks){
        return ("!!!!!!!!");
    }
}