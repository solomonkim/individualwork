package interestcalculator;

import java.util.Scanner;
import java.text.NumberFormat;

/**
 * @author Carrie and Solomon
 */
public class InterestCalculator {

    public static void main(String[] args) {
        // declare variables
        NumberFormat nf = NumberFormat.getNumberInstance(); //imported class is used to round to two digits
        nf.setMaximumFractionDigits(2); //set maximum decimal places to 2
        nf.setMinimumFractionDigits(2); //...and minimum to 2
        Scanner sc = new Scanner(System.in); 
        double intRateYearly = -1;
        double intRateQuarterly;
        double beginningPrincipal = -1;
        double numYears = -1;

        double intEarned;

        // take and validate user input
        while (intRateYearly < 0) {
            System.out.println("What is the annual interest rate?");
            intRateYearly = sc.nextDouble();
        }

        while (beginningPrincipal < 0) {
            System.out.println("What is the starting principal amount?");
            beginningPrincipal = sc.nextDouble();
        }

        while (numYears < 0) {
            System.out.println("How many years will the money stay in the fund?");
            numYears = sc.nextDouble();
        }

        intRateQuarterly = (intRateYearly / 4);
        double currentBal = beginningPrincipal;
        double prevBal;
        prevBal = currentBal;

        // calculate principal and interest earned each year for number of years specified by user
        for (int i = 1; i <= numYears * 4; i++) {
            currentBal = currentBal * (1 + (intRateQuarterly / 100)); //calculate balance after interest is compounded quarterly
            intEarned = currentBal - prevBal;

            //yearly summary
            if (i % 4 == 0) {
                System.out.println("************************************"); //visually divide each year's summary output
                System.out.println("Year: " + i / 4);
                System.out.println("Year Start Balance: $" + nf.format(prevBal)); //print starting balance
                System.out.println("Year End Balance: $" + nf.format(currentBal)); //...and ending balance every 4th quarter
                prevBal = currentBal; //update previous balance before recalculating new current balance
                System.out.println("Interest Earned: $" + nf.format(intEarned));
                System.out.println("************************************");
            }

        }
    }
}
