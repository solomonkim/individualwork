package windowsmastery;

import java.util.Scanner;

public class WindowsMastery {

    public static void main(String[] args) {
        double height = 0;
        double width = 0;
        double perimeter = 0;
        double area = 0;
        double totalCost = 0;
        double glassCost = 3.5;
        double trimCost = 1;
        Scanner sc = new Scanner(System.in);
        String input = "";

        //get the specs of the window and material costs from user input
        System.out.println("Enter the width of your window in feet: ");
        input = sc.nextLine();
        width = Double.parseDouble(input);

        System.out.println("Enter the height of your window in feet: ");
        input = sc.nextLine();
        height = Double.parseDouble(input);

        System.out.println("Enter the cost of glass in dollars/square foot: ");
        input = sc.nextLine();
        glassCost = Double.parseDouble(input);

        System.out.println("Enter the cost of trim in dollars/foot: ");
        input = sc.nextLine();
        trimCost = Double.parseDouble(input);

        //calculate the area and perimeter of the window
        area = height * width;
        perimeter = 2 * (height + width);

        //print the specs and costs
        System.out.println("The height of the window is " + height + ".");
        System.out.println("The width of the window is " + width + ".");
        System.out.println("The area of the window is " + area + ".");
        System.out.println("The perimeter of the window is " + perimeter + ".");
        System.out.println("The cost of glass is " + glassCost + " per square foot.");
        System.out.println("The cost of trim is " + trimCost + " per foot.");

        //calculate and print the cost of the window
        totalCost = (perimeter * trimCost) + (area * glassCost);
        System.out.println("The total cost is " + totalCost + ".");

    }
}