package datastructures.stacks.test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import datastructures.stacks.ArrayStack;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class ArrayStackTest {

    ArrayStack testObj = new ArrayStack();

    public ArrayStackTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testIsEmptyEmptyStack() {
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyPushOne() {
        testObj.push(1);
        Assert.assertFalse(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyPushPastResizeThreshold() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.push(5);
        Assert.assertFalse(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyPushOneThenPopOne() {
        testObj.push(1);
        testObj.pop();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyPushPastResizeThresholdPopAll() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.push(5);
        testObj.pop();
        testObj.pop();
        testObj.pop();
        testObj.pop();
        testObj.pop();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyPopOneEmptyStack() {
        testObj.pop();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void testSizeEmptyStack() {
        Assert.assertEquals(testObj.size(), 0);
    }

    @Test
    public void testSizePushOne() {
        testObj.push(1);
        Assert.assertEquals(testObj.size(), 1);
    }

    @Test
    public void testSizePushPastResizeThreshold() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.push(5);
        Assert.assertEquals(testObj.size(), 5);
    }

    @Test
    public void testSizePushOnePopOne() {
        testObj.push(1);
        testObj.pop();
        Assert.assertEquals(testObj.size(), 0);
    }

    @Test
    public void testSizePushFourPopTwo() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.pop();
        testObj.pop();
        Assert.assertEquals(testObj.size(), 2);
    }

    @Test
    public void testSizePushPastResizeThresholdPopWithoutResizingBackDown() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.push(5);
        testObj.push(6);
        testObj.pop();
        testObj.pop();
        Assert.assertEquals(testObj.size(), 4);
    }

    @Test
    public void testSizePushPastResizeThresholdPopPastResizeThreshold() {
        testObj.push(1);
        testObj.push(2);
        testObj.push(3);
        testObj.push(4);
        testObj.push(5);
        testObj.push(6);
        testObj.pop();
        testObj.pop();
        testObj.pop();
        testObj.pop();
        testObj.pop();
        Assert.assertEquals(testObj.size(), 1);
    }

    @Test
    public void testPushOnePopOne() {
        testObj.push(102);
        Assert.assertTrue(testObj.pop().equals(102));
    }

    @Test
    public void testPushTwoPopOne() {
        testObj.push(102);
        testObj.push(19);
        Assert.assertTrue(testObj.pop().equals(19));
    }

    @Test
    public void testPushThreePopThree() {
        testObj.push(102);
        testObj.push(19);
        testObj.push(3155);
        Assert.assertTrue(testObj.pop().equals(3155));
        Assert.assertTrue(testObj.pop().equals(19));
        Assert.assertTrue(testObj.pop().equals(102));
    }

    @Test
    public void testPushPastResizeThresholdPopOne() {
        testObj.push(102);
        testObj.push(19);
        testObj.push(3155);
        testObj.push(6);
        testObj.push(77777);
        testObj.push(599);
        Assert.assertTrue(testObj.pop().equals(599));
    }

    @Test
    public void testPushPastResizeThresholdPopAll() {
        testObj.push(102);
        testObj.push(19);
        testObj.push(3155);
        testObj.push(6);
        testObj.push(77777);
        testObj.push(599);
        Assert.assertTrue(testObj.pop().equals(599));
        Assert.assertTrue(testObj.pop().equals(77777));
        Assert.assertTrue(testObj.pop().equals(6));
        Assert.assertTrue(testObj.pop().equals(3155));
        Assert.assertTrue(testObj.pop().equals(19));
        Assert.assertTrue(testObj.pop().equals(102));
    }

}
