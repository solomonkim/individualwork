/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.stacks.test;

import datastrucutres.linkedlists.LinkedList;
import datastrucutres.linkedlists.LinkedListNodeImpl;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ahill
 */
public class LinkedListTest {

    LinkedList linkedList = new LinkedListNodeImpl();

    @Test
    public void testEmptyWhenNew() {
        Assert.assertTrue(linkedList.isEmpty());
        Assert.assertEquals(0, linkedList.size());
    }

    @Test
    public void testGetWhenNew() {
        Assert.assertNull(linkedList.get(0));
        Assert.assertNull(linkedList.get(100));
        Assert.assertNull(linkedList.get(-1));
        Assert.assertTrue(linkedList.isEmpty());
        Assert.assertEquals(0, linkedList.size());
    }

    @Test
    public void testRemoveWhenNew() {
        Assert.assertNull(linkedList.remove(0));
        Assert.assertNull(linkedList.remove(100));
        Assert.assertNull(linkedList.remove(-1));
        Assert.assertTrue(linkedList.isEmpty());
        Assert.assertEquals(0, linkedList.size());
    }

    @Test
    public void testInsertBadWhenNew() {
        linkedList.insert(1, "Thing");
        Assert.assertTrue(linkedList.isEmpty());
        Assert.assertEquals(0, linkedList.size());

        linkedList.insert(100, "Thing");
        Assert.assertTrue(linkedList.isEmpty());
        Assert.assertEquals(0, linkedList.size());

        linkedList.insert(-1, "Thing");
        Assert.assertTrue(linkedList.isEmpty());
        Assert.assertEquals(0, linkedList.size());
    }

    @Test
    public void testInsertWhenNew() {
        linkedList.insert(0, "Thing");
        Assert.assertFalse(linkedList.isEmpty());
        Assert.assertEquals(1, linkedList.size());
    }

    @Test
    public void testAppendWhenNew() {
        linkedList.append("Thing1");
        Assert.assertFalse(linkedList.isEmpty());
        Assert.assertEquals(1, linkedList.size());
        Assert.assertEquals("Thing1", linkedList.get(0));
        Assert.assertEquals("Thing1", linkedList.remove(0));
    }

    @Test
    public void testPrependWhenNew() {
        linkedList.prepend("Thing1");
        Assert.assertFalse(linkedList.isEmpty());
        Assert.assertEquals(1, linkedList.size());
        Assert.assertEquals("Thing1", linkedList.get(0));
        Assert.assertEquals("Thing1", linkedList.remove(0));
    }

    @Test
    public void testAppendMoreWhenNew() {
        linkedList.append("Thing1");
        linkedList.append("Thing2");
        Assert.assertFalse(linkedList.isEmpty());
        Assert.assertEquals(2, linkedList.size());
        Assert.assertEquals("Thing1", linkedList.get(0));
        Assert.assertEquals("Thing2", linkedList.remove(1));
    }

    @Test
    public void testPrependMoreWhenNew() {
        linkedList.prepend("Thing1");
        linkedList.prepend("Thing2");
        Assert.assertFalse(linkedList.isEmpty());
        Assert.assertEquals(2, linkedList.size());
        Assert.assertEquals("Thing2", linkedList.get(0));
        Assert.assertEquals("Thing1", linkedList.remove(1));
    }

    @Test
    public void testAppendPrependCombo() {
        linkedList.prepend("Thing1");
        linkedList.prepend("Thing2");
        linkedList.append("Thing3");
        linkedList.append("Thing4");
        Assert.assertFalse(linkedList.isEmpty());
        Assert.assertEquals(4, linkedList.size());
        Assert.assertEquals("Thing2", linkedList.get(0));
        Assert.assertEquals("Thing4", linkedList.remove(3));
    }

    @Test
    public void testGetOutsideFullStackIndex() {
        linkedList.prepend("Thing1");
        linkedList.prepend("Thing2");
        linkedList.append("Thing3");
        linkedList.append("Thing4");
        Assert.assertNull(linkedList.get(4));
        Assert.assertNull(linkedList.get(400));
        Assert.assertNull(linkedList.get(-12));
        Assert.assertNull(linkedList.get(5));
    }
    
    @Test
    public void testRemoveOutsideFullStackIndex() {
        linkedList.prepend("Thing1");
        linkedList.prepend("Thing2");
        linkedList.append("Thing3");
        linkedList.append("Thing4");
        Assert.assertNull(linkedList.remove(4));
        Assert.assertNull(linkedList.remove(400));
        Assert.assertNull(linkedList.remove(-12));
        Assert.assertNull(linkedList.remove(5));
    }
    
    @Test
    public void testInsertOutsideFullStackIndex() {
        linkedList.prepend("Thing1");
        linkedList.prepend("Thing2");
        linkedList.append("Thing3");
        linkedList.append("Thing4");
        
        linkedList.insert(-14, "Thing");
        Assert.assertEquals(4, linkedList.size());

        linkedList.insert(100, "Thing");
        Assert.assertEquals(4, linkedList.size());

        linkedList.insert(5, "Thing");
        Assert.assertEquals(4, linkedList.size());

    }
    
    @Test
    public void testInsertFullStackIndex() {
        linkedList.prepend("Thing1");
        linkedList.prepend("Thing2");
        linkedList.append("Thing3");
        linkedList.append("Thing4");
        
        linkedList.insert(2, "Llama");
        Assert.assertEquals(5, linkedList.size());
        Assert.assertEquals("Llama", linkedList.get(2));
        
        linkedList.insert(0, "Poofhead Chicken");
        Assert.assertEquals(6, linkedList.size());
        Assert.assertEquals("Poofhead Chicken", linkedList.get(0));
        
        linkedList.insert(6, "Tadpole");
        Assert.assertEquals(7, linkedList.size());
        Assert.assertEquals("Tadpole", linkedList.get(6));
    }
    
    @Test
    public void testRemoveUntilEmptyFromFront(){
        linkedList.prepend("Thing1");
        linkedList.prepend("Thing2");
        linkedList.append("Thing3");
        linkedList.append("Thing4");
        
        Assert.assertEquals(4, linkedList.size());
        Assert.assertEquals("Thing2", linkedList.remove(0));
        Assert.assertEquals("Thing1", linkedList.remove(0));
        Assert.assertEquals("Thing3", linkedList.remove(0));
        Assert.assertEquals("Thing4", linkedList.remove(0));
        Assert.assertEquals(0, linkedList.size());
    }
    
    @Test
    public void testRemoveUntilEmptyFromEnd(){
        linkedList.prepend("Thing1");
        linkedList.prepend("Thing2");
        linkedList.append("Thing3");
        linkedList.append("Thing4");
        
        Assert.assertEquals(4, linkedList.size());
        Assert.assertEquals("Thing4", linkedList.remove(3));
        Assert.assertEquals("Thing3", linkedList.remove(2));
        Assert.assertEquals("Thing1", linkedList.remove(1));
        Assert.assertEquals("Thing2", linkedList.remove(0));
        Assert.assertEquals(0, linkedList.size());
    }
    
    @Test
    public void testRemoveUntilEmpty(){
        linkedList.prepend("Thing1");
        linkedList.prepend("Thing2");
        linkedList.append("Thing3");
        linkedList.append("Thing4");
        
        Assert.assertEquals(4, linkedList.size());
        Assert.assertEquals("Thing1", linkedList.remove(1));
        Assert.assertEquals("Thing3", linkedList.remove(1));
        Assert.assertEquals("Thing4", linkedList.remove(1));
        Assert.assertEquals("Thing2", linkedList.remove(0));
        Assert.assertEquals(0, linkedList.size());
    }
    
    @Test
    public void testGet(){
        linkedList.prepend("Llama");
        linkedList.prepend("Tadpole");
        linkedList.append("Elephant");
        linkedList.insert(2, "Fox");
        linkedList.append("Poofhead Chicken");
        linkedList.prepend("Gorilla");
        linkedList.insert(5, "Lion");
        linkedList.prepend("Seal");
        linkedList.insert(1, "Meerkat");
        
        // Seal, Meerkat, Gorilla, Tadpole, Llama, Fox, Elephant, Lion, Poofhead Chicken
        
        Assert.assertEquals(9, linkedList.size());
        Assert.assertEquals("Seal", linkedList.get(0));
        Assert.assertEquals("Meerkat", linkedList.get(1));
        Assert.assertEquals("Gorilla", linkedList.get(2));
        Assert.assertEquals("Tadpole", linkedList.get(3));
        Assert.assertEquals("Llama", linkedList.get(4));
        Assert.assertEquals("Fox", linkedList.get(5));
        Assert.assertEquals("Elephant", linkedList.get(6));
        Assert.assertEquals("Lion", linkedList.get(7));
        Assert.assertEquals("Poofhead Chicken", linkedList.get(8));
    }
    
}
