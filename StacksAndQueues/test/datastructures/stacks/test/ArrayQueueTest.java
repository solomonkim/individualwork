package datastructures.stacks.test;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import datastructures.queues.ArrayQueue;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class ArrayQueueTest {

    ArrayQueue testObj = new ArrayQueue();

    public ArrayQueueTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testIsEmptyEmptyQueue() {
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyEnqueueOne() {
        testObj.enqueue(1);
        Assert.assertFalse(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyEnqueuePastResizeThreshold() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.enqueue(5);
        Assert.assertFalse(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyEnqueueOneThenDequeueOne() {
        testObj.enqueue(1);
        testObj.dequeue();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyEnqueuePastResizeThresholdDequeueAll() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.enqueue(5);
        testObj.dequeue();
        testObj.dequeue();
        testObj.dequeue();
        testObj.dequeue();
        testObj.dequeue();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void testIsEmptyDequeueOneEmptyQueue() {
        testObj.dequeue();
        Assert.assertTrue(testObj.isEmpty());
    }

    @Test
    public void testSizeEmptyQueue() {
        Assert.assertEquals(testObj.size(), 0);
    }

    @Test
    public void testSizeEnqueueOne() {
        testObj.enqueue(1);
        Assert.assertEquals(testObj.size(), 1);
    }

    @Test
    public void testSizeEnqueuePastResizeThreshold() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.enqueue(5);
        Assert.assertEquals(testObj.size(), 5);
    }

    @Test
    public void testSizeEnqueueOneDequeueOne() {
        testObj.enqueue(1);
        testObj.dequeue();
        Assert.assertEquals(testObj.size(), 0);
    }

    @Test
    public void testSizeEnqueueFourDequeueTwo() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.dequeue();
        testObj.dequeue();
        Assert.assertEquals(testObj.size(), 2);
    }

    @Test
    public void testSizeEnqueuePastResizeThresholdDequeueWithoutResizingBackDown() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.enqueue(5);
        testObj.enqueue(6);
        testObj.dequeue();
        testObj.dequeue();
        Assert.assertEquals(testObj.size(), 4);
    }

    @Test
    public void testSizeEnqueuePastResizeThresholdDequeuePastResizeThreshold() {
        testObj.enqueue(1);
        testObj.enqueue(2);
        testObj.enqueue(3);
        testObj.enqueue(4);
        testObj.enqueue(5);
        testObj.enqueue(6);
        testObj.dequeue();
        testObj.dequeue();
        testObj.dequeue();
        testObj.dequeue();
        testObj.dequeue();
        Assert.assertEquals(testObj.size(), 1);
    }

    @Test
    public void testEnqueueOneDequeueOne() {
        testObj.enqueue(102);
        Assert.assertTrue(testObj.dequeue().equals(102));
    }

    @Test
    public void testEnqueueTwoDequeueOne() {
        testObj.enqueue(102);
        testObj.enqueue(19);
        Assert.assertTrue(testObj.dequeue().equals(102));
    }

    @Test
    public void testEnqueueThreeDequeueThree() {
        testObj.enqueue(102);
        testObj.enqueue(19);
        testObj.enqueue(3155);
        Assert.assertTrue(testObj.dequeue().equals(102));
        Assert.assertTrue(testObj.dequeue().equals(19));
        Assert.assertTrue(testObj.dequeue().equals(3155));
    }

    @Test
    public void testEnqueuePastResizeThresholdDequeueOne() {
        testObj.enqueue(102);
        testObj.enqueue(19);
        testObj.enqueue(3155);
        testObj.enqueue(6);
        testObj.enqueue(77777);
        testObj.enqueue(599);
        Assert.assertTrue(testObj.dequeue().equals(102));
    }

    @Test
    public void testEnqueuePastResizeThresholdDequeueAll() {
        testObj.enqueue(102);
        testObj.enqueue(19);
        testObj.enqueue(3155);
        testObj.enqueue(6);
        testObj.enqueue(77777);
        testObj.enqueue(599);
        Assert.assertTrue(testObj.dequeue().equals(102));
        Assert.assertTrue(testObj.dequeue().equals(19));
        Assert.assertTrue(testObj.dequeue().equals(3155));
        Assert.assertTrue(testObj.dequeue().equals(6));
        Assert.assertTrue(testObj.dequeue().equals(77777));
        Assert.assertTrue(testObj.dequeue().equals(599));
    }

}
