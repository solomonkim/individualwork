/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.generics;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * @author Special K
 */
public class GenericQueue<T> implements Queue<T> {

    ArrayList<T> items = new ArrayList<>();

    @Override
    public void enqueue(T element) {
        items.add(element);
    }

    @Override
    public T dequeue() {
        return items.remove(0);
    }

    @Override
    public T peek() {
        return items.get(0);
    }

    @Override
    public int size() {
        return items.size();
    }

    @Override
    public Boolean isEmpty() {
        return items.isEmpty();
    }

    @Override
    public String getAuthorName() {
        return "Special K";
    }

    @Override
    public Iterator<T> iterator() {
        return new GenericIterator();
    }

    private class GenericIterator implements Iterator {

        int visited;

        @Override
        public boolean hasNext() {
            return visited < items.size();
        }

        @Override
        public T next() {
            visited++;
            return items.get(0);
        }

    }

}
