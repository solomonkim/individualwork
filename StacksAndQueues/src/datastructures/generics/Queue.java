/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.generics;

import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public interface Queue<T> extends Iterable<T> {

    void enqueue(T element);

    T dequeue();

    T peek();

    int size();

    Boolean isEmpty();

    String getAuthorName();
}
