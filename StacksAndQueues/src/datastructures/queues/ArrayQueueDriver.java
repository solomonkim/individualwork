/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.queues;

/**
 *
 * @author apprentice
 */
public class ArrayQueueDriver {

    public static void main(String[] args) {

        ArrayQueue myQueue = new ArrayQueue();
        int count = 0;

        myQueue.enqueue(count++);
        System.out.println(myQueue.dequeue());

        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());

        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());

        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());

        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        myQueue.enqueue(count++);
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
        System.out.println(myQueue.dequeue());
    }
}
