//package datastructures.queues;
//
//import java.util.Iterator;
//
///**
// * @author Solomon Kim
// */
//public class ArrayQueue implements Queue {
//
//    private Object[] items;
//    private int numItems;
//    private int head = 0;
//    private int tail = 0;
//    private static final int DEFAULT_INITIAL_SIZE = 4;
//    public static final String ANSI_RESET = "\u001B[0m";
//    public static final String ANSI_BLACK = "\u001B[30m";
//    public static final String ANSI_RED = "\u001B[31m";
//    public static final String ANSI_GREEN = "\u001B[32m";
//
//    public ArrayQueue() {
//        this.items = new Object[DEFAULT_INITIAL_SIZE];
//        this.numItems = 0;
//    }
//
//    @Override
//    public String toString() {
//        String queue = "]";
//        for (int i = 0; i < items.length; i++) {
//            queue = "|" + queue;
//            if (i == head) {
//                queue = ANSI_GREEN + "h" + ANSI_RESET + queue;
//            }
//
//            if (items[i] == null) {
//                queue = "  " + queue;
//            } else {
//                queue = items[i].toString() + queue;
//            }
//
//            if (i == tail) {
//                queue = ANSI_RED + "t" + ANSI_RESET + queue;
//            }
//        }
//        queue = "[" + queue.replace("|]", "]");
//
//        return queue;
//    }
//
//    @Override
//    public void enqueue(Object item) {
//        System.out.println("before enqueue");
//        System.out.println(this.toString());
//        //if zero items, head and tail must be at index 0
//        head = 0;
//        tail = 0;
//        //enqueue the item
//        items[tail] = item;
//        //increment numItems, move tail position to next
//        numItems++;
//        tail++;
//        //if new tail position is out of bounds, move it to index 0
//        if (tail + 1 == items.length) {
//            tail = 0;
//        }
//        //if the queue is full, resize it
//        if (numItems == items.length) {
//            resize(items.length * 2);
//        }
//        //if there is one item, head and tail must be in same position
//        if (numItems == 1) {
//            tail = head;
//        }
//        System.out.println("after enqueue");
//        System.out.println(this.toString());
//    }
//
//    @Override
//    public Object dequeue() {
//        System.out.println("before dequeue");
//        System.out.println(this.toString());
//        Object itemToDequeue;
//        //if there are no items in queue, return null
//        if (numItems == 0) {
//            return null;
//        } else {
//            //dequeue the item at the head position
//            itemToDequeue = items[head];
//            //decrement numItems, move head position to next
//            numItems--;
//            head++;
//            //if the head is at the end of the queue, move it to the beginning
//            if (head + 1 == items.length) {
//                head = 0;
//            }
//            //if there is one item, head and tail must be in same position
//            if (numItems == 1) {
//                head = tail;
//            }
//            System.out.println("after dequeue");
//            System.out.println(this.toString());
//            return itemToDequeue;
//        }
//    }
//
//    private void resize(int newSize) {
//        System.out.println("before resize");
//        System.out.println(this.toString());
//        //declare a new temporary Object array of the new size
//        Object[] temp = new Object[newSize];
//        //if head > tail, enqueue values from head to end, then zero to tail
//        if (head > tail) {
//            int j = 0;
//            for (int i = head; i < items.length; i++) {
//                temp[j] = items[i];
//                j++;
//            }
//            for (int i = 0; i < tail; i++) {
//                temp[j] = items[i];
//                j++;
//            }
//        } //otherwise, if tail > head, enqueue values from head to tail
//        else {
//            int j = 0;
//            for (int i = head; i < head + numItems; i++) {
//                temp[j] = items[i];
//                j++;
//            }
//
//        }//reset head and tail positions;
//        head = 0;
//        tail = items.length - 1;
//
//        //set the working array equal to the temporary array
//        items = temp;
//        System.out.println("after resize");
//        System.out.println(this.toString());
//    }
//
//    @Override
//    public boolean isEmpty() {
//        return numItems == 0;
//    }
//
//    @Override
//    public int size() {
//        return numItems;
//    }
//
//    @Override
//    public Iterator iterator() {
//        return new QueueIterator();
//    }
//
//    private class QueueIterator implements Iterator {
//
//        int index;
//        int visited;
//
//        public QueueIterator() {
//            index = head;
//            visited = 0;
//        }
//
//        @Override
//        public boolean hasNext() {
//            return visited < numItems;
//        }
//
//        @Override
//        public Object next() {
//            Object item;
//            if (index >= items.length) {
//                index = 0;
//            }
//            item = items[index];
//            index++;
//            visited++;
//            return item;
//        }
//
//    }
//
//}
