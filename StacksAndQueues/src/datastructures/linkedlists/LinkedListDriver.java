/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.linkedlists;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class LinkedListDriver {

    public static void main(String[] args) {
        
        LinkedList list = new LinkedListNodeImpl();
        list.append("llama");
        list.append("water buffalo");
        list.append(4);
        list.append(4.9);
        list.append(true);
        
        LinkedList integerList = new LinkedListNodeImpl();
        integerList.append(1);
        integerList.append(34);
        integerList.append(4);
        integerList.append(23);
        integerList.append(7);
        integerList.append("llama");
        
        int sum = 0;
        
        for (int i = 0; i < integerList.size(); i++) {
            Object integerToCast = integerList.get(i);
            Integer castInteger = (Integer)integerToCast;
            sum += castInteger;
                    
        }
        
        ArrayList<Integer> actualIntegerList = new ArrayList<>();
        
        System.out.println("Total is " + sum);
    }

}
