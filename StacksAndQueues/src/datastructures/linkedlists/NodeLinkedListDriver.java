/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datastructures.linkedlists;

/**
 *
 * @author ahill
 */
public class NodeLinkedListDriver {
    public static void main(String[] args) {
        LinkedList nodeList = new LinkedListNodeImpl();
        int count = 1;
        
        nodeList.prepend(count++);
        nodeList.prepend(count++);
        nodeList.prepend(count++);
        nodeList.prepend(count++);
        nodeList.prepend(count++);
        nodeList.prepend(count++);
        nodeList.prepend(count++);
        nodeList.prepend(count++);
        
        System.out.println(nodeList.get(5));
        
        nodeList.append(count++);
        nodeList.append(count++);
        nodeList.append(count++);
        nodeList.append(count++);
        nodeList.append(count++);
        nodeList.append(count++);
        
        nodeList.insert(0, count++);
        nodeList.insert(5, count++);
        nodeList.insert(2, count++);
        nodeList.insert(2, count++);
        nodeList.insert(2, count++);
        
        nodeList.remove(8);
        
        
        System.out.println(nodeList.get(0));
        System.out.println(nodeList.get(nodeList.size() - 1));
        
        
    }
}
