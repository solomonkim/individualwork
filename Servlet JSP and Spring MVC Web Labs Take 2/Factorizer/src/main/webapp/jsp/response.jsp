<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Factorizer - Results</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
    <center>
        <div class="container">
            <h1>Results</h1>
            <p>
                factors of <c:out value="${factorizedNumber}" />: <c:out value="${listOfFactors}" />
            </p>
            <p>
                <c:out value="${factorizedNumber}" default="n/a" /> is<c:if test="${!isPerfect}"> not</c:if> perfect.
                </p>
                <p>
                <c:out value="${factorizedNumber}" default="n/a" /> is<c:if test="${!isPrime}"> not</c:if> prime.
            </p>
        </div>
    </center>
</body>
</html>
