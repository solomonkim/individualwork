<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>

    <head>
        <title>Factorizer Spring MVC Web Lab</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
    <center>
        <div class="container">
            <h1>Factorizer Spring MVC Web Lab</h1>
            <hr/>
            <p>Enter a positive integer to be factorized...</p>
            <form action="Factorizer" method="POST">
                <input type="number" step="1" name="numberToFactorize" min="1">
                <br />
                <button type="submit" class="btn btn-info">Factorize it!</button>
            </form>
        </div>
    </center>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>

</html>

