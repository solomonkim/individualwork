<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Spring MVC Site Lab - Interest Calculator</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class="container">
            <h1>Interest Calculator</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevenscontroller/getluckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizercontroller/getfactorizer">Factorizer</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/interestcalculatorcontroller/getinterestcalculator">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalculatorcontroller/getflooringcalculator">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalculatorcontroller/gettipcalculator">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconvertercontroller/getunitconverter">Unit Converter</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3>Description</h3>
                    <hr />
                    <p>Fill in the fields provided and click "Earn interest!" 
                        The app will summarize how much interest you would earn,
                        given the parameters you provided.
                    </p>
                    <form action="runinterestcalculator" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                beginning principal:
                            </div>
                            <div class="col-md-8">
                                <input type="number" min="0" step="0.01" name="principal">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                annual interest rate:
                            </div>
                            <div class="col-md-8">
                                <input type="number" min="0" step="0.01" name="rate">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                number of years:
                            </div>
                            <div class="col-md-8">
                                <input type="number" min="0" step="0.01" name="years">
                            </div>
                        </div>
                        <br /><br />
                        <button type="submit" class="btn btn-info">Earn interest!</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <h3>Results</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">beginning principal:</div>
                        <div class="col-md-8"><c:out value="${beginningPrincipal}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">annual interest rate  (compounded quarterly):</div>
                        <div class="col-md-8"><c:out value="${interestRate}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">number of years:</div>
                        <div class="col-md-8"><c:out value="${numYears}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">ending balance:</div>
                        <div class="col-md-8"><c:out value="${currentBalance}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">interest earned:</div>
                        <div class="col-md-8"><c:out value="${interestEarned}" /></div>
                    </div>
                </div>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>

</html>