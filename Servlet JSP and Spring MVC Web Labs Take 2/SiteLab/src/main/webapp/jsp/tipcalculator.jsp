<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Spring MVC Site Lab - Tip Calculator</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class="container">
            <h1>Tip Calculator</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevenscontroller/getluckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizercontroller/getfactorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalculatorcontroller/getinterestcalculator">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalculatorcontroller/getflooringcalculator">Flooring Calculator</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/tipcalculatorcontroller/gettipcalculator">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconvertercontroller/getunitconverter">Unit Converter</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3>Description</h3>
                    <hr />
                    <p>Enter the amount of your bill, choose a percentage tip to
                        leave and click "Tip!" The app will calculate it.
                    </p>
                    <form action="runtipcalculator" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                bill amount:
                            </div>
                            <div class="col-md-8">
                                <input type="number" min="0" step="0.01" name="baseAmount">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                tip percentage:
                            </div>
                            <div class="col-md-8">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                15%
                            </div>
                            <div class="col-md-8">
                                <input type="radio" name="tipPercentage" value="fifteen">
                            </div>
                            <div class="col-md-4">
                                18%
                            </div>
                            <div class="col-md-8">
                                <input type="radio" name="tipPercentage" value="eighteen">
                            </div>
                            <div class="col-md-4">
                                20%
                            </div>
                            <div class="col-md-8">
                                <input type="radio" name="tipPercentage" value="twenty">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">Tip!</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <h3>Results</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">bill amount:</div>
                        <div class="col-md-8"><c:out value="${baseAmount}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">tip (<c:out value="${tipPercentageInt}" />%):</div>
                        <div class="col-md-8"><c:out value="${tipAmount}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">total:</div>
                        <div class="col-md-8"><c:out value="${totalAmount}" /></div>
                    </div>
                </div>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>

</html>