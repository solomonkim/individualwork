<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Spring MVC Site Lab - Lucky Sevens</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class="container">
            <h1>Factorizer</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevenscontroller/getluckysevens">Lucky Sevens</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/factorizercontroller/getfactorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalculatorcontroller/getinterestcalculator">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalculatorcontroller/getflooringcalculator">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalculatorcontroller/gettipcalculator">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconvertercontroller/getunitconverter">Unit Converter</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3>Description</h3>
                    <hr />
                    <p>Enter a positive integer and click "Factorize it!" The
                        app will tell you its factors and whether the number is 
                        prime (no factors other than 1 and itself) and perfect
                        (sum of the factors equals twice the number itself).
                    </p>
                    <form action="runfactorizer" method="POST">
                        <input type="number" min="1" name="numberToFactorize">
                        <br /><br />
                        <button type="submit" class="btn btn-info">Factorize it!</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <h3>Results</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">your number:</div>
                        <div class="col-md-8"><c:out value="${factorizedNumber}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">factors:</div>
                        <div class="col-md-8"><c:out value="${listOfFactors}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">prime:</div>
                        <div class="col-md-8"><c:out value="${isPrime}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">perfect:</div>
                        <div class="col-md-8"><c:out value="${isPerfect}" /></div>
                    </div>
                </div>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>

</html>