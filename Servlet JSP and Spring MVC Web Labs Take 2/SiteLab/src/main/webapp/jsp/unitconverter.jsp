<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Spring MVC Site Lab - Unit Converter</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class="container">
            <h1>Unit Converter</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevenscontroller/getluckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizercontroller/getfactorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalculatorcontroller/getinterestcalculator">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalculatorcontroller/getflooringcalculator">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalculatorcontroller/gettipcalculator">Tip Calculator</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/unitconvertercontroller/getunitconverter">Unit Converter</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3>Description</h3>
                    <hr />
                    <p>Enter a temperature and the unit conversion you want the
                        app to perform then click "Convert those units!"
                    </p>
                    <form action="rununitconverter" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                degrees:
                            </div>
                            <div class="col-md-8">
                                <input type="number" step="0.1" name="degrees">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                conversion:
                            </div>
                            <div class="col-md-8">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                C to F
                            </div>
                            <div class="col-md-8">
                                <input type="radio" name="units" value="C">
                            </div>
                            <div class="col-md-4">
                                F to C
                            </div>
                            <div class="col-md-8">
                                <input type="radio" name="units" value="F">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-info">Convert those units!</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <h3>Results</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">entered temperature (<c:out value="${originalUnits}" />):</div>
                        <div class="col-md-8"><c:out value="${originalDegrees}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">converted temperature (<c:out value="${convertedUnits}" />):</div>
                        <div class="col-md-8"><c:out value="${convertedDegrees}" /></div>
                    </div>
                </div>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>

</html>