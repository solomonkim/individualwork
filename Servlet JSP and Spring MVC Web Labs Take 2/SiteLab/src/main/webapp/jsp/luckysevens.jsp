<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Spring MVC Site Lab - Lucky Sevens</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class="container">
            <h1>Lucky Sevens</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/luckysevenscontroller/getluckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizercontroller/getfactorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalculatorcontroller/getinterestcalculator">Interest Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/flooringcalculatorcontroller/getflooringcalculator">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalculatorcontroller/gettipcalculator">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconvertercontroller/getunitconverter">Unit Converter</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3>Description</h3>
                    <hr />
                    <p>Enter the amount of money you want to start with, then
                        click "Roll em!" The app will roll a pair of dice. If a
                        7 is rolled, you will win $4. If any other number is
                        rolled, you will lose $1. This will repeat until you are
                        out of money then give you a summary of the results. 
                        Good luck!</p>
                    <form action="runluckysevens" method="POST">
                        <input type="number" min="1" name="startingBankroll">
                        <br /><br />
                        <button type="submit" class="btn btn-info">Roll 'em!</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <h3>Results</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">total dice rolls:</div>
                        <div class="col-md-8"><c:out value="${rollCount}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">bankroll peak:</div>
                        <div class="col-md-8"><c:out value="${maxBankroll}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">roll count at peak:</div>
                        <div class="col-md-8"><c:out value="${rollCountAtMax}" /></div>
                    </div>
                </div>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>

</html>