<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Spring MVC Site Lab - Flooring Calculator</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class="container">
            <h1>Flooring Calculator</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/index.jsp">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/luckysevenscontroller/getluckysevens">Lucky Sevens</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/factorizercontroller/getfactorizer">Factorizer</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/interestcalculatorcontroller/getinterestcalculator">Interest Calculator</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/flooringcalculatorcontroller/getflooringcalculator">Flooring Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/tipcalculatorcontroller/gettipcalculator">Tip Calculator</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/unitconvertercontroller/getunitconverter">Unit Converter</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h3>Description</h3>
                    <hr />
                    <p>Fill in the fields provided and click "Get flooring costs!" 
                        The app will calculate the total cost of flooring.
                    </p>
                    <form action="runflooringcalculator" method="POST">
                        <div class="row">
                            <div class="col-md-4">
                                width (feet):
                            </div>
                            <div class="col-md-8">
                                <input type="number" min="0" step="0.01" name="width">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                length (feet):
                            </div>
                            <div class="col-md-8">
                                <input type="number" min="0" step="0.01" name="length">
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                materials cost per square foot:
                            </div>
                            <div class="col-md-8">
                                <input type="number" min="0" step="0.01" name="costSqFt">
                            </div>
                        </div>
                        <br /><br />
                        <button type="submit" class="btn btn-info">Get flooring costs!</button>
                    </form>
                </div>
                <div class="col-md-6">
                    <h3>Results</h3>
                    <hr>
                    <div class="row">
                        <div class="col-md-4">width (feet):</div>
                        <div class="col-md-8"><c:out value="${width}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">length (feet):</div>
                        <div class="col-md-8"><c:out value="${length}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">materials cost per square foot:</div>
                        <div class="col-md-8"><c:out value="${costSqFt}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">total materials cost:</div>
                        <div class="col-md-8"><c:out value="${flooringCost}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">time billed:</div>
                        <div class="col-md-8"><c:out value="${laborTime}" /></div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">total labor cost:</div>
                        <div class="col-md-8"><c:out value="${laborCost}" /></div>
                    </div>
                </div>
            </div>
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>

</html>