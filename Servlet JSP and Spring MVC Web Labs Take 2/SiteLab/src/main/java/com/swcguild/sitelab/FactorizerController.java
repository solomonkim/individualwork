package com.swcguild.sitelab;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/factorizercontroller"})
public class FactorizerController {

    public FactorizerController() {

    }

    @RequestMapping(value = "/getfactorizer", method = RequestMethod.GET)
    public String getFactorizer(Map<String, Object> model) {
        return "factorizer";
    }

    @RequestMapping(value = "/runfactorizer", method = RequestMethod.POST)
    public String runFactorizer(HttpServletRequest request, Model model) {
        int num = Integer.parseInt(request.getParameter("numberToFactorize"));
        Factorizer factor = new Factorizer(num);
        List<Integer> listOfFactors = new ArrayList<>();

        for (int i = 0; i < factor.getFactors().size(); i++) {
            Integer a = factor.getFactors().get(i);
            listOfFactors.add(a);
        }

        model.addAttribute("factorizedNumber", num);
        model.addAttribute("listOfFactors", listOfFactors);
        model.addAttribute("isPerfect", factor.isPerfect());
        model.addAttribute("isPrime", factor.isPrime());

        return "factorizer";
    }

}
