package com.swcguild.sitelab;

import java.util.ArrayList;

public class Factorizer {

    private int num;
    private ArrayList<Integer> factors = new ArrayList<>();
    private boolean isPerfect;
    private boolean isPrime;
    private int factorSum = 0;

    public Factorizer(int number) {
        num = number;
        for (int i = 1; i <= num; i++) {
            if (num % i == 0) {
                factors.add(i);
                factorSum += i;
            }
        }

        if (factorSum == 2 * num) {
            isPerfect = true;
        } else {
            isPerfect = false;
        }

        if (factors.size() == 2) {
            isPrime = true;
        } else {
            isPrime = false;
        }
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public ArrayList<Integer> getFactors() {
        return factors;
    }

    public boolean isPerfect() {
        return isPerfect;
    }

    public boolean isPrime() {
        return isPrime;
    }
}
