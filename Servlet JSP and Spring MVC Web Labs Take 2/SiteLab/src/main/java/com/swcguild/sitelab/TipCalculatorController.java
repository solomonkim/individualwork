package com.swcguild.sitelab;

import java.text.DecimalFormat;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/tipcalculatorcontroller"})
public class TipCalculatorController {

    public TipCalculatorController() {

    }

    @RequestMapping(value = "/gettipcalculator", method = RequestMethod.GET)
    public String getTipCalculator(Map<String, Object> model) {
        return "tipcalculator";
    }

    @RequestMapping(value = "/runtipcalculator", method = RequestMethod.POST)
    public String runTipCalculator(HttpServletRequest request, Model model) {

        double baseAmount = Double.parseDouble(request.getParameter("baseAmount"));
        String tipPercentage = request.getParameter("tipPercentage");
        int tipPercentageInt = 0;
        double tipAmount = 0;
        double totalAmount = 0;
        DecimalFormat df = new DecimalFormat("####0.00");

        switch (tipPercentage) {
            case "fifteen":
                tipAmount = baseAmount * 0.15;
                totalAmount = baseAmount * 1.15;
                tipPercentageInt = 15;
                break;
            case "eighteen":
                tipAmount = baseAmount * 0.18;
                totalAmount = baseAmount * 1.18;
                tipPercentageInt = 18;
                break;
            case "twenty":
                tipAmount = baseAmount * 0.2;
                totalAmount = baseAmount * 1.2;
                tipPercentageInt = 20;
                break;
        }

        model.addAttribute("baseAmount", df.format(baseAmount));
        model.addAttribute("tipPercentageInt", tipPercentageInt);
        model.addAttribute("tipAmount", df.format(tipAmount));
        model.addAttribute("totalAmount", df.format(totalAmount));

        return "tipcalculator";
    }
}
