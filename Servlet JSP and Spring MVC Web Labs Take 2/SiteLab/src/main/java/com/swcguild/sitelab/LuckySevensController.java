package com.swcguild.sitelab;

import java.util.Map;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "luckysevenscontroller")
public class LuckySevensController {

    public LuckySevensController() {
    }

    @RequestMapping(value = "/getluckysevens", method = RequestMethod.GET)
    public String getLuckySevens(Map<String, Object> model) {
        model.put("luckysevenscontrollermessage", "This is a message that was put into the model from LuckySevensController.");
        return "luckysevens";
    }

    @RequestMapping(value = "/runluckysevens", method = RequestMethod.POST)
    public String runLuckySevens(HttpServletRequest request, Model model) {

        int bankroll = Integer.parseInt(request.getParameter("startingBankroll"));
        int rollCount = 0;
        int rollCountAtMax = 0;
        int maxBankroll = bankroll;

        while (bankroll >= 1) {

            Random r = new Random();
            int d1 = r.nextInt(6) + 1;
            int d2 = r.nextInt(6) + 1;
            rollCount = rollCount + 1;
            int diceTotal = d1 + d2;

            if (diceTotal == 7) {
                bankroll = bankroll + 4;
            } else {
                bankroll = bankroll - 1;
            }
            if (bankroll > maxBankroll) {
                maxBankroll = bankroll;
                rollCountAtMax = rollCount;
            }
        }

        model.addAttribute("rollCount", rollCount);
        model.addAttribute("maxBankroll", maxBankroll);
        model.addAttribute("rollCountAtMax", rollCountAtMax);

        return "luckysevens";
    }

}
