package com.swcguild.sitelab;

import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/flooringcalculatorcontroller"})
public class FlooringCalculatorController {

    public FlooringCalculatorController() {

    }

    @RequestMapping(value = "/getflooringcalculator", method = RequestMethod.GET)
    public String getFlooringCalculator(Map<String, Object> model) {
        return "flooringcalculator";
    }

    @RequestMapping(value = "/runflooringcalculator", method = RequestMethod.POST)
    public String runFlooringCalculator(HttpServletRequest request, Model model) {

        String widthString = request.getParameter("width");
        String lengthString = request.getParameter("length");
        String costString = request.getParameter("costSqFt");

        if (widthString == null || lengthString == null || costString == null) {
            request.setAttribute("badInput", true);
        } else {
            try {
                double width = Double.parseDouble(widthString);
                double length = Double.parseDouble(lengthString);
                double costSqFt = Double.parseDouble(costString);
                if (width <= 0 || length <= 0 || costSqFt <= 0) {
                    request.setAttribute("badInput", true);
                } else {
                    FlooringCalculator calc = new FlooringCalculator(width, length, costSqFt);
                    model.addAttribute("calculations", calc);
                    model.addAttribute("width", width);
                    model.addAttribute("length", length);
                    model.addAttribute("costSqFt", costSqFt);
                    model.addAttribute("laborTime", calc.getLaborTime());
                    model.addAttribute("laborCost", calc.getLaborCost());
                    model.addAttribute("flooringCost", calc.getFlooringCost());
                }

            } catch (NumberFormatException ex) {
                request.setAttribute("badInput", true);
            }
        }
        return "flooringcalculator";
    }
}
