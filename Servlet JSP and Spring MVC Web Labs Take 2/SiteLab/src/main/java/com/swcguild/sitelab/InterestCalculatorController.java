package com.swcguild.sitelab;

import java.text.NumberFormat;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/interestcalculatorcontroller"})
public class InterestCalculatorController {

    public InterestCalculatorController() {

    }

    @RequestMapping(value = "/getinterestcalculator", method = RequestMethod.GET)
    public String getInterestCalculator(Map<String, Object> model) {
        return "interestcalculator";
    }

    @RequestMapping(value = "/runinterestcalculator", method = RequestMethod.POST)
    public String runInterestCalculator(HttpServletRequest request, Model model) {

        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        double intRateYearly = Double.parseDouble(request.getParameter("rate"));
        double beginningPrincipal = Double.parseDouble(request.getParameter("principal"));
        double numYears = Double.parseDouble(request.getParameter("years"));
        double intEarned = 0;
        double intRateQuarterly = (intRateYearly / 4);
        double currentBal = beginningPrincipal;
        double prevBal = currentBal;

        for (int i = 1; i <= numYears * 4; i++) {
            currentBal = currentBal * (1 + (intRateQuarterly / 100));
            intEarned = currentBal - prevBal;
        }

        model.addAttribute("beginningPrincipal", nf.format(beginningPrincipal));
        model.addAttribute("interestRate", nf.format(intRateYearly));
        model.addAttribute("numYears", nf.format(numYears));
        model.addAttribute("currentBalance", nf.format(currentBal));
        model.addAttribute("interestEarned", nf.format(intEarned));

        return "interestcalculator";
    }

}
