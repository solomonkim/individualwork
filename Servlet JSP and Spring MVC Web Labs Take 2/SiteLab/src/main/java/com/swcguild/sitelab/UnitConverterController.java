package com.swcguild.sitelab;

import java.text.DecimalFormat;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/unitconvertercontroller"})
public class UnitConverterController {

    public UnitConverterController() {

    }

    @RequestMapping(value = "/getunitconverter", method = RequestMethod.GET)
    public String getUnitConverter(Map<String, Object> model) {
        return "unitconverter";
    }

    @RequestMapping(value = "/rununitconverter", method = RequestMethod.POST)
    public String runUnitConverter(HttpServletRequest request, Model model) {

        if (request.getParameter("degrees") != null && !request.getParameter("degrees").equals("")
                && request.getParameter("units") != null && !request.getParameter("units").equals("")) {
            double originalDegrees = Double.parseDouble(request.getParameter("degrees"));
            String originalUnits = request.getParameter("units");
            double convertedDegrees = 0;
            String convertedUnits = "";
            DecimalFormat df = new DecimalFormat("####0.0");

            if (originalUnits.equals("C")) {
                convertedUnits = "F";
                convertedDegrees = (originalDegrees * 1.8) + 32;
            }

            if (originalUnits.equals("F")) {
                convertedUnits = "C";
                convertedDegrees = (originalDegrees - 32) / 1.8;
            }

            model.addAttribute("convertedDegrees", df.format(convertedDegrees));//these names have to match the ones on the jsp
            model.addAttribute("convertedUnits", convertedUnits);
            model.addAttribute("originalDegrees", df.format(originalDegrees));
            model.addAttribute("originalUnits", originalUnits);

        }

        return "unitconverter";
    }
}
