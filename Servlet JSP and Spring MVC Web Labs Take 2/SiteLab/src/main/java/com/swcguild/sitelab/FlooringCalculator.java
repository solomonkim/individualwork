/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.sitelab;

/**
 *
 * @author apprentice
 */
public class FlooringCalculator {

    private double width, length, costSqFt, flooringCost, laborCost, laborTime, areaSqFt;
    private final double LABOR_COST_PER_QUARTER_HOUR = 86 / 4;

    public FlooringCalculator(double width, double length, double costSqFt) {
        this.width = width;
        this.length = length;
        this.costSqFt = costSqFt;
        this.areaSqFt = width * length;
        this.flooringCost = areaSqFt * costSqFt;
        double leftOverFt = areaSqFt % 5;
        double laborTime = (areaSqFt - leftOverFt) / 5;
        if (leftOverFt > 0) {
            laborTime = laborTime + 1;
        }
        laborCost = laborTime * LABOR_COST_PER_QUARTER_HOUR;

    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public double getCostSqFt() {
        return costSqFt;
    }

    public double getFlooringCost() {
        return flooringCost;
    }

    public double getLaborCost() {
        return laborCost;
    }

    public double getLaborTime() {
        return laborTime;
    }

    public double getAreaSqFt() {
        return areaSqFt;
    }

    public double getLABOR_COST_PER_QUARTER_HOUR() {
        return LABOR_COST_PER_QUARTER_HOUR;
    }
}
