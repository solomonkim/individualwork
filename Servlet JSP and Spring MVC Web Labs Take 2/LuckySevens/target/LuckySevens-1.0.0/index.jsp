<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Sevens</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
    <center>
        <div class="container">
            <h1>Lucky Sevens</h1>
            <hr />
            <p>Enter the amount of money you want to start with. The program will roll
                the dice. If a 7 is rolled, you will win $4. If any other number is 
                rolled, you will lose $1. This will repeat until you are out of money 
                then give you a summary of the results. Good luck!</p>
            <form action="LuckySevens" method="POST">
                <!-- the value in action matches the value in "urlPatterns" in LuckySevensServlet.java -->
                How much money do you want to start with?
                <br /><br />
                <input type="number" min="1" name="startingBankroll">
                <br /><br />
                <button type="submit" class="btn btn-info">Roll 'em!</button>
            </form>
        </div>
    </center>
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
</body>

</html>
