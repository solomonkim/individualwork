<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Sevens</title>
    </head>

    <body>
    <center>
        <h1>Lucky Sevens Results</h1>
        <p>You rolled the dice <c:out value="${rollCount}" /> times.</p>
        <!-- The prefix "c" initiates the JSTL tags that are enabled by the tag 
        lib referenced in line 1. "$" means "Hey JSTL, do/get the stuff in the "{}"
        which are referenced in the request that was sent by the servlet.-->
        <p>The best time to quit would have been after <c:out value="${rollCountAtMax}" /> 
            rolls, when you had $<c:out value="${maxBankroll}" />.</p>
    </center>
</body>

</html>
