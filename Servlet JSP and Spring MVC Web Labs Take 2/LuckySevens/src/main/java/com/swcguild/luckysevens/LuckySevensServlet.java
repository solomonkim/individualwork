package com.swcguild.luckysevens;

import java.io.IOException;
import java.util.Random;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "LuckySevensServlet", urlPatterns = {"/LuckySevens"})
public class LuckySevensServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int rollCount = 0;
        int rollCountAtMax = 0;
        int bankroll = Integer.parseInt(request.getParameter("startingBankroll"));
        int maxBankroll = bankroll;

        while (bankroll >= 1) {

            Random r = new Random();
            int d1 = r.nextInt(6) + 1;
            int d2 = r.nextInt(6) + 1;
            rollCount = rollCount + 1;
            int diceTotal = d1 + d2;

            if (diceTotal == 7) {
                bankroll = bankroll + 4;
            } else {
                bankroll = bankroll - 1;
            }
            if (bankroll > maxBankroll) {
                maxBankroll = bankroll;
                rollCountAtMax = rollCount;
            }
        }

        request.setAttribute("rollCount", rollCount);
        request.setAttribute("maxBankroll", maxBankroll);
        request.setAttribute("rollCountAtMax", rollCountAtMax);
        request.getRequestDispatcher("response.jsp").forward(request, response);
        // Use the setAttribute method to declare fields in the response named in
        // quotations marks and set them equal to the variables named after the 
        // comma. The getRequestDispatcher.forward method sends the request and 
        // response to the JSP file specified in quotes ("response.jsp").

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}
