<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>
    <body>
    <center>

        <div class="container">

            <h1>Dictionary</h1>
            <hr />
            <div class="col-offset-1 col-md-5" id="newSubmission">
                <h2>add a word</h2>
                <form action="[SOMETHING]" method="[SOMETHING]">
                    <table>
                        <tr>
                            <td align="right">word: </td>
                            <td><input type="text" name="word"></td>
                        </tr>
                        <tr>
                            <td align="right">definition: </td>
                            <td><input type="text" name="definition"></td>
                        </tr>
                    </table>
                    <input type="submit" value="submit">
                </form>
            </div>

            <div class="col-offset-1 col-md-4" id="list">
                <h2>word list</h2>
            </div>

        </div>

    </center>

    <!-- Placed at the end of the document so the pages load faster -->
    <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

</body>
</html>

