package blackjack;

public class Blackjack {

    public static void main(String[] args) {

        Card[] deck = new Card[52];

        for (int i = 0; i < deck.length; i++) {

            deck[i] = new Card();

            //set label and value
            switch (i % 13) {
                case 0:
                    deck[i].setLabel('A');
                    deck[i].setValue(1);
                    break;
                case 1:
                    deck[i].setLabel('2');
                    deck[i].setValue(2);
                    break;
                case 2:
                    deck[i].setLabel('3');
                    deck[i].setValue(3);
                    break;
                case 3:
                    deck[i].setLabel('4');
                    deck[i].setValue(4);
                    break;
                case 4:
                    deck[i].setLabel('5');
                    deck[i].setValue(5);
                    break;
                case 5:
                    deck[i].setLabel('6');
                    deck[i].setValue(6);
                    break;
                case 6:
                    deck[i].setLabel('7');
                    deck[i].setValue(7);
                    break;
                case 7:
                    deck[i].setLabel('8');
                    deck[i].setValue(8);
                    break;
                case 8:
                    deck[i].setLabel('9');
                    deck[i].setValue(0);
                    break;
                case 9:
                    deck[i].setLabel('T');
                    deck[i].setValue(10);
                    break;
                case 10:
                    deck[i].setLabel('J');
                    deck[i].setValue(10);
                    break;
                case 11:
                    deck[i].setLabel('Q');
                    deck[i].setValue(10);
                    break;
                default:
                    deck[i].setLabel('K');
                    deck[i].setValue(10);
                    break;

            }

            // set suit
            if (i < 13) {
                deck[i].setSuit('s');
            } else if (i < 26) {
                deck[i].setSuit('h');
            } else if (i < 39) {
                deck[i].setSuit('c');
            } else {
                deck[i].setSuit('d');
            }

            System.out.println(deck[i].getLabel() + "" + deck[i].getSuit());
        }
    }
}
