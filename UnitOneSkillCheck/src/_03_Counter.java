public class _03_Counter {

    public static void main(String[] args) {
        toTen(3);
        toTen(8);
        toTen(200);
    }

    public static void toTen(int meaninglessNumber) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(i);
        }
    }
}
