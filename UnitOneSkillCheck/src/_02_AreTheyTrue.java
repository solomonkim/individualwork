public class _02_AreTheyTrue {
    public static void main(String[] args) {
        boolean x = true;
        boolean y = true;
        System.out.println(howTrue(x,y));
        System.out.println(howTrue(!x,!y));
        System.out.println(howTrue(x,!y));
        System.out.println(howTrue(!x,y));
    }
    
    public static String howTrue (boolean x, boolean y) {
        String truthiness = "error";
            if(x && y)
                truthiness = "both";
            if((x || y) && !(x && y))
                truthiness = "only one";
            if (!x && !y)
                truthiness = "neither";
        return truthiness;
    }
}
