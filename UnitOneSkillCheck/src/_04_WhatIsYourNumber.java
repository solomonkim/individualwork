
import java.util.Scanner;

public class _04_WhatIsYourNumber {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter an integer.");
        int x = sc.nextInt();
        for (int i = 0; i <= x; i++) {
            System.out.println(i);
        }
        System.out.println("Thanks for playing!");
    }
}
