package rpsrefactor;

import java.util.Random;
import java.util.Scanner;

public class RPSRefactor {

    static Scanner sc = new Scanner(System.in);
    static Random r = new Random();

    public static void main(String[] args) {
        System.out.println(printResults(findWinner(getCompChoice(), getPlayerChoice()))); //Print the return of the findWinner method call (a string), passing in the getCompChoice method call and the getPlayerChoice method call...Yo dawg...

    }

    public static int getCompChoice() {
        return (1 + r.nextInt(3));
    }

    public static int getPlayerChoice() {
        System.out.println("1. Rock\n2. Paper\n3. Scissors\nChoose 1/2/3...");
        return sc.nextInt();
    }

    public static int findWinner(int cChoice, int pChoice) {
        String c, p;

        switch (cChoice) {
            case 1:
                c = "rock";
                break;
            case 2:
                c = "paper";
                break;
            default:
                c = "scissors";
                break;
        }
        switch (pChoice) {
            case 1:
                p = "rock";
                break;
            case 2:
                p = "paper";
                break;
            default:
                p = "scissors";
                break;
        }
        System.out.println("You chose " + p + " and I chose " + c + "... ");
        if (cChoice == pChoice) {
            return 0;
        } else if ((cChoice == 1) && (pChoice == 3) || (cChoice == 2) && (pChoice == 1) || (cChoice == 3) && (pChoice == 2)) {
            return 1;
        } else {
            return 2;
        }
    }

    public static String printResults(int winnerCode) {
        switch (winnerCode) {
            case 0:
                return "It's a tie!";
            case 1:
                return "You lose!";
            default:
                return "You win!";
        }
    }
}
