<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Sevens</title>
    </head>
    <body>
    <center>
        <h1>Lucky Sevens</h1>
        <p>Enter a starting bankroll. The program will roll the dice. If you roll
            a 7, you will win $4. Roll any other number and lose $1. Good luck!</p>
        <form action="LuckySevens" method="POST">
            starting bankroll: <input type="number" min="1" name="startingBankroll">
            <br />
            <input type="submit" value="Submit">
        </form>
    </center>
</body>
</html>
