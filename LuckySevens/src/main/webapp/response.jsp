<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Lucky Sevens</title>
    </head>
    <body>
    <center>
        <h1>Lucky Sevens</h1>
        <h3>Results</h3>
        <p>You rolled the dice <c:out value="${rollCount}" default="n/a" /> times.</p>
        <p>You should have quit after <c:out value="${rollCountAtMax}" default="n/a" /> rolls, when you had $<c:out value="${maxBankroll}" />.</p>
    </center>
</body>
</html>