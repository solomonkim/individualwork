package com.swcguild.tipcalculator;

import java.io.IOException;
import java.text.DecimalFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "TipCalculatorServlet", urlPatterns = {"/TipCalculator"})
public class TipCalculatorServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        double baseAmount = Double.parseDouble(request.getParameter("baseAmount"));
        String tipPercentage = request.getParameter("tipPercentage");
        int tipPercentageInt = 0;
        double tipAmount = 0;
        double totalAmount = 0;
        DecimalFormat df = new DecimalFormat("####0.00");

        switch (tipPercentage) {
            case "fifteen":
                tipAmount = baseAmount * 0.15;
                totalAmount = baseAmount * 1.15;
                tipPercentageInt = 15;
                break;
            case "eighteen":
                tipAmount = baseAmount * 0.18;
                totalAmount = baseAmount * 1.18;
                tipPercentageInt = 18;
                break;
            case "twenty":
                tipAmount = baseAmount * 0.2;
                totalAmount = baseAmount * 1.2;
                tipPercentageInt = 20;
                break;
        }
        request.setAttribute("baseAmount", df.format(baseAmount));
        request.setAttribute("tipPercentageInt", tipPercentageInt);
        request.setAttribute("tipAmount", df.format(tipAmount));
        request.setAttribute("totalAmount", df.format(totalAmount));
        request.getRequestDispatcher("response.jsp").forward(request, response);
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
