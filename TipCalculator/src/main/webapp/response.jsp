<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tip Calculator</title>
    </head>
    <body>
    <center>
        <h1>Tip Calculation</h1>
        <table>
            <tr>
                <td>base amount</td>
                <td align="right"><c:out value="${baseAmount}" default="n/a" /></td>
            </tr>

            <tr>
                <td>tip (<c:out value="${tipPercentageInt}" default = "n/a"/>%)</td>
                <td align="right"><c:out value="${tipAmount}" default="n/a" /></td>
            </tr>
            <tr>
                <td><b>total</b></td>
                <td align="right"><b><c:out value="${totalAmount}" default="n/a" /></b></td>
            </tr>
        </table>
    </center>
</body>
</html>
