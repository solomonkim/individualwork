<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tip Calculator</title>
    </head>
    <body>
    <center>
        <h1>Tip Calculator</h1>
        <form action="TipCalculator" method="POST">
            <table>
                <tr>
                    <td>base amount </td>
                    <td><input type="number" min="0" step="0.01" name="baseAmount"></td>
                </tr>
                <tr>
                    <td>tip percentage </td>
                    <td>
                        15% <input type="radio" name="tipPercentage" value="fifteen">
                        <br />
                        18% <input type="radio" name="tipPercentage" value="eighteen">
                        <br />
                        20% <input type="radio" name="tipPercentage" value="twenty">
                    </td>
                </tr>
            </table> 
            <br />
            <input type="submit" value="Submit">
        </form>
    </center>
</body>
</html>
