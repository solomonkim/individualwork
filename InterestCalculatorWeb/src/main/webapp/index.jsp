<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator</title>
    </head>
    <body>
    <center>
        <h1>Interest Calculator</h1>
        <form action="InterestCalculatorWeb" method="POST">
            <table>
                <tr>
                    <td>starting principal</td>
                    <td><input type="number" step=".01" min="0" name="principal"></td>
                </tr>
                <tr>
                    <td>yearly interest rate (0 - 100)</td>
                    <td><input type="number" step=".01" min="0" max="100" name="rate"></td>
                </tr>
                <tr>
                    <td>period (years)</td>
                    <td><input type="number" step=".01" min="0" name="years"></td>
                </tr>
            </table>
            <input type="submit" value="Submit">
        </form>
    </center>
</body>
</html>
