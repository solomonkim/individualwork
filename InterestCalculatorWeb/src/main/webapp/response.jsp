<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator</title>
    </head>
    <body>
    <center>
        <h1>Interest Calculator Results</h1>
        <table>
            <tr>
                <td>starting principal</td>
                <td><b>$<c:out value="${beginningPrincipal}" /></b></td>
            </tr>
            <tr>
                <td>annual interest rate (compounded quarterly)</td>
                <td><b><c:out value="${interestRate}" />%</b></td>
            </tr>
            <tr>
                <td>period</td>
                <td><b><c:out value="${numYears}" /> years</b></td>
            </tr>
            <tr>
                <td>ending balance</td>
                <td><b>$<c:out value="${currentBalance}" /></b></td>
            </tr>
            <tr>
                <td>interest earned</td>
                <td><b>$<c:out value="${interestEarned}" /></b></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
            </tr>
        </table>
    </center>
</body>
</html>
