package com.swcguild.interestcalculatorweb;

import java.io.IOException;
import java.text.NumberFormat;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "InterestCalculatorWebServlet", urlPatterns = {"/InterestCalculatorWeb"})
public class InterestCalculatorWebServlet extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        double intRateYearly = Double.parseDouble(request.getParameter("rate"));
        double beginningPrincipal = Double.parseDouble(request.getParameter("principal"));
        double numYears = Double.parseDouble(request.getParameter("years"));
        double intEarned = 0;
        double intRateQuarterly = (intRateYearly / 4);
        double currentBal = beginningPrincipal;
        double prevBal = currentBal;

        for (int i = 1; i <= numYears * 4; i++) {
            currentBal = currentBal * (1 + (intRateQuarterly / 100));
            intEarned = currentBal - prevBal;
        }

        request.setAttribute("beginningPrincipal", nf.format(beginningPrincipal));
        request.setAttribute("interestRate", nf.format(intRateYearly));
        request.setAttribute("numYears", nf.format(numYears));
        request.setAttribute("currentBalance", nf.format(currentBal));
        request.setAttribute("interestEarned", nf.format(intEarned));
        request.getRequestDispatcher("response.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
