/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package unitconversion.controller;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class UnitConversionController {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Choose the type of units you want to convert...");
        System.out.println("1. temperature");
        System.out.println("2. currency");
        System.out.println("3. volume");
        System.out.println("4. mass");
        System.out.println("5. exit");

        switch (sc.nextInt()) {
            case 1:
                convertTemperature();
                break;
            case 2:
                convertCurrency();
                break;
            case 3:
                convertVolume();
                break;
            case 4:
                convertMass();
                break;
            case 5:
                break;
        }
    }

}
