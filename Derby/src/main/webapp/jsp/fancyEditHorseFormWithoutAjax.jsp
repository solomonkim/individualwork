<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Solomon's Amazing Derby App</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <h1>Solomon's Amazing Derby App</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">Search</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">Stats</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displayHorseListNoAjax">Horse List(No Ajax)</a></li>
                </ul>    
            </div>

            <sf:form modelAttribute="horseToEdit"
                     class="form-horizontal"
                     role="form"
                     action="editFancyHorseNoAjaxWithValidation"
                     method="POST">

                <div class="form-group">
                    <label for="edit-name"
                           class="col-md-4 control-label">Name:</label>
                    <div class="col-md-8">
                        <sf:input type="text"
                                  class="form-control"
                                  id="edit-name"
                                  path="name"
                                  placeholder="name" />
                        <sf:errors path="name"
                                   cssClass="error"
                                   cssStyle="color:red">

                        </sf:errors>
                    </div>
                </div>

                <div class="form-group">
                    <label for="edit-jockey"
                           class="col-md-4 control-label">Jockey:</label>
                    <div class="col-md-8">
                        <sf:input type="text"
                                  class="form-control"
                                  id="edit-jockey"
                                  path="jockey"
                                  placeholder="jockey" />
                        <sf:errors path="jockey"
                                   cssClass="error"
                                   cssStyle="color:red">

                        </sf:errors>
                    </div>
                </div>    

                <div class="form-group">
                    <label for="edit-trainer"
                           class="col-md-4 control-label">Trainer:</label>
                    <div class="col-md-8">
                        <sf:input type="text"
                                  class="form-control"
                                  id="edit-trainer"
                                  path="trainer"
                                  placeholder="trainer" />
                        <sf:errors path="trainer"
                                   cssClass="error"
                                   cssStyle="color:red">

                        </sf:errors>
                    </div>
                </div>    

                <div class="form-group">
                    <label for="edit-owner"
                           class="col-md-4 control-label">Owner:</label>
                    <div class="col-md-8">
                        <sf:input type="text"
                                  class="form-control"
                                  id="edit-owner"
                                  path="owner"
                                  placeholder="owner" />
                        <sf:errors path="owner"
                                   cssClass="error"
                                   cssStyle="color:red">

                        </sf:errors>
                    </div>
                </div>


                <div class="form-group">
                    <sf:hidden path="id" />
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit"
                                id="edit-button"
                                class="btn btn-default">Save Changes
                        </button>
                    </div>
                </div>
            </sf:form>  

        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>