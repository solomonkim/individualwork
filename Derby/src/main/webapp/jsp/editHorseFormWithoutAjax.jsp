<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Solomon's Amazing Derby App</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <h1>Solomon's Amazing Derby App</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/displayContactListNoAjax">Contact List(No Ajax)</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/fancyNewHorseFormNoAjax">Fancy New Horse Form (No Ajax)</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/search">(Another Tab)</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/stats">(Yet Another Tab)</a></li>
                </ul>    
            </div>

            <sf:form modelAttribute="horseToEdit" action="editHorseNoAjax" method="POST">
                <sf:input type="text" path="name" />
                <sf:input type="text" path="jockey"/>
                <sf:input type="text" path="trainer" />
                <sf:input type="text" path="owner" />
                <sf:hidden path="id" />
                <button type="submit">Save Changes</button>
            </sf:form>   


        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>

