<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <title>New Horse Registration</title>
    </head>

    <body>
        <div class="container">
            <h1>New Horse</h1>
            <hr />
            <form class="form-horizontal" role="form" action="addHorse" method="POST">

                <div class="form-group">
                    <label for="add-name" class=col-md-4 control-label>name: </label>
                    <!--    for: the element that this label is bound to
                            col-md-4: in Bootstrap, creates a medium-sized column (4 wide)
                    -->
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-name" name="name" placeholder="name" />
                        <!--    form-control: all elements with the form-control class have a width of 100%
                                placeholder: the text that shows up grayed out in the blank input field
                        -->
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-jockey" class="col-md-4 control-label">jockey: </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-jockey" name="jockey" placeholder="jockey" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-owner" class="col-md-4 control-label">owner: </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-owner" name="owner" placeholder="owner" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-trainer" class="col-md-4 control-label">trainer: </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" id="add-trainer" name="trainer" placeholder="trainer" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" id="add-button" class="btn btn-default">add horse</button>
                    </div>
                </div>

            </form>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
