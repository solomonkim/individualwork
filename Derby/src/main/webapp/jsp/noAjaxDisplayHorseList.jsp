<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Solomon's Amazing Derby App</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    </head>

    <body>
        <div class="container">
            <h1>Solomon's Amazing Derby App</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayHorseListNoAjax">Horse List(No Ajax)</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/fancyNewHorseFormNoAjax">Fancy New Horse Form (No Ajax)</a></li>
                </ul>    
            </div>

            <c:if test="${fromSearch}">
                Here are the results of your search... <br/><br/>
                <c:if test="${listSize < 1}">
                    No results found. Please try search again.<br/>
                </c:if>
            </c:if>

            <c:forEach var="horse" items="${horseList}">
                <s:url value="deleteHorseNoAjax" var="deleteHorse_url">
                    <s:param name="id" value="${horse.id}" />
                </s:url>
                <s:url value="editFancyHorseNoAjaxWithValidation" var="editHorse_url">
                    <s:param name="id" value="${Horse.id}" />
                </s:url>

                Name: ${horse.name} | 
                <a href="${deleteHorse_url}">Delete</a> | 
                <a href="${editHorse_url}">Edit</a>
                <br/>
                Jockey: ${horse.jockey}
                <br/>
                Trainer: ${horse.trainer}
                <br/>
                Owner: ${horse.owner}
                <br/><br/>
                <hr/>
            </c:forEach>

        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>