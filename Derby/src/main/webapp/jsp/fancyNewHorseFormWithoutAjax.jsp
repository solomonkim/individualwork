<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Solomon's Amazing Derby App</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <h1>Solomon's Amazing Derby App</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/fancyNewHorseFormNoAjax">Fancy New Horse Form (No Ajax)</a></li>

                </ul>    
            </div>

            <form class="form-horizontal"
                  role="form"
                  action="addHorseNoAjax"
                  method="POST">

                <div class="form-group">
                    <label for="add-name"
                           class="col-md-4 control-label">Name:
                    </label>
                    <div class="col-md-8">
                        <input type="text"
                               class="form-control"
                               id="add-first-name"
                               name="name"
                               placeholder="Name" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-jockey"
                           class="col-md-4 control-label">Jockey:
                    </label>
                    <div class="col-md-8">
                        <input type="text"
                               class="form-control"
                               id="add-jockey"
                               name="jockey"
                               placeholder="Jockey" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-trainer"
                           class="col-md-4 control-label">Trainer:
                    </label>
                    <div class="col-md-8">
                        <input type="text"
                               class="form-control"
                               id="add-trainer"
                               name="trainer"
                               placeholder="Trainer" />
                    </div>
                </div>

                <div class="form-group">
                    <label for="add-owner"
                           class="col-md-4 control-label">Owner:
                    </label>
                    <div class="col-md-8">
                        <input type="text"
                               class="form-control"
                               id="add-owner"
                               name="owner"
                               placeholder="Owner" />
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit"
                                id="edit-button"
                                class="btn btn-default">Add</button>
                    </div>
                </div>

            </form>    


        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>