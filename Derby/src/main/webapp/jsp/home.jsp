<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

    <head>
        <title>Solomon's Amazing Derby App</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
    </head>

    <body>
        <div class="container">
            <h1>2016 Kentucky Derby Contenders</h1>
            <hr/>
            <table id="contenders" class="table table-striped">
                <tr>
                    <th width="60%">horse (owner)</th>
                    <th width="20%">trainer</th>
                    <th width="20%">jockey</th>
                </tr>
                <tbody id="contentRows"></tbody>
            </table>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>

</html>