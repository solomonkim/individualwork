<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    
    <head>
        
        <title>Solomon's Amazing Derby App</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        
    </head>
    
    <body>
        
        <div class="container">
            <h1>Solomon's Amazing Derby App</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation"><a href="${pageContext.request.contextPath}/">Home</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/displayContactListNoAjax">Contact List(No Ajax)</a></li>
                    <li role="presentation" class="active"><a href="${pageContext.request.contextPath}/newContactFormNoAjax">Simple New Contact Form(No Ajax)</a></li>
                    <li role="presentation"><a href="${pageContext.request.contextPath}/fancyNewContactFormNoAjax">Fancy New Contact Form(No Ajax)</a></li>
                </ul>    
            </div>

            <form action="searchWithoutAjax" method="POST">
                <input type="text" name="name" />
                <input type="text" name="jockey"/>
                <input type="text" name="trainer" />
                <input type="text" name="owner" />
                <button type="submit">Search</button>
            </form>
        </div>
                
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
    
</html>

