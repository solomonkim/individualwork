$(document).ready(function () {
    loadContenders();
});

function loadContenders() {
    var tableBody = $('#contentRows');
    tableBody.empty();

    $.ajax({
        url: "contenders",
        type: "GET",
        headers: {
            'Accept': 'application/json'
        }
    }).success(function (data, status) {
        fillTable(data, status);
    });
}

function fillTable(contendersList) {
    var tableBody = $('#contentRows');
    tableBody.empty();
    $.each(contendersList, function (horse) {

        tableBody.append($('<tr>'
                .append($('<td>').text('horse.name'))
                .append($('<td>').text('horse.trainer'))
                .append($('<td>').text('horse.jockey')))
                );
    });

}