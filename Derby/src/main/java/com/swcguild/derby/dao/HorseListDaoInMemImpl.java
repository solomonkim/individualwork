package com.swcguild.derby.dao;

import com.swcguild.derby.model.Horse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class HorseListDaoInMemImpl implements HorseListDao {

    private Map<Integer, Horse> horseMap = new HashMap<>();
    private static int idCounter = 0;

    public HorseListDaoInMemImpl() {

        Horse horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Gun Runner");
        horse.setOwner("Winchell Thoroughbreds, LLC");
        horse.setTrainer("Steven M. Asmussen");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Nyquist");
        horse.setOwner("Reddam Racing, LLC");
        horse.setTrainer("Doug F. O'Neill");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Lani");
        horse.setOwner("(not specified)");
        horse.setTrainer("Mikio Matsunaga");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Mohaymen");
        horse.setOwner("Shadwell Stable");
        horse.setTrainer("Kiaran P. McLaughlin");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Destin");
        horse.setOwner("Twin Creeks Racing Stables, LLC");
        horse.setTrainer("Todd A. Pletcher");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Cupid");
        horse.setOwner("Let's Go Stable");
        horse.setTrainer("Bob Baffert");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Danzing Candy");
        horse.setOwner("Halo Farms");
        horse.setTrainer("Clifford W. Sise, Jr.");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Shagaf");
        horse.setOwner("Shadwell Stable");
        horse.setTrainer("Chad C. Brown");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Mor Spirit");
        horse.setOwner("Michael Lund Petersen");
        horse.setTrainer("Bob Baffert");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Tom's Ready");
        horse.setOwner("GMB Racing");
        horse.setTrainer("Dallas Stewart");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Majesto");
        horse.setOwner("Grupo 7C Racing Stable");
        horse.setTrainer("Gustavo Delgado");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Mo Tom");
        horse.setOwner("GMB Racing");
        horse.setTrainer("Thomas M. Amoss");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Fellowship");
        horse.setOwner("Jacks or Better Farm, Inc.");
        horse.setTrainer("Stanley I. Gold");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Exaggerator");
        horse.setOwner("Big Chief Racing, LLC");
        horse.setTrainer("J. Keith Desormeaux");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Whitmore");
        horse.setOwner("Southern Springs Stables");
        horse.setTrainer("Ron Moquett");
        this.addHorse(horse);

        horse = new Horse();
        horse.setJockey("TBA");
        horse.setName("Laoban");
        horse.setOwner("McCormick Racing, LLC");
        horse.setTrainer("Eric J. Guillot");
        this.addHorse(horse);

    }

    @Override
    public Horse addHorse(Horse horse) {
        horse.setId(idCounter);
        idCounter++;
        horseMap.put(horse.getId(), horse);
        return horse;
    }

    @Override
    public void removeHorse(int id) {
        horseMap.remove(id);
    }

    @Override
    public void updateHorse(Horse horse) {
        horseMap.put(horse.getId(), horse);
    }

    @Override
    public List<Horse> getAllHorses() {
        return new ArrayList(horseMap.values());
    }

    @Override
    public Horse getHorseById(int id) {
        return horseMap.get(id);
    }

    @Override
    public List<Horse> searchHorses(Map<SearchTerm, String> criteria) {
        String jockeyCriteria = criteria.get(SearchTerm.JOCKEY);
        String nameCriteria = criteria.get(SearchTerm.NAME);
        String ownerCriteria = criteria.get(SearchTerm.OWNER);
        String trainerCriteria = criteria.get(SearchTerm.TRAINER);
        Predicate<Horse> jockeyPred;
        Predicate<Horse> namePred;
        Predicate<Horse> ownerPred;
        Predicate<Horse> trainerPred;
        Predicate<Horse> allPass = (horse) -> {
            return true;
        };

        jockeyPred = (jockeyCriteria == null || jockeyCriteria.isEmpty())
                ? allPass : (horse) -> horse.getJockey().toLowerCase().contains(jockeyCriteria.toLowerCase());
        namePred = (nameCriteria == null || nameCriteria.isEmpty())
                ? allPass : (horse) -> horse.getName().toLowerCase().contains(nameCriteria.toLowerCase());
        ownerPred = (ownerCriteria == null || ownerCriteria.isEmpty())
                ? allPass : (horse) -> horse.getOwner().toLowerCase().contains(ownerCriteria.toLowerCase());
        trainerPred = (trainerCriteria == null || trainerCriteria.isEmpty())
                ? allPass : (horse) -> horse.getTrainer().toLowerCase().contains(trainerCriteria.toLowerCase());

        return horseMap.values().stream()
                .filter(jockeyPred)
                .filter(namePred)
                .filter(ownerPred)
                .filter(trainerPred)
                .collect(Collectors.toList());

    }

    @Override
    public List<Horse> searchHorses(Predicate<Horse> filter) {
        return horseMap.values().stream()
                .filter(filter)
                .collect(Collectors.toList());
    }

}