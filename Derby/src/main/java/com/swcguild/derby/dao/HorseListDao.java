package com.swcguild.derby.dao;

import com.swcguild.derby.model.Horse;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

public interface HorseListDao {

    public Horse addHorse(Horse horse);

    public void removeHorse(int id);

    public void updateHorse(Horse horse);

    public List<Horse> getAllHorses();

    public Horse getHorseById(int id);

    public List<Horse> searchHorses(Map<SearchTerm, String> criteria);

    public List<Horse> searchHorses(Predicate<Horse> filter);

}
