package com.swcguild.derby.controller;

import com.swcguild.derby.dao.HorseListDao;
import com.swcguild.derby.dao.SearchTerm;
import com.swcguild.derby.model.Horse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeControllerNoAjax {

    private HorseListDao dao;

    @Inject
    public HomeControllerNoAjax(HorseListDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/displayHorseList", method = RequestMethod.GET)
    public String displayHorseList(Model model) {
        List<Horse> allHorses = dao.getAllHorses();
        model.addAttribute("horseList", allHorses);

        return "displayHorseList";
    }

//    @RequestMapping(value = "/displayHorseListNoAjax", method = RequestMethod.GET)
//    public String displayHorseList(Model model) {
//        List<Horse> allHorses = dao.getAllHorses();
//        model.addAttribute("horseList", allHorses);
//
//        return "noAjaxDisplayHorseList";
//    }
    
    @RequestMapping(value = "/displaySearchFormNoAjax", method = RequestMethod.GET)
    public String displaySearchFormWithoutAjax(Model model) {
        return "searchFormWithoutAjax";
    }

    @RequestMapping(value = "/searchWithoutAjax", method = RequestMethod.POST)
    public String searchFormWithoutAjax(Model model, HttpServletRequest req) {
        String name = req.getParameter("name");
        String jockey = req.getParameter("jockey");
        String trainer = req.getParameter("trainer");
        String owner = req.getParameter("owner");

        Map<SearchTerm, String> criteriaForSearch = new HashMap<>();
        criteriaForSearch.put(SearchTerm.NAME, name);
        criteriaForSearch.put(SearchTerm.JOCKEY, jockey);
        criteriaForSearch.put(SearchTerm.TRAINER, trainer);
        criteriaForSearch.put(SearchTerm.OWNER, owner);

        List<Horse> foundHorses = dao.searchHorses(criteriaForSearch);
        model.addAttribute("horseList", foundHorses);
        model.addAttribute("fromSearch", true);
        model.addAttribute("listSize", foundHorses.size());

        return "noAjaxDisplayHorseList";
    }

    @RequestMapping(value = "/newHorseFormNoAjax", method = RequestMethod.GET)
    public String displayNewHorseFormNoAjax() {
        return "newHorseFormWithoutAjax";
    }

    @RequestMapping(value = "/newHorseForm", method = RequestMethod.GET)
    public String displayFancyNewHorseFormNoAjax() {
        return "newHorseForm";
    }
//    @RequestMapping(value = "/fancyNewHorseFormNoAjax", method = RequestMethod.GET)
//    public String displayFancyNewHorseFormNoAjax() {
//        return "fancyNewHorseFormWithoutAjax";
//    }

    @RequestMapping(value = "/addHorseNoAjax", method = RequestMethod.POST)
    public String addNewContact(HttpServletRequest req) {
        String name = req.getParameter("name");
        String jockey = req.getParameter("jockey");
        String trainer = req.getParameter("trainer");
        String owner = req.getParameter("owner");

        Horse newHorse = new Horse();
        newHorse.setName(name);
        newHorse.setJockey(jockey);
        newHorse.setTrainer(trainer);
        newHorse.setOwner(owner);

        dao.addHorse(newHorse);

        return "redirect:displayHorseListNoAjax";
    }

    @RequestMapping(value = "/deleteHorseNoAjax", method = RequestMethod.GET)
    public String deleteHorse(HttpServletRequest req) {

        String id = req.getParameter("id");

        int sanitizedId = this.sanitizeId(id);
        if (sanitizedId >= 0) {
            dao.removeHorse(sanitizedId);
        }

        return "redirect:displayHorseListNoAjax";
    }

    @RequestMapping(value = "/editHorseNoAjax", method = RequestMethod.GET)
    public String displayEditFormNoAjax(HttpServletRequest req, Model model) {

        int sanitizedId = this.sanitizeId(req.getParameter("id"));
        if (sanitizedId >= 0) {
            Horse horse = dao.getHorseById(sanitizedId);
            model.addAttribute("horseToEdit", horse);
        }
        return "fancyEditHorseFormWithoutAjax";
    }

    @RequestMapping(value = "/fancyEditHorseNoAjax", method = RequestMethod.GET)
    public String displayFancyEditFormNoAjax(HttpServletRequest req, Model model) {

        int sanitizedId = this.sanitizeId(req.getParameter("id"));
        if (sanitizedId >= 0) {
            Horse horse = dao.getHorseById(sanitizedId);
            model.addAttribute("horseToEdit", horse);
        }
        return "fancyEditHorseFormWithoutAjax";
    }

    @RequestMapping(value = "/editHorseNoAjax", method = RequestMethod.POST)
    public String editModelHorseNoAjax(@ModelAttribute("horseToEdit") Horse horse) {
        dao.updateHorse(horse);
        return "redirect:displayHorseListNoAjax";
    }

    @RequestMapping(value = "/editFancyHorseNoAjaxWithValidation", method = RequestMethod.POST)
    public String editModelHorseNoAjaxValidation(@Valid @ModelAttribute("horseToEdit") Horse horse, BindingResult result) {
        if (result.hasErrors()) {
            return "fancyEditHorseFormWithoutAjax";
        }

        dao.updateHorse(horse);
        return "redirect:displayHorseListNoAjax";
    }

    private int sanitizeId(String idParam) {
        int idInt;

        try {
            idInt = Integer.parseInt(idParam);
        } catch (NumberFormatException e) {
            idInt = -1;
        }

        return idInt;
    }

}
