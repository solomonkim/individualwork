/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.aspects;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 *
 * @author apprentice
 */
public class SimpleTimerAdvice {

    public Object timeMethod(ProceedingJoinPoint jp) {
        Object ret = null;
        try {
            //starts timer
            long start = System.currentTimeMillis();
            ret = jp.proceed(); //tell the target method you are executing "around" to go ahead
            long end = System.currentTimeMillis(); //end the timer
            System.out.println("mmmmmmmmmmmmmmmmmmmmmmmmm");
            String nameOfMethodThatJustExecuted = jp.getSignature().getName();
            System.out.println(nameOfMethodThatJustExecuted + " took " + (end - start) + " milliseconds.");
            System.out.println("mmmmmmmmmmmmmmmmmmmmmmmmm");

        } catch (Throwable ex) {
            System.out.println("Exception occurred in SimpleTimerAdvice.timeMethod()");
        }
        return ret;
    }

}
