<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Unit Conversion</title>
    </head>
    <body>
    <center>
        <h1>Unit Conversion Results</h1>
        <p><c:out value="${originalDegreesOrMoneys}" /> <c:out value="${originalUnits}" />
            is equivalent to <c:out value="${convertedDegreesOrMoneys}" /> <c:out value="${convertedUnits}" />.  
        </p>
    </center>
</body>
</html>
