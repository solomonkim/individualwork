package com.swcguild.unitconvertermvc;

import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class UnitConversionController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayHome() {

        return "index";
    }

    @RequestMapping(value = "/currency", method = RequestMethod.POST)
    public String currency() {
//make it look like temperature but with currency
//and better math
        return "response";
    }

    @RequestMapping(value = "/temperature", method = RequestMethod.POST)
    public String temperature(HttpServletRequest request, Model model) {

        if (request.getParameter("degrees") != null && !request.getParameter("degrees").equals("")
                && request.getParameter("units") != null && !request.getParameter("units").equals("")) {
            double originalDegrees = Double.parseDouble(request.getParameter("degrees"));
            String originalUnits = request.getParameter("units");
            double convertedDegrees = 0;
            String convertedUnits = "";

            if (originalUnits.equals("C")) {
                convertedUnits = "F";
                convertedDegrees = (originalDegrees * 1.8) + 32;
            }

            if (originalUnits.equals("F")) {
                convertedUnits = "C";
                convertedDegrees = (originalDegrees - 32) / 1.8;
            }

            model.addAttribute("convertedDegreesOrMoneys", convertedDegrees);//these names have to match the ones on the jsp
            model.addAttribute("convertedUnits", convertedUnits);
            model.addAttribute("originalDegreesOrMoneys", originalDegrees);
            model.addAttribute("originalUnits", originalUnits);

        }
        return "response";
    }

}
