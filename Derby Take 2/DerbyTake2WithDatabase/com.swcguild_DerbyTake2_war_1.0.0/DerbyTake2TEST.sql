DROP DATABASE IF EXISTS Derby;
CREATE DATABASE Derby;
USE Derby;

CREATE TABLE IF NOT EXISTS Horses (
    name varchar(100) NOT NULL,
    owner varchar(100),
    trainer varchar(100),
    jockey varchar(100),
    PRIMARY KEY (name)
)  ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=23;

INSERT INTO Horses (name, owner, trainer, jockey) VALUES
('Creator', 'WinStar Farm, LLC', 'Steven M. Asmussen', 'Ricardo Santana, Jr.'),
('Lani', 'Koji Maeda', 'Mikio Matsunaga', 'Yutaka Take'),
('Mor Spirit', 'Michael Lund Petersen', 'Bob Baffert', 'Gary L. Stevens'),
('Mohaymen', 'Shadwell Stable', 'Kiaran P. McLaughlin', 'Junior Alvarado'),
('Danzing Candy', 'Halo Farms', 'Clifford W. Sise, Jr.', 'Mike E. Smith');

SELECT 
    *
FROM
    Horses;