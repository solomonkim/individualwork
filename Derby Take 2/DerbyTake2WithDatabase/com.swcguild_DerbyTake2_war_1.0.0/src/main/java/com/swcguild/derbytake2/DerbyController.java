package com.swcguild.derbytake2;

import java.util.List;
import javax.inject.Inject;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Solomon Kim
 */
@Controller
public class DerbyController {

    private DerbyDao dao;

    @Inject
    public DerbyController(DerbyDao dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayHomePage() {
        return "index";
    }

    @RequestMapping(value = "/addHorse", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public void addHorse(@RequestBody Horse horseToAdd) {

        dao.addHorse(horseToAdd);

    }

    @RequestMapping(value = "/getHorses", method = RequestMethod.GET)
    @ResponseBody
    public List<Horse> displayAllHorses() {
        return dao.getAllHorses();
    }

    @RequestMapping(value = "/displayHorse/{horseName}", method = RequestMethod.GET)
    @ResponseBody
    public Horse displayHorse(@PathVariable("horseName") String horseName) {

        return dao.getHorseByName(horseName);

    }

    @RequestMapping(value = "/editHorse/{horseName}", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.ACCEPTED)
    public void editHorse(@PathVariable("horseName") String horseName, @RequestBody Horse horse) {

        dao.updateHorse(horseName, horse);

    }

    @RequestMapping(value = "deleteHorse/{nameOfHorse}", method = RequestMethod.DELETE)
    @ResponseBody
    public void deleteHorse(@PathVariable("nameOfHorse") String nameOfHorse) {

        Horse horseToDelete = dao.getHorseByName(nameOfHorse);

        if (horseToDelete != null) {

            dao.deleteHorse(nameOfHorse);

        }

    }

}
