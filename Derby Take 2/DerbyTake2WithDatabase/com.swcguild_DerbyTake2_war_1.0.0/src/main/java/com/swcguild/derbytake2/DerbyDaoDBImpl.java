/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.derbytake2;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

/**
 *
 * @author apprentice
 */
public class DerbyDaoDBImpl implements DerbyDao {

    private static final String SQL_CREATE_HORSE = "INSERT INTO Horses (name, owner, trainer, jockey) VALUES (?, ?, ?, ?)";
    private static final String SQL_READ_HORSE = "SELECT * FROM Horses WHERE name = ?";
    private static final String SQL_READ_ALL_HORSES = "SELECT * FROM Horses WHERE 1=1";
    private static final String SQL_UPDATE_HORSE = "UPDATE Horses SET name = ?, owner = ?, trainer = ?, jockey = ?";
    private static final String SQL_DELETE_HORSE = "DELETE FROM Horses WHERE name = ?";

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public Horse addHorse(Horse horseToAdd) {
        jdbcTemplate.update(SQL_CREATE_HORSE,
                horseToAdd.getName(),
                horseToAdd.getOwner(),
                horseToAdd.getTrainer(),
                horseToAdd.getJockey()
        );

        return horseToAdd;
    }

    @Override
    public void deleteHorse(String nameOfHorseToDelete) {
        jdbcTemplate.update(SQL_DELETE_HORSE, nameOfHorseToDelete);
    }

    @Override
    public List<Horse> getAllHorses() {
        return jdbcTemplate.query(SQL_READ_ALL_HORSES, new Mapper());
    }

    @Override
    public Horse getHorseByName(String nameOfHorseToGet) {
        try {
            return jdbcTemplate.queryForObject(SQL_READ_HORSE,
                    new Mapper(), nameOfHorseToGet);
        } catch (EmptyResultDataAccessException ex) {
            return null;
        }
    }

    @Override
    public void updateHorse(String originalName, Horse horseToUpdate) {
        jdbcTemplate.update(SQL_UPDATE_HORSE,
                horseToUpdate.getName(),
                horseToUpdate.getOwner(),
                horseToUpdate.getTrainer(),
                horseToUpdate.getJockey()
        );
    }

    private static final class Mapper implements RowMapper<Horse> {

        public Horse mapRow(ResultSet rs, int rowNum) throws SQLException {
            Horse horse = new Horse();
            horse.setName(rs.getString("name"));
            horse.setOwner(rs.getString("owner"));
            horse.setTrainer(rs.getString("trainer"));
            horse.setJockey(rs.getString("jockey"));

            return horse;
        }

    }
}
