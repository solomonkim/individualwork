package com.swcguild.derbytake2;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Solomon Kim
 */
public class DerbyDaoImpl implements DerbyDao {

    private ArrayList<Horse> allHorses = new ArrayList<>();

    public DerbyDaoImpl() {

        Horse horse = new Horse();
        horse.setName("Gun Runner");
        horse.setOwner("Winchell Thoroughbreds LLC");
        horse.setTrainer("Steven M. Asmussen");
        horse.setJockey("TBA");
        this.addHorse(horse);

        horse = new Horse();
        horse.setName("Nyquist");
        horse.setOwner("Reddam Racing LLC");
        horse.setTrainer("Doug F. O'Neill");
        horse.setJockey("TBA");
        this.addHorse(horse);

        horse = new Horse();
        horse.setName("Exaggerator");
        horse.setOwner("Big Chief Racing LLC");
        horse.setTrainer("J. Keith Desormeaux");
        horse.setJockey("TBA");
        this.addHorse(horse);

        horse = new Horse();
        horse.setName("Outwork");
        horse.setOwner("Repole Stable");
        horse.setTrainer("Todd A. Pletcher");
        horse.setJockey("TBA");
        this.addHorse(horse);

        horse = new Horse();
        horse.setName("Brodys Cause");
        horse.setOwner("Albaugh Family Stable");
        horse.setTrainer("Dale L. Romans");
        horse.setJockey("TBA");
        this.addHorse(horse);

    }

    @Override
    public Horse addHorse(Horse horseToAdd) {
        allHorses.add(horseToAdd);
        return horseToAdd;
    }

    @Override
    public List<Horse> getAllHorses() {
        return allHorses;
    }

    @Override
    public Horse getHorseByName(String nameOfHorseToGet) {

        for (Horse h : allHorses) {
            if (h.getName().equalsIgnoreCase(nameOfHorseToGet)) {
                return h;
            }
        }

        return null;

    }

    @Override
    public void updateHorse(String originalName, Horse horseToUpdate) {

        Horse originalHorse = null;

        for (Horse h : allHorses) {

            if (h.getName().equals(originalName)) {
                originalHorse = h;
            }

        }

        if (originalHorse != null) {
            allHorses.remove(originalHorse);
            allHorses.add(horseToUpdate);
        }
    }

    @Override
    public void deleteHorse(String nameOfHorseToDelete) {

        allHorses.remove(getHorseByName(nameOfHorseToDelete));

    }

}
