/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.derbytake2;

import java.util.List;

/**
 *
 * @author apprentice
 */
public interface DerbyDao {

    Horse addHorse(Horse horseToAdd);

    void deleteHorse(String nameOfHorseToDelete);

    List<Horse> getAllHorses();

    Horse getHorseByName(String nameOfHorseToGet);

    void updateHorse(String originalName, Horse horseToUpdate);
    
}
