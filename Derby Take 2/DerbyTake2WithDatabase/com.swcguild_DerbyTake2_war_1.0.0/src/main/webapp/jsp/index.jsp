<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>

    <head>

        <title>2016 Kentucky Derby</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    </head>

    <body>

        <h1>2016 Kentucky Derby</h1>
        <hr />

        <div class="container">
            <table id="horseTable" class="table table-striped">
                <tr>
                    <th width="20%">Horse</th>
                    <th width="20%">Owner</th>
                    <th width="20%">Trainer</th>
                    <th width="20%">Jockey</th>
                    <th width="20%"></th>
                </tr>
                <tbody id="contentRows"></tbody>
            </table>

        </div>

        <!-- edit modal start -->
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog"
             aria-labelledby="detailsModalLabel" aria-hidden="true">

            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="detailsModalLabel">Edit Horse</h4>
                    </div>
                    <div class="modal-body">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="edit-name" class="col-md-4 control-label">Name:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-name" placeholder="Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-owner" class="col-md-4 control-label">Owner:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-owner" placeholder="Owner"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-trainer" class="col-md-4 control-label">Trainer:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-trainer" placeholder="Trainer"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-jockey" class="col-md-4 control-label">Jockey:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-jockey" placeholder="Jockey"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit" id="edit-button" class="btn btn-default" data-dismiss="modal">Edit</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <input type="hidden" id="originalName"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- edit modal end -->
        <br />
        <h3>add a new horse</h3>
        <br />
        <form class="form-horizontal" role="form">
            <div class="form-group">
                <label for="add-name" class="col-md-4 control-label">Name:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="add-name" placeholder="Name"/>
                </div>
            </div>
            <div class="form-group">
                <label for="add-owner" class="col-md-4 control-label">Owner:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="add-owner" placeholder="Owner"/>
                </div>
            </div>
            <div class="form-group">
                <label for="add-trainer" class="col-md-4 control-label">Trainer:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="add-trainer" placeholder="Trainer"/>
                </div>
            </div>
            <div class="form-group">
                <label for="add-jockey" class="col-md-4 control-label">Jockey:</label>
                <div class="col-md-4">
                    <input type="text" class="form-control" id="add-jockey" placeholder="Jockey"/>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-4 col-md-4">
                    <button id="add-button" class="btn btn-default">Submit</button>
                </div>
            </div>
        </form>

        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/derby.js"></script>

    </body>

</html>

