$(document).ready(function () {
    getHorses();
    $('#edit-button').click(editHorse);
    $('#add-button').click(function (event) {

        event.preventDefault();

        $.ajax({
            url: 'addHorse',
            type: 'POST',
            data: JSON.stringify({
                name: $('#add-name').val(),
                owner: $('#add-owner').val(),
                trainer: $('#add-trainer').val(),
                jockey: $('#add-jockey').val()
            }),
            headers: {
                'Content-Type': 'application/json'
            }

        }).success(function () {
            getHorses();
        });
    });
});

function getHorses() {
    $.ajax({
        url: 'getHorses',
        type: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) { //why do we put a function inside an anonymous function here instead of just calling the function on success?
        populateTable(data, status); // horseList comes from data--where is data coming from?
    }
    );
}

function populateTable(horseList, status) {
    var tableBody = $('#contentRows');
    tableBody.empty();

    for (var i = 0; i < horseList.length; i++) {

        var horse = horseList[i];
        $("#contentRows").append("<tr><td width=\"20%\">" +
                horse.name + "</td><td width=\"20%\">" +
                horse.owner + "</td><td width=\"20%\">" +
                horse.trainer + "</td><td width=\"20%\">" +
                horse.jockey + "</td>" +
                "<td width=\"20%\"><button onclick='displayEditHorse(\"" + horse.name + "\")'>edit</button> <button onclick='deleteHorse(\"" + horse.name + "\")'>delete</button></td></tr>");
    }
}

function deleteHorse(horseName) {

    $.ajax({
        url: 'deleteHorse/' + horseName,
        type: 'DELETE'
    }).success(function () {
        window.alert("Successfully removed.");
        getHorses();
    });

}

function displayEditHorse(horseName) {

    $.ajax({
        url: 'displayHorse/' + horseName,
        type: 'GET',
        headers: {
            'Accept': 'application/json'
        },
        dataType: 'json'
    }).success(function (horse) {
        $('#edit-name').val(horse.name);
        $('#edit-owner').val(horse.owner);
        $('#edit-trainer').val(horse.trainer);
        $('#edit-jockey').val(horse.jockey);
        $('#editModal').modal('toggle');
        $('#originalName').val(horse.name);
    });

}

function editHorse(event) {

    $.ajax({
        url: 'editHorse/' + $('#originalName').val(),
        type: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        data: JSON.stringify({
            name: $('#edit-name').val(),
            owner: $('#edit-owner').val(),
            trainer: $('#edit-trainer').val(),
            jockey: $('#edit-jockey').val()
        })
    }).success(function () {
        getHorses();
    });
}