DROP DATABASE IF EXISTS Derby;
CREATE DATABASE Derby;
USE Derby;

CREATE TABLE Horses (
    id int(11) AUTO_INCREMENT NOT NULL,
    name varchar(100),
    owner varchar(100),
    trainer varchar(100),
    jockey varchar(100),
    PRIMARY KEY (id)
);

INSERT INTO Horses (name, owner, trainer, jockey) VALUES
('Creator', 'WinStar Farm, LLC', 'Steven M. Asmussen', 'Ricardo Santana, Jr.'),
('Lani', 'Koji Maeda', 'Mikio Matsunaga', 'Yutaka Take'),
('Mor Spirit', 'Michael Lund Petersen', 'Bob Baffert', 'Gary L. Stevens'),
('Mohaymen', 'Shadwell Stable', 'Kiaran P. McLaughlin', 'Junior Alvarado'),
('Danzing Candy', 'Halo Farms', 'Clifford W. Sise, Jr.', 'Mike E. Smith');

SELECT 
    *
FROM
    Horses;