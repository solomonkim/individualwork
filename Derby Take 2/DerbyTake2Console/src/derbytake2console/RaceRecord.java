package derbytake2console;

/**
 *
 * @author Solomon Kim
 */
public class RaceRecord {

    RaceProfile race;
    int finish;
    double quarterMileSplit;

    public RaceRecord() {
    }

    public RaceProfile getRace() {
        return race;
    }

    public void setRace(RaceProfile race) {
        this.race = race;
    }

    public int getFinish() {
        return finish;
    }

    public void setFinish(int finish) {
        this.finish = finish;
    }

    public double getQuarterMileSplit() {
        return quarterMileSplit;
    }

    public void setQuarterMileSplit(double quarterMileSplit) {
        this.quarterMileSplit = quarterMileSplit;
    }

}
