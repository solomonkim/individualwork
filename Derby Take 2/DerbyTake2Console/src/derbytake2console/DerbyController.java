package derbytake2console;

import java.util.ArrayList;

public class DerbyController {

    DerbyDao dao = new DerbyDao();
    ConsoleIO console = new ConsoleIO();

    public DerbyController() {
    }

    public void run() {

        console.print("2016 Kentucky Derby App");
        int mainMenuChoice;
        boolean keepRunning = true;

        while (keepRunning) {

            printMenu();
            mainMenuChoice = console.readInt("Select an option (1-12)", 1, 12);

            switch (mainMenuChoice) {

                case 1:
                    addHorse();
                    break;

//                case 2:
//                    addRaceProfile();
//                    break;
//                case 3:
//                    addRaceRecord();
//                    break;
                case 4:
                    displayHorse();
                    break;

//                case 5:
//                    displayRaceRecord();
//                    break;
                case 6:
                    editHorse();
                    break;

//                case 7:
//                    editRaceProfile();
//                    break;
//                case 8:
//                    editRaceRecord();
//                    break;
                case 9:
                    deleteHorse();
                    break;

//                case 10:
//                    deleteRaceProfile();
//                    break;
//                case 11:
//                    deleteRaceRecord();
//                    break;
                case 12:
                    keepRunning = false;
                    break;

            }

        }

    }

    public void printMenu() {

        console.print("\n1: add a horse profile");
        console.print("2: add a race profile");
        //console.print("3: add a race record"); //put into editHorse?
        console.print("4: view a horse profile");
        //console.print("5: view a race record");
        console.print("6: edit a horse profile");
//        console.print("7: edit a race profile");
//        console.print("8: edit a race record");
        console.print("9: delete a horse profile");
//        console.print("10: delete a race profile");
//        console.print("11: delete a race record");
        console.print("12: exit");

    }

    public void addHorse() {

        String name = console.readString("\nname: ");
        String owner = console.readString("owner: ");
        String trainer = console.readString("trainer: ");
        String jockey = console.readString("jockey: ");

        Horse horseToAdd = new Horse(name, owner, trainer, jockey);

        dao.addHorse(horseToAdd);

    }

    public void addRaceProfile() {

        String name = console.readString("\n name: ");
        int grade = console.readInt("grade (1 - 3): ", 1, 3);
        double distanceFurlongs = console.readDouble("distance (furlongs): ");
        String surface = console.readString("\n surface: ");
        int daysBeforeDerby = console.readInt("days before Kentucky Derby: ");
        ArrayList<Horse> ran = null;

        RaceProfile raceProfileToAdd = new RaceProfile(name, grade, distanceFurlongs, surface, daysBeforeDerby, ran);

        dao.addRaceProfile(raceProfileToAdd);

    }

//    public void addRaceRecord() {
//
//        displayAllHorses();
//        Horse horseToAddRaceRecordFor = dao.getHorseByName(console.readString("For which horse do you want to add a race record?"));
//        displayAllRaceProfiles(); //Make displayAllRaceProfiles()
//        RaceProfile raceProfileToAddRecordFor = dao.getRaceProfileByName(console.readString("Which race?")); //Make dao.getRaceProfileByName()
//        if (raceProfileToAddRecordFor == null) {
//
//            int addRaceProfileChoice = console.readInt("That race was not found. Do you want to add it?"
//                    + "\n1. Yes"
//                    + "\n2. No", 1, 2);
//
//            switch (addRaceProfileChoice) {
//
//                case 1:
//                    addRaceProfile(); //Loop back to adding a race record after adding the profile
//                    break;
//
//                case 2:
//                    break;
//
//            }
//
//        } else {
//            
//            int finish = console.readInt("In what place did " + horseToAddRaceRecordFor.getName() + " finish? ");
//            double quarterMileSplit = console.readDouble("What was " + horseToAddRaceRecordFor.getName() + "'s quarter mile split? (seconds) ");
//            
//            dao. dao.addRaceRecord(raceProfileToAddRecordFor, finish, quarterMileSplit); //
//                    
//        }
//
//    }

    public void displayAllHorses() {

        ArrayList<Horse> allHorses = dao.getAllHorses();

        console.print("\n2016 Kentucky Derby Contenders");

        for (Horse h : allHorses) {
            console.print(h.name);
        }

    }

    public void displayHorse() {

        Horse horseToDisplay = null;
        String nameOfHorseToDisplay;

        while (horseToDisplay == null) {
            displayAllHorses();
            nameOfHorseToDisplay = console.readString("\nname of horse to display (enter \"0\" to cancel and go back to the main menu): ");
            if (nameOfHorseToDisplay.equals("0")) {
                break;
            }
            horseToDisplay = dao.getHorseByName(nameOfHorseToDisplay);
            if (horseToDisplay == null) {
                console.print("That horse was not found.");
            }

        }

        if (horseToDisplay != null) {
            console.print("\n" + horseToDisplay.name);
            console.print("owner: " + horseToDisplay.owner);
            console.print("trainer: " + horseToDisplay.trainer);
            console.print("jockey: " + horseToDisplay.jockey);
        }

    }

    public void editHorse() {

        Horse horseToEdit = null;
        String nameOfHorseToEdit;

        while (horseToEdit == null) {

            displayAllHorses();
            nameOfHorseToEdit = (console.readString("\nname of horse to edit (enter \"0\" to cancel and go back to the main menu): "));

            if (nameOfHorseToEdit.equals("0")) {
                break;
            }

            horseToEdit = dao.getHorseByName(nameOfHorseToEdit);

            if (horseToEdit == null) {
                console.print("That horse was not found.");
            }

        }

        if (horseToEdit != null) {
            console.print("field (current value) - Leave the field blank to keep the current value.");
            String newName = console.readString("name (" + horseToEdit.name + "): ");
            String newOwner = console.readString("owner (" + horseToEdit.owner + "): ");
            String newTrainer = console.readString("trainer (" + horseToEdit.trainer + "): ");
            String newJockey = console.readString("jockey (" + horseToEdit.jockey + "): ");

            if (!newName.equals("")) {
                horseToEdit.name = newName;
            }

            if (!newOwner.equals("")) {
                horseToEdit.owner = newName;
            }

            if (!newTrainer.equals("")) {
                horseToEdit.trainer = newTrainer;
            }

            if (!newJockey.equals("")) {
                horseToEdit.jockey = newJockey;
            }

        }

    }

    public void deleteHorse() {

        Horse horseToDelete = null;
        String nameOfHorseToDelete;

        displayAllHorses();

        while (horseToDelete == null) {

            nameOfHorseToDelete = console.readString("\nname of horse to delete (enter \"0\" to cancel and go back to the main menu): ");

            if (nameOfHorseToDelete.equals("0")) {
                break;
            }

            horseToDelete = dao.getHorseByName(nameOfHorseToDelete);

            if (horseToDelete != null) {

                console.print("\n" + horseToDelete.name);
                console.print("owner: " + horseToDelete.owner);
                console.print("trainer: " + horseToDelete.trainer);
                console.print("jockey: " + horseToDelete.jockey);

                switch (console.readInt("\n Are you sure you want to delete this horse?"
                        + "\n 1: Yes, delete this horse."
                        + "\n 2: No, cancel deletion and return to main menu.", 1, 2)) {
                    case 1:
                        dao.deleteHorse(horseToDelete);
                        break;
                    case 2:
                        break;

                }

            }
        }

    }

}
