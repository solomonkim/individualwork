package derbytake2console;

import java.util.ArrayList;

/**
 *
 * @author Solomon Kim
 */
public class Horse {

    String name;
    String owner;
    String trainer;
    String jockey;
    ArrayList<RaceRecord> prepRaceHistory;

    public Horse() {
    }

    public Horse(String name, String owner, String trainer, String jockey) {
        this.name = name;
        this.owner = owner;
        this.trainer = trainer;
        this.jockey = jockey;
    }

    public Horse(String name, String owner, String trainer, String jockey, ArrayList<RaceRecord> prepRaceHistory) {
        this.name = name;
        this.owner = owner;
        this.trainer = trainer;
        this.jockey = jockey;
        this.prepRaceHistory = prepRaceHistory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTrainer() {
        return trainer;
    }

    public void setTrainer(String trainer) {
        this.trainer = trainer;
    }

    public String getJockey() {
        return jockey;
    }

    public void setJockey(String jockey) {
        this.jockey = jockey;
    }
    
    public ArrayList<RaceRecord> getPrepRaceHistory() {
        return prepRaceHistory;
    }

    public void setPrepRaceHistory(ArrayList<RaceRecord> prepRaceHistory) {
        this.prepRaceHistory = prepRaceHistory;
    }

}
