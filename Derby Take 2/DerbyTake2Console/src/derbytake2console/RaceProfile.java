package derbytake2console;

import java.util.ArrayList;

/**
 *
 * @author Solomon Kim
 */
public class RaceProfile {

    String name;
    int grade;
    double distanceFurlongs;
    String surface;
    int daysBeforeDerby;
    ArrayList<Horse> ran;

    public RaceProfile() {
    }

    public RaceProfile(String name, int grade, double distanceFurlongs, String surface, int daysBeforeDerby, ArrayList<Horse> ran) {
        this.name = name;
        this.grade = grade;
        this.distanceFurlongs = distanceFurlongs;
        this.surface = surface;
        this.daysBeforeDerby = daysBeforeDerby;
        this.ran = ran;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }

    public double getDistanceFurlongs() {
        return distanceFurlongs;
    }

    public void setDistanceFurlongs(double distanceFurlongs) {
        this.distanceFurlongs = distanceFurlongs;
    }

    public String getSurface() {
        return surface;
    }

    public void setSurface(String surface) {
        this.surface = surface;
    }

    public int getDaysBeforeDerby() {
        return daysBeforeDerby;
    }

    public void setDaysBeforeDerby(int daysBeforeDerby) {
        this.daysBeforeDerby = daysBeforeDerby;
    }

    public ArrayList<Horse> getRan() {
        return ran;
    }

    public void setRan(ArrayList<Horse> ran) {
        this.ran = ran;
    }

}
