/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package derbytake2console;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class DerbyDao {

    private ArrayList<Horse> allHorses = new ArrayList<>();
    private ArrayList<RaceProfile> allRaces = new ArrayList<>();

    public DerbyDao() {

        Horse horse = new Horse();
        horse.setName("Gun Runner");
        horse.setOwner("Winchell Thoroughbreds LLC");
        horse.setTrainer("Steven M. Asmussen");
        horse.setJockey("TBA");
        this.addHorse(horse);

        horse = new Horse();
        horse.setName("Nyquist");
        horse.setOwner("Reddam Racing LLC");
        horse.setTrainer("Doug F. O'Neill");
        horse.setJockey("TBA");
        this.addHorse(horse);

        horse = new Horse();
        horse.setName("Exaggerator");
        horse.setOwner("Big Chief Racing LLC");
        horse.setTrainer("J. Keith Desormeaux");
        horse.setJockey("TBA");
        this.addHorse(horse);

        horse = new Horse();
        horse.setName("Outwork");
        horse.setOwner("Repole Stable");
        horse.setTrainer("Todd A. Pletcher");
        horse.setJockey("TBA");
        this.addHorse(horse);

        horse = new Horse();
        horse.setName("Brody's Cause");
        horse.setOwner("Albaugh Family Stable");
        horse.setTrainer("Dale L. Romans");
        horse.setJockey("TBA");
        this.addHorse(horse);

    }

    public void addHorse(Horse horseToAdd) {
        
        allHorses.add(horseToAdd);
        
    }

    public ArrayList<Horse> getAllHorses() {
        
        return allHorses;
        
    }

    public void addRaceProfile(RaceProfile raceProfileToAdd) {
        
        allRaces.add(raceProfileToAdd);
        
    }

    public Horse getHorseByName(String nameOfHorseToGet) {

        for (Horse h : allHorses) {
            
            if (h.getName().equalsIgnoreCase(nameOfHorseToGet)) {
                return h;
                
            }
            
        }

        return null;

    }

    public void deleteHorse(Horse horseToDelete) {

        allHorses.remove(horseToDelete);

    }

}
