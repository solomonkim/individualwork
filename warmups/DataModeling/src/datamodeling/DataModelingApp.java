/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodeling;

import datamodeling.dto.Class;
import datamodeling.dto.Student;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author apprentice
 */
public class DataModelingApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        HashMap<Class, ArrayList<Student>> classes = new HashMap<>();

        Student Solomon = new Student("Solomon", "solomon@solomon.com");
        Student Carrie = new Student("Carrie", "carrie@carrie.com");
        Student Andrew = new Student("Andrew", "andrew@andrew.com");
        Student Austyn = new Student("Austyn", "austyn@austyn.com");
        Student Patrick = new Student("Patrick", "patrick@patrick.com");
        Student Winona = new Student("Winona", "winona@winona.com");
        Student Rene = new Student("Rene", "rene@rene.com");

        ArrayList<Student> englishStudents = new ArrayList<>();
        ArrayList<Student> mathStudents = new ArrayList<>();
        ArrayList<Student> historyStudents = new ArrayList<>();

        englishStudents.add(Solomon);
        englishStudents.add(Andrew);
        englishStudents.add(Patrick);
        englishStudents.add(Rene);
        englishStudents.add(Carrie);
        mathStudents.add(Austyn);
        mathStudents.add(Winona);
        mathStudents.add(Solomon);
        mathStudents.add(Andrew);
        historyStudents.add(Patrick);
        historyStudents.add(Rene);
        historyStudents.add(Carrie);
        historyStudents.add(Austyn);
        historyStudents.add(Winona);
        historyStudents.add(Solomon);
        historyStudents.add(Andrew);

        Class English = new Class(englishStudents);
        Class Math = new Class(mathStudents);
        Class History = new Class(historyStudents);

        classes.put(English, englishStudents);
        classes.put(Math, mathStudents);
        classes.put(History, historyStudents);

        System.out.println("\nEnglish student roster");
        for (Student s : English.roster) {
            System.out.println(s.getName());
        }
        System.out.println("\nMath student roster");
        for (Student s : Math.roster) {
            System.out.println(s.getName());
        }
        System.out.println("\nHistory student roster");
        for (Student s : History.roster) {
            System.out.println(s.getName());
        }

    }

}
