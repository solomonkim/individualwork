/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodeling.dto;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class Student {

    String name;
    String email;
    ArrayList<Class> classSchedule = new ArrayList<Class>();

    public Student(String name, String email) {
        this.name = name;
        this.email = email;
    }

    public String getName() {
        return name;
    }

}
