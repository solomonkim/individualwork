/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datamodeling.dto;

import java.util.ArrayList;

/**
 *
 * @author apprentice
 */
public class Class {

    public ArrayList<Student> roster;

    public Class(ArrayList<Student> roster) {
        this.roster = roster;
    }

    public Class() {
    }

    public ArrayList<Student> getRoster() {
        return roster;
    }

    public void setRoster(ArrayList<Student> roster) {
        this.roster = roster;
    }

}
