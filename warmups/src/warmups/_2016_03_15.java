/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warmups;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class _2016_03_15 {

    public static void main(String[] args) {
        HashMap<String, String> teams = new HashMap<>();
        teams.put("Cleveland", "Browns");
        teams.put("Pittsburgh", "Steelers");
        teams.put("Cincinnati", "Bengals");
        teams.put("Minneapolis", "Vikings");
        teams.put("Kansas City", "Chiefs");

        Set<String> keys = teams.keySet();

        for (String s : keys) {
            System.out.println(s + " " + teams.get(s));
        }

        Iterator<String> i = keys.iterator();

        while (i.hasNext()) {
            String key = i.next();
            String value = teams.get(key);
            System.out.println(key + " " + value);

        }

        Set<Map.Entry<String, String>> entrySet = teams.entrySet();

        for (Map.Entry<String, String> entry : entrySet) {
            System.out.println(entry.getKey() + " " + entry.getValue());
        }

    }

}
