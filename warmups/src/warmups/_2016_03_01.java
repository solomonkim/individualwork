/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package warmups;

import java.util.HashMap;

/**
 *
 * @author apprentice
 */
public class _2016_03_01 {

    public static void main(String[] args) {
        HashMap<String, Pet> petShelter = new HashMap<>();
    }
    
    public class Pet {
        
        String name;
        String species;
        String condition;

        public Pet(String name, String species, String condition) {
            this.name = name;
            this.species = species;
            this.condition = condition;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSpecies() {
            return species;
        }

        public void setSpecies(String species) {
            this.species = species;
        }

        public String getCondition() {
            return condition;
        }

        public void setCondition(String condition) {
            this.condition = condition;
        }
        
        
        
    }

}
