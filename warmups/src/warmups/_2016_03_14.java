package warmups;

import java.util.ArrayList;
import java.util.Iterator;

public class _2016_03_14 {

    public static void main(String[] args) {
        ArrayList<String> teams = new ArrayList<>();
        teams.add("Vikings");
        teams.add("Packers");
        teams.add("Lions");
        teams.add("Bears");
        teams.add("Browns");
        teams.add("Bengals");
        teams.add("Steelers");
        teams.add("Ravens");
        for (String s : teams) {
            System.out.println(s);
        }
        Iterator<String> teamsIterator = teams.iterator();
        while (teamsIterator.hasNext()) {
            System.out.println(teamsIterator.next());
        }
        for (int i = 0; i < teams.size(); i++) {
            System.out.println(teams.get(i));
        }
    }
}
