package warmups;
import java.util.Scanner;
public class _2016_02_23 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter your age. ");
        System.out.println(ageMessage(sc.nextInt()));
    }
    private static String ageMessage(int age) {
        if (age < 19) {
            return "You must be in school.";
        } else if (age <= 65) {
            return "Time to go to work.";
        } else {
            return "Enjoy retirement!";
        }
    }
}