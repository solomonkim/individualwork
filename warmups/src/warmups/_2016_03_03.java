package warmups;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

/**
 * @author Solomon Kim a.k.a. "Special K"
 */
public class _2016_03_03 {

    public static void main(String[] args) {
        HashMap<String, Integer> points = new HashMap<>();
        int sum = 0;

        points.put("Smith", 23);
        points.put("Jones", 12);
        points.put("Jordan", 45);
        points.put("Pippen", 32);
        points.put("Kerr", 15);

        Set<String> keys = points.keySet();

        for (String k : keys) {
            System.out.println(k + " - " + points.get(k));
            sum += points.get(k);
        }

        System.out.println("The average is " + sum / points.size() + ".");

    }
}
