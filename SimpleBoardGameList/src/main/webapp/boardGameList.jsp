<%-- 
    Document   : boardGameList
    Created on : Mar 25, 2016, 1:46:15 PM
    Author     : apprentice
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
           uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Solomon's Board Game Library</title>
    </head>
    <body>
        <h1>Solomon's Board Game Library</h1>
        <hr />
        <c:forEach items="${myExcellentLibrary}" var="boardGame">
            <c:out value="${boardGame.name}" default = "[unknown]" /> 
            (<c:out value="${boardGame.publisher}" default="[unknown]" />)
            <br />
            <c:out value="${boardGame.minPlayers}" default="?" /> - 
            <c:out value="${boardGame.maxPlayers}" default="?" /> players
            <br />
            <c:out value="${boardGame.avgDuration}" default="?" /> minutes
            <hr />
        </c:forEach>
    </body>
</html>
