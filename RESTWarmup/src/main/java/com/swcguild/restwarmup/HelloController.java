package com.swcguild.restwarmup;

import java.util.Random;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller //Handles the instantiation of the controller bean --see spring-dispatcher-servlet.xml for details
public class HelloController {

//do not need the landing page return method because of the welcome file list in web.xml
    public HelloController() {
    }

    Random r = new Random();

    @RequestMapping(value = "/dieRoll/d{sideNum}", method = RequestMethod.POST)
    // alternatively, you can get the number of sides from the request body
    @ResponseBody
    public int getDieRoll(@PathVariable("sideNum") int numSides) {
        return r.nextInt(numSides) + 1;
    }

    @RequestMapping(value = "/number", method = RequestMethod.GET)
    @ResponseBody
    public int getNumber() {
        return r.nextInt(1000);
    }

    @RequestMapping(value = "/compliment", method = RequestMethod.POST)
    @ResponseBody
    public String getCompliment(@RequestBody String name) {
        String[] compliments = {
            "Sharp shootin' there, %s...",
            "Way to be in the game, %s...",
            "I like the cut of your jib, %s...",
            "You look really pretty today, %s..."
        };

        return String.format(compliments[r.nextInt(compliments.length)], name);
    }

    @RequestMapping(value = "/animal", method = RequestMethod.GET)
    @ResponseBody
    public String getRandomAnimal() {
        String[] animals = {"melancholic panda", "phlegmatic panda", "choleric panda", "sanguine panda", "untempered panda"};
        return animals[r.nextInt(animals.length)];
    }

}
