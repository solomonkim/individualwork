$(document).ready(function () {

    $('#dieroll-button').click(function (event) {
        $.ajax({
            url: 'dieroll/d' + $('#sides').val(),
            type: 'POST',
            data: $('#sides').val(),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            dataType: 'application/json'
        }).success(function (dieroll) {
            $('#dieroll').text(dieroll);
        });
    });

    $('#number-button').click(function (event) {
        $.ajax({
            url: 'number',
            type: 'GET',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        }).success(function (number) {
            $('#number').text(number);
        });
    });

    $('#compliment-button').click(function (event) {
        $.ajax({
            url: 'compliment',
            type: 'POST',
            data: $('#name').val(),
            headers: {
                'Accept': 'text/plain',
                'Content-Type': 'text/plain'
            }
        }).success(function (incomingCompliment) {
            $('#compliment').text(incomingCompliment);
        });
    });

    $('#animal-button').click(function (event) {
        $.ajax({
            url: 'animal',
            type: 'GET',
            headers: {
                'Accept': 'text/plain'
            }
        }).success(function (animal) {
            $('#animal').text(animal);
        });
    });
});