<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- SWC Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">

    </head>

    <body>
        <div class="container">

            <h1>REST Warmup - Monday, April 11</h1>
            <hr/>

            <div class="col-md-6" >
                <h2>random die roll</h2>
                <input type="number" class="form-control" min="1" step="1" id="sides" placeholder="How many sides on your die?">
                <br />
                <button id="dieroll-button" class="btn btn-danger">Roll the die!</button>
                <h1 id="dieroll" />
            </div>

            <div class="col-md-6">
                <h2>random number</h2>
                <br /><br />
                <button id="number-button" class="btn btn-info">Give me a number!</button>
                <h1 id="number" />
            </div>

            <div class="col-md-6">
                <h2>random compliment</h2>
                <input type="text" class="form-control" id="name" placeholder="What is your name?">
                <br />
                <button id="compliment-button" class="btn">Be nice to me!</button>
                <h1 id="compliment" />
            </div>

            <div class="col-md-6">
                <h2>random animal</h2>
                <br /><br />
                <button id="animal-button" class="btn btn-success">Give me an adorable animal!</button>
                <h1 id="animal" />
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/restpractice.js"></script>

    </body>
</html>