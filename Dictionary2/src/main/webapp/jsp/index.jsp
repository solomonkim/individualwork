<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Personal Dictionary</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

    </head>
    <body>
        <div class="container">
            <div class="col-sm-offset-1 col-sm-5" style="background-color: #a6e1ec">
                <h1>add a new word</h1>
                <form action="" method="POST">
                    <table>
                        <tr>
                            <td align="right">word:</td>
                            <td align="right"><input type="text" name="word"></td>
                        </tr>
                        <tr>
                            <td align="left">definition:</td>
                            <td align="left"><input type="text" name="definition"></td>
                        </tr>
                    </table>
                    <button type="submit">Submit</button>
                </form>
            </div>
            <div class="col-sm-5" style="background-color: #f7ecb5">
                <h1>words</h1>
                <ul>
                    <c:forEach items="${words}" var="wordObj">
                        <li>${wordObj.word}</li>
                        </c:forEach>
                </ul>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.12.2.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>

    </body>
</html>