package com.swcguild.dictionary2.dao;

import com.swcguild.dictionary2.model.Word;
import java.util.ArrayList;
import java.util.List;

public class DictionaryDaoImpl implements DictionaryDao {

    ArrayList<Word> dictionary = new ArrayList<>();

    @Override
    public Word addWord(Word wordToAdd) {
        dictionary.add(wordToAdd);
        return wordToAdd;
    }

    @Override
    public List<Word> getAllWords() {
        return dictionary;
    }

}