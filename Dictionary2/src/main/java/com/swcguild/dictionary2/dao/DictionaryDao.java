package com.swcguild.dictionary2.dao;

import com.swcguild.dictionary2.model.Word;
import java.util.List;

public interface DictionaryDao {

    public Word addWord(Word wordToAdd);

    public List<Word> getAllWords();
}