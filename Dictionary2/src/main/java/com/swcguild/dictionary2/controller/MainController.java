/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.swcguild.dictionary2.controller;

import com.swcguild.dictionary2.dao.DictionaryDao;
import com.swcguild.dictionary2.dao.DictionaryDaoImpl;
import com.swcguild.dictionary2.model.Word;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author apprentice
 */
@Controller //import org.springframework.stereotype.Controller;
public class MainController {

    DictionaryDao dao = new DictionaryDaoImpl();

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String displayMainPage(Model model) { //need the model so that the dao can talk to the jsp since the jsp cannot talk to it directly
        model.addAttribute("words", dao.getAllWords()); //adding the List<Word> to the model
        return "index"; //needs to match the name of a .jsp file in the jsp folder
    }

    @RequestMapping(value = "/", method = RequestMethod.POST) //can have the same address as long as the methods are different
    public String addNewWord(Model model, HttpServletRequest request) { //need the model to add the word to it as well as the request to know what to add to it
        String wordParam = request.getParameter("word"); //getting word from the request
        String defParam = request.getParameter("definition"); //getting definintion from the request
        if (wordParam != null && !wordParam.equals("")
                && defParam != null && !defParam.equals("")) {
            Word newWord = new Word(wordParam, defParam); //creating a new Word
            dao.addWord(newWord); //adding the new Word using the method in the DAO
        }
        model.addAttribute("words", dao.getAllWords()); //the main page will not have access to the List unless we put it in the model so that the jsp can render it into html
        return "index";
    }

    @RequestMapping(value = "/word/{wordParam}", method = RequestMethod.GET)
    @ResponseBody
    public Word getWord(@PathVariable("wordParam") String wordToGet) {
        return dao.getAllWords().stream().filter(w -> w.getWord().equalsIgnoreCase(wordToGet)).collect(Collectors.toList()).get(0);
    }

}
