package _076_076_Projects;

import java.util.Random;
import java.util.Scanner;

public class _076_BlackJack {

//    public static void main(String[] args) {
//        String draw;
//        draw = drawCard();
//        System.out.print(draw + " ");
//        draw = drawCard();
//        System.out.println(draw);
//    }

    public static void main(String[] args) {
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int draw = 0;
        int pHand = 0;
        int dHand = 0;
        boolean wantHit = true;
        boolean pBust = false;
        boolean dBust = false;

        System.out.println("Blackjack");
        draw = (2 + r.nextInt(10));
        dHand = draw;
        System.out.println("Dealer Hand: " + draw + " ?");
        draw = (2 + r.nextInt(10));
        pHand = draw;
        System.out.print("Player Hand: " + draw + " ");
        draw = (2 + r.nextInt(10));
        pHand = pHand + draw;
        System.out.println(draw + " (total: " + pHand + ")");
        System.out.println("(h)it/(s)tand");
        if ("s".equals(sc.nextLine())) {
            wantHit = false;
        } else {
            wantHit = true;
        }

        while (wantHit && pHand < 21) {
            draw = (2 + r.nextInt(10));
            pHand = pHand + draw;
            System.out.println("Player draws " + draw + " (total: " + pHand + ")");
            if (pHand > 21) {
                pBust = true;
                wantHit = false;
            } else if (pHand == 21) {
                pBust = false;
                wantHit = false;
            } else {
                pBust = false;
                wantHit = true;
                System.out.println("(h)it/(s)tand");
                if ("s".equals(sc.nextLine())) {
                    wantHit = false;
                } else {
                    wantHit = true;
                }
            }

        }

        draw = (2 + r.nextInt(10));
        dHand = dHand + draw;
        System.out.println("Dealer turns over " + draw + " (total: " + dHand + ")");

        if (!pBust && dHand < 17) {
            while (dHand < 17 && !dBust) {
                draw = (2 + r.nextInt(10));
                dHand = dHand + draw;
                System.out.println("Dealer draws " + draw + " (total: " + dHand + ")");
                if (dHand > 21) {
                    dBust = true;
                } else {
                    dBust = false;
                }
            }

        }

        System.out.println("Dealer Hand: " + dHand);
        System.out.println("Player Hand: " + pHand);
        if (pBust) {
            System.out.println("Player busted--dealer wins!");
        } else if (dBust && !pBust) {
            System.out.println("Dealer busted--player wins!");
        } else if (!pBust && !dBust && pHand > dHand) {
            System.out.println("Player wins!");
        } else if (!pBust && !dBust && pHand < dHand) {
            System.out.println("Dealer wins!");
        } else if (!pBust && !dBust && pHand == dHand) {
            System.out.println("It's a push!");
        }
//    public static String drawCard() {
//        Random cardDraw = new Random();
//        int card = cardDraw.nextInt(13);
//        if (card == 0) {
//            return ("A");
//        } else if (card == 1) {
//            return ("2");
//        } else if (card == 2) {
//            return ("3");
//        } else if (card == 3) {
//            return ("4");
//        } else if (card == 4) {
//            return ("5");
//        } else if (card == 5) {
//            return ("6");
//        } else if (card == 6) {
//            return ("7");
//        } else if (card == 7) {
//            return ("8");
//        } else if (card == 8) {
//            return ("9");
//        } else if (card == 9) {
//            return ("T");
//        } else if (card == 10) {
//            return ("J");
//        } else if (card == 11) {
//            return ("Q");
//        } else {
//            return ("K");
//        }
    }
}
