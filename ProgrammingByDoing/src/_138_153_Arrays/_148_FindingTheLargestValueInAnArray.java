/*
Write a program that creates an array which can hold ten values. Fill the array
with random numbers from 1 to 100. Display the values in the array on the
screen. Then use a linear search to find the largest value in the array, and
display that value.
 */
package _138_153_Arrays;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class _148_FindingTheLargestValueInAnArray {

    public static void main(String[] args) {
        int[] myArray = new int[10];
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int largest = 0;

        System.out.print("numbers in the array: {");
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = 1 + r.nextInt(100);
            System.out.print(myArray[i]);
            if (i < myArray.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("}");

        for (Integer i : myArray) {
            if (i > largest) {
                largest = i;
            }
        }

        System.out.println("The largest value in the array is " + largest + ".");
    }
}
