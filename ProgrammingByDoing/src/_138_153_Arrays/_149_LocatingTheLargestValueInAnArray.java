/*
Write a program that creates an array which can hold ten values. Fill the array
with random numbers from 1 to 100. Display the values in the array on the
screen. Then use a linear search to find the largest value in the array, and
display that value and its slot number.
 */
package _138_153_Arrays;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class _149_LocatingTheLargestValueInAnArray {

    public static void main(String[] args) {
        int[] myArray = new int[10];
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int largest = 0;
        ArrayList<Integer> locationsOfLargest = new ArrayList<>();

        System.out.print("numbers in the array: {");
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = 1 + r.nextInt(100);
            System.out.print(myArray[i]);
            if (i < myArray.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("}");

        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] > largest) {
                largest = myArray[i];
//                locationsOfLargest.add(i);
            }
        }

        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] == largest) {
                locationsOfLargest.add(i);
            }
        }

        System.out.print("The largest value in the array is " + largest + " and it is located at ");
        if (locationsOfLargest.size() == 1) {
            System.out.print("index ");
        } else {
            System.out.print("indices ");
        }

        for (int i = 0; i < locationsOfLargest.size(); i++) {
            System.out.print(locationsOfLargest.get(i));
            if (i < (locationsOfLargest.size() - 1)) {
                System.out.print(", ");
            }
        }
        System.out.println("");
    }

}
