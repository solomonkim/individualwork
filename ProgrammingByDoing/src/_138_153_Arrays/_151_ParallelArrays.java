/*
Create three arrays to store data about five people. The first array should be
Strings and should hold their last names. The next array should be doubles and
should hold a grade average (on a 100-point scale). The last array should be
ints, should hold their student id numbers. Give each of the arrays values
(using array initializers). Then print the values of all three arrays on the
screen. Finally, ask the user for an ID number to lookup. Search through the ID
array until you find that ID, and then print out the values from the same slot
number of the other two arrays.


 */
package _138_153_Arrays;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class _151_ParallelArrays {

    public static void main(String[] args) {

        String[] lastName = {"Kim", "Hill", "Wixson", "Burns", "Culp"};
        Double[] avg = {80.0, 85.0, 90.0, 95.0, 100.0};
        int[] id = {1000, 2000, 3000, 4000, 5000};
        Scanner sc = new Scanner(System.in);
        int keyIndex = 6;

        System.out.println("NAME\tAVG\tID");
        for (int i = 0; i < 5; i++) {
            System.out.println(lastName[i] + "\t" + avg[i] + "\t" + id[i]);
        }

        System.out.println("Enter a student's ID number:");
        int searchTerm = sc.nextInt();

        for (int i = 0; i < id.length; i++) {
            if (id[i] == searchTerm) {
                keyIndex = i;
            }
        }

        System.out.println("Found in slot " + keyIndex);
        System.out.println("Name: " + lastName[keyIndex]);
        System.out.println("Average: " + avg[keyIndex]);
        System.out.println("ID: " + id[keyIndex]);
    }
}
