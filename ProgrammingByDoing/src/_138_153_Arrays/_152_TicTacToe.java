/*
Code an interactive, two-player game of Tic-Tac-Toe. You'll use a two-
dimensional array of chars.
 */
package _138_153_Arrays;

import java.util.Scanner;

/**
 *
 * @author Solomon Kim
 */
public class _152_TicTacToe {

    private static char[][] board = new char[3][3];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        boolean firstPlayersTurn = true;
        boolean gameOver = false;
        boolean playerWins = false;
        boolean catsGame = false;
        int space = 0, row = 0, col = 0;

        initBoard();
        displayBoard();

        while (!playerWins) {

            //prompt the appropriate player to choose
            System.out.print("\nPlayer ");
            if (firstPlayersTurn) {
                System.out.print("1");
            } else {
                System.out.print("2");
            }
            System.out.println(", enter the space (1 - 9): ");

            //reset the choice
            space = 0;

            //validate user input
            while (space < 1 || space > 9) {
                space = sc.nextInt();
                if (space < 1 || space > 9) {
                    System.out.println("Invalid selection. Enter the space (1 - 9): ");
                }
            }

            //set the row and column according to the choice
            if (space == 1 || space == 2 || space == 3) {
                row = 0;
            }
            if (space == 4 || space == 5 || space == 6) {
                row = 1;
            }
            if (space == 7 || space == 8 || space == 9) {
                row = 2;
            }

            if (space == 1 || space == 4 || space == 7) {
                col = 0;
            }
            if (space == 2 || space == 5 || space == 8) {
                col = 1;
            }
            if (space == 3 || space == 6 || space == 9) {
                col = 2;
            }

            //mark the board with "X" if first player's turn, "O" if second player's
            if (firstPlayersTurn) {
                board[row][col] = 'X';
                firstPlayersTurn = false;
            } else {
                board[row][col] = 'O';
                firstPlayersTurn = true;
            }

            //check if either player has won
            if ((board[0][0] == board[0][1] && board[0][1] == board[0][2])
                    || (board[1][0] == board[1][1] && board[1][1] == board[1][2])
                    || (board[2][0] == board[2][1] && board[2][1] == board[2][2])
                    || (board[0][0] == board[1][0] && board[1][0] == board[2][0])
                    || (board[0][1] == board[1][1] && board[1][1] == board[2][1])
                    || (board[0][2] == board[1][2] && board[1][2] == board[2][2])
                    || (board[0][0] == board[1][1] && board[1][1] == board[2][2])
                    || (board[0][2] == board[1][1] && board[1][1] == board[2][0])) {
                playerWins = true;
                gameOver = true;
            } //check for a cat's game
            else if ((board[0][0] == 'X' || board[0][0] == 'O')
                    && (board[0][1] == 'X' || board[0][1] == 'O')
                    && (board[0][2] == 'X' || board[0][2] == 'O')
                    && (board[1][0] == 'X' || board[1][0] == 'O')
                    && (board[1][1] == 'X' || board[1][1] == 'O')
                    && (board[1][2] == 'X' || board[1][2] == 'O')
                    && (board[2][0] == 'X' || board[2][0] == 'O')
                    && (board[2][1] == 'X' || board[2][1] == 'O')
                    && (board[2][2] == 'X' || board[2][2] == 'O')) {
                catsGame = true;
                gameOver = true;
            }

            displayBoard();

            if (gameOver) {
                if (playerWins) {
                    System.out.print("Game over--player ");
                    if (firstPlayersTurn) {
                        System.out.print("2 ");
                    } else {
                        System.out.print("1 ");
                    }
                    System.out.println("wins.");
                }
                if (catsGame) {
                    System.out.println("Game over--it's a cat's game.");
                }
            }
        }
    }

    public static void initBoard() {
        //populate the array with the number choices that will correspond to the user's choice
        board[0][0] = '1';
        board[0][1] = '2';
        board[0][2] = '3';
        board[1][0] = '4';
        board[1][1] = '5';
        board[1][2] = '6';
        board[2][0] = '7';
        board[2][1] = '8';
        board[2][2] = '9';
    }

    public static void displayBoard() {
        System.out.println(board[0][0] + "|" + board[0][1] + "|" + board[0][2]);
        System.out.println("--+-+--");
        System.out.println(board[1][0] + "|" + board[1][1] + "|" + board[1][2]);
        System.out.println("--+-+--");
        System.out.println(board[2][0] + "|" + board[2][1] + "|" + board[2][2]);
    }
}
