/*
Write a program to play a word-guessing game like Hangman.
It must randomly choose a word from a list of words.
It must stop when all the letters are guessed.
It must give them limited tries and stop after they run out.
It must display letters they have already guessed (either only the incorrect guesses or all guesses).
 */
package _138_153_Arrays;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author Solomon Kim
 */
public class _153_Hangman {

    public static void main(String[] args) {

        String[] wordBank = {"ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIET", "KILO", "LIMA", "MIKE", "NOVEMBER", "OSCAR", "PAPA", "QUEBEC", "ROMEO", "SIERRA", "TANGO", "UNIFORM", "VICTOR", "WHISKEY", "XRAY", "YANKEE", "ZULU"};
        Random r = new Random();
        boolean gameOver = false;
        String wordToGuess = wordBank[r.nextInt(26)];
        char[] incorrectGuesses = new char[6];
        char[] puzzleSolution = wordToGuess.toCharArray();
        char[] puzzleInProgress = new char[puzzleSolution.length];
        Scanner sc = new Scanner(System.in);
        String guess;
        int numberOfIncorrectGuesses = 0;
        boolean alreadyGuessed;

        //populate puzzleInProgress with underscores which will be replaced with
        //letters as they're guessed; print the puzzle
        for (int i = 0; i < wordToGuess.length(); i++) {
            puzzleInProgress[i] = '_';
            System.out.print(puzzleInProgress[i] + " ");
        }
        System.out.println("");

        do {
            //prompt the user to guess a letter, make sure it's one character,
            //convert it to upper case, make sure it hasn't already been guessed
            System.out.println("Guess a letter...");
            do {
                alreadyGuessed = false;
                guess = sc.next().toUpperCase();
                for (int i = 0; i < incorrectGuesses.length; i++) {
                    if (incorrectGuesses[i] == guess.charAt(0)) {
                        alreadyGuessed = true;
                    }
                }
                if (guess.length() != 1 || alreadyGuessed || !Character.isLetter(guess.charAt(0))) {
                    System.out.println("Invalid input...");
                }
            } while (guess.length() != 1 || alreadyGuessed || !Character.isLetter(guess.charAt(0)));

            //if puzzleSolution contains guess, replace the underscores with guess
            if (wordToGuess.contains(guess)) {
                for (int i = 0; i < puzzleSolution.length; i++) {
                    if (guess.charAt(0) == puzzleSolution[i]) {
                        puzzleInProgress[i] = guess.charAt(0);
                    }
                }
            } //if puzzleSolution doesn't contain guess, store it in
            //incorrectGuesses and increment numberOfIncorrectGuesses
            else {
                incorrectGuesses[numberOfIncorrectGuesses] = guess.charAt(0);
                numberOfIncorrectGuesses++;
            }

            //print puzzle
            for (int i = 0; i < puzzleInProgress.length; i++) {
                System.out.print(puzzleInProgress[i] + " ");
                if (i == puzzleInProgress.length) {
                    System.out.println("");
                }
            }

            //print incorrect guesses
            System.out.print("\nincorrect guesses (" + (6 - numberOfIncorrectGuesses) + " left):");
            for (int i = 0; i < incorrectGuesses.length; i++) {
                System.out.print(incorrectGuesses[i] + " ");
            }
            System.out.println("\n");

            //check if the puzzle is solved or if 6 incorrect guesses have been made
            if (Arrays.equals(puzzleSolution, puzzleInProgress)) {
                System.out.println("Game over--you won! The word was " + wordToGuess + ".");
                gameOver = true;
            }
            if (numberOfIncorrectGuesses >= 6) {
                System.out.println("Game over--you lost! The word was " + wordToGuess + ".");
                gameOver = true;
            }
        } while (!gameOver);
    }
}