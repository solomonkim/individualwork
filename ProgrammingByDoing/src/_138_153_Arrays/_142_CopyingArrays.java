package _138_153_Arrays;

import java.util.Random;

public class _142_CopyingArrays {

    public static void main(String[] args) {
        Random r = new Random();
        int[] myArray = new int[10];
        int[] copyOfMyArray = new int[10];

        for (int i = 0; i < 10; i++) {
            myArray[i] = r.nextInt(100);
        }

        for (int i = 0; i < 10; i++) {
            copyOfMyArray[i] = myArray[i];
        }

        myArray[9] = -7;

        System.out.print("Array 1: ");
        for (int i = 0; i < 10; i++) {
            System.out.print(myArray[i] + " ");
        }

        System.out.println("");
        
        System.out.print("Array 2: ");
        for (int i = 0; i < 10; i++) {
            System.out.print(copyOfMyArray[i] + " ");
        }
    }
}
