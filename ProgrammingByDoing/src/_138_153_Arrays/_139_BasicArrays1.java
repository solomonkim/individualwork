package _138_153_Arrays;

public class _139_BasicArrays1 {
    public static void main(String[] args) {
        int[] myArray = new int[10];
        
        for (int i = 0; i < 10; i++) {
            myArray[i] = -113;
            System.out.println("Slot " + i + " contains " + myArray[i]);
        }
    }
}
