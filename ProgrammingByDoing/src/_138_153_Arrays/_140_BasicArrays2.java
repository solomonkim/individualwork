package _138_153_Arrays;

import java.util.Random;

public class _140_BasicArrays2 {
    public static void main(String[] args) {
        Random r = new Random();
        int[] myArray = new int[10];
        
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = 1 + r.nextInt(100);
            System.out.println("Slot " + i + " contains " + myArray[i]);
        }
    }
}
