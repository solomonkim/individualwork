/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _138_153_Arrays;

/**
 *
 * @author apprentice
 */
public class _150_GivingAnArrayABunchOfValuesAtOnce {

    public static void main(String[] args) {
        String[] arr1 = {"alpha", "bravo", "charlie", "delta", "echo"};

        System.out.print("The first array is filled with the following values:\n\t");
        for (int i = 0; i < arr1.length; i++) {
            System.out.print(arr1[i] + " ");
        }
        System.out.println();

        int[] arr2 = {11, 22, 33, 44, 55};
        System.out.print("The second array is filled with the following values:\n\t");
        for (int i = 0; i < arr2.length; i++) {
            System.out.print(arr2[i] + " ");
        }
        System.out.println();

    }
}
