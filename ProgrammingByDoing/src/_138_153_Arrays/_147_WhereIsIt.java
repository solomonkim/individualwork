/*
Create an array that can hold ten integers, and fill each slot with a different
random value from 1-50. Display those values on the screen, and then prompt the
user for an integer. Search through the array, and if the item is present, give
the slot number where it is located. If the value is not in the array, display a
single message saying so. If the value is present more than once, you may either
display the message as many times as necessary, or display a single message
giving the last slot number in which it appeared.
 */
package _138_153_Arrays;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class _147_WhereIsIt {

    public static void main(String[] args) {
        int[] myArray = new int[10];
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int searchTerm;
        boolean containsSearchTerm = false;
        ArrayList<Integer> indicesContainingSearchTerm = new ArrayList<>();

        System.out.print("numbers in the array: {");
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = 1 + r.nextInt(50);
            System.out.print(myArray[i]);
            if (i < myArray.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("}");

        System.out.println("Choose a number...");
        searchTerm = sc.nextInt();

        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] == searchTerm) {
                indicesContainingSearchTerm.add(i);
                containsSearchTerm = true;
            }
        }
        System.out.print(searchTerm + " is");
        if (!containsSearchTerm) {
            System.out.print(" not");
        }
        System.out.println(" in the array.");
        if (containsSearchTerm) {
            System.out.print(searchTerm + " is found at the following ");
            if (indicesContainingSearchTerm.size() == 1) {
                System.out.print("index: ");
            } else {
                System.out.print("indices: ");
            }
            for (Integer i : indicesContainingSearchTerm) {
                System.out.print(i);
                if (i < indicesContainingSearchTerm.size()) {
                    System.out.print(", ");
                }
            }
        }
        System.out.println("");
    }

}
