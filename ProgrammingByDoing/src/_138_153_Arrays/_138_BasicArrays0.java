package _138_153_Arrays;

public class _138_BasicArrays0 {
    public static void main(String[] args) {
        int[] myArray = new int[10];
        
        myArray[0] = -113;
        myArray[1] = -113;
        myArray[2] = -113;
        myArray[3] = -113;
        myArray[4] = -113;
        myArray[5] = -113;
        myArray[6] = -113;
        myArray[7] = -113;
        myArray[8] = -113;
        myArray[9] = -113;
        
        System.out.println("Slot 0 contains " + myArray[0]);
        System.out.println("Slot 1 contains " + myArray[1]);
        System.out.println("Slot 2 contains " + myArray[2]);
        System.out.println("Slot 3 contains " + myArray[3]);
        System.out.println("Slot 4 contains " + myArray[4]);
        System.out.println("Slot 5 contains " + myArray[5]);
        System.out.println("Slot 6 contains " + myArray[6]);
        System.out.println("Slot 7 contains " + myArray[7]);
        System.out.println("Slot 8 contains " + myArray[8]);
        System.out.println("Slot 9 contains " + myArray[9]);
    }
}
