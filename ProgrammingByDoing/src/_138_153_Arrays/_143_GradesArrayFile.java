package _138_153_Arrays;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Random;
import java.util.Scanner;

public class _143_GradesArrayFile {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        int[] grades = new int[5];
        
        
        System.out.println("What is your name?");
        String name = sc.nextLine();
        
        System.out.println("What do you want your file name to be?");
        String fileName = sc.nextLine();
        PrintWriter out = new PrintWriter(new FileWriter(fileName));
        
        for (int i = 0; i < grades.length; i++) {
            grades[i] = 1 + r.nextInt(100);
        }
        
        out.println(name);
        out.println(fileName);
        out.println("Here are your randomly selected grades:");
        out.println("Grade 1: " + grades[0]);
        out.println("Grade 2: " + grades[1]);
        out.println("Grade 3: " + grades[2]);
        out.println("Grade 4: " + grades[3]);
        out.println("Grade 5: " + grades[4]);
        out.close();

    }
}
