package _138_153_Arrays;

import java.util.Random;

public class _141_BasicArrays3 {
    public static void main(String[] args) {
        Random r = new Random();
        int[] myArray = new int[1000];
        
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = 10 + r.nextInt(90);
            System.out.print(myArray[i] + "  ");
            if ((i + 1) % 20 == 0)
                System.out.println("\n");
        }
    }
}
