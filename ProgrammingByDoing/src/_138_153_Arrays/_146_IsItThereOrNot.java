/*
Create an array that can hold ten integers, and fill each slot with a different
random value from 1-50. Display those values on the screen, and then prompt the
user for an integer. Search through the array, and if the item is present, say
so. If the value is not in the array, display a single message saying so. Just
like the previous assignment, if the value is present more than once, you may
display the message as many times as necessary.
 */
package _138_153_Arrays;

import java.util.Random;
import java.util.Scanner;

public class _146_IsItThereOrNot {

    public static void main(String[] args) {

        int[] myArray = new int[10];
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int searchTerm;
        boolean containsSearchTerm = false;

        System.out.print("numbers in the array: {");
        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = 1 + r.nextInt(50);
            System.out.print(myArray[i]);
            if (i < myArray.length - 1) {
                System.out.print(", ");
            }
        }
        System.out.println("}");

        System.out.println("Choose a number...");
        searchTerm = sc.nextInt();

        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] == searchTerm) {
                containsSearchTerm = true;
            }
        }
        System.out.print(searchTerm + " is");
        if (!containsSearchTerm) {
            System.out.print(" not");
        }
        System.out.println(" in the array.");
    }
}
