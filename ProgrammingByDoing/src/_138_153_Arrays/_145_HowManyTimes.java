package _138_153_Arrays;

import java.util.Random;
import java.util.Scanner;

public class _145_HowManyTimes {
    public static void main(String[] args) {
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int[] myArray = new int[10];
        int instances = 0;

        for (int i = 0; i < myArray.length; i++) {
            myArray[i] = 1 + r.nextInt(50);
            System.out.print(myArray[i] + " ");
        }
        
        System.out.println("Choose a number between 1 and 50 to search for.");
        int searchTerm = sc.nextInt();
        
        for (int i = 0; i < myArray.length; i++) {
            if (myArray[i] == searchTerm)
                instances++;
        }
        
        System.out.print(searchTerm + " was found " + instances + " time");
        if (instances == 1)
            System.out.println(".");
        else
            System.out.println("s.");
    }
}
