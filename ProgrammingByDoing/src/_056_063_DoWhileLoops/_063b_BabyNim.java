package _056_063_DoWhileLoops;

import java.util.Scanner;

public class _063b_BabyNim {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int pile1 = 3;
        int pile2 = 3;
        int pile3 = 3;
        int choice, take;
        boolean noEmptyPiles = true;

        while (noEmptyPiles) {
            System.out.println("pile 1: " + pile1);
            System.out.println("pile 2: " + pile2);
            System.out.println("pile 3: " + pile3);
            System.out.println("Which pile do you want to take from? 1/2/3");
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("How many stones do you want to take from pile 1?");
                    take = sc.nextInt();
                    pile1 -= take;
                    if (pile1 <= 0) {
                        noEmptyPiles = false;
                    }
                    break;
                case 2:
                    System.out.println("How many stones do you want to take from pile 2?");
                    take = sc.nextInt();
                    pile2 -= take;
                    if (pile2 <= 0) {
                        noEmptyPiles = false;
                    }
                    break;
                case 3:
                    System.out.println("How many stones do you want to take from pile 3?");
                    take = sc.nextInt();
                    pile3 -= take;
                    if (pile3 <= 0) {
                        noEmptyPiles = false;
                    }
                    break;
            }
        }
        System.out.println("Game Over");
        System.out.println("pile 1: " + pile1);
        System.out.println("pile 2: " + pile2);
        System.out.println("pile 3: " + pile3);
    }
}
