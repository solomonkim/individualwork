package _056_063_DoWhileLoops;

import java.util.Scanner;

public class _060_SafeSquareRoot {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double x = 0;

        System.out.println("Square Root");
        System.out.println("Enter a number for which to find the square root:");
        while (x <= 0) {
            x = sc.nextDouble();
            if (x < 0) {
                System.out.println("Cannot take the square root of a negative number. Choose a non-negative number:");
            } else {
                System.out.println("The square root of " + x + " is " + Math.sqrt(x));
            }
        }
    }
}
