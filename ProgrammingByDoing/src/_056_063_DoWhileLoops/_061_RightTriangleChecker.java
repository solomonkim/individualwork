package _056_063_DoWhileLoops;

import java.util.Scanner;

public class _061_RightTriangleChecker {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        double sideA, sideB, sideC;

        System.out.println("Right Triangle Checker");
        System.out.println("Enter the lengths of three sides of a triangle IN ASCENDING ORDER.");

        System.out.println("side A:");
        sideA = sc.nextDouble();
        while (sideA <= 0) {
            System.out.println("Side A must be a positive number.");
            System.out.println("side A:");
            sideA = sc.nextDouble();
        }
        
        System.out.println("side B:");
        sideB = sc.nextDouble();
        while (sideA > sideB) {
            System.out.println("Side B must be greater than or equal to side A.");
            System.out.println("side B:");
            sideB = sc.nextDouble();
        }
        
        System.out.println("side C:");
        sideC = sc.nextDouble();
        while (sideB > sideC) {
            System.out.println("Side C must be greater than or equal to side B.");
            System.out.println("side C:");
            sideC = sc.nextDouble();
        }
        if (((sideA) * (sideA)) + ((sideB) * (sideB)) == (sideC)*(sideC)) {
            System.out.println("This is a right triangle.");
        } else {
            System.out.println("This is NOT a right triangle.");
        }
    }
}
