package _056_063_DoWhileLoops;

import java.util.Random;
import java.util.Scanner;

public class _059_NumberGuessingDoWhile {

    public static void main(String[] args) {
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int num = 1 + r.nextInt(10);
        int guess;
        int numGuesses = 0;

        System.out.println("I'm thinking of a number between 1 and 10. Guess!");

        do {
            guess = sc.nextInt();
            numGuesses++;
            if (num != guess) {
                System.out.println("Nope! Try again...");
            }
        } while (num != guess);

        System.out.print("That's right! The number was " + num + ". It took you " + numGuesses + " guess");
        if (numGuesses > 1) {
            System.out.println("es.");
        } else {
            System.out.println(".");
        }
    }
}
