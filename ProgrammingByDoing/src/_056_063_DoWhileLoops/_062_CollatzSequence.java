package _056_063_DoWhileLoops;

import java.util.Scanner;

public class _062_CollatzSequence {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int cols = 0;
        int largestVal = 0;

        System.out.println("Enter an integer.");
        int x = sc.nextInt();
        System.out.println("Starting number: " + x);

        do {
            if (x % 2 == 0) {//even
                x = (x / 2);
                System.out.print(x + "\t");
                cols++;
                if (cols % 6 == 0) {
                    System.out.println("");
                }
                if (x > largestVal) {
                    largestVal = x;
                }
            } else {//odd
                x = ((3 * x) + 1);
                System.out.print(x + "\t");
                cols++;
                if (cols % 6 == 0) {
                    System.out.println("");
                }
                if (x > largestVal) {
                    largestVal = x;
                }
            }
        } while (x != 1);
        System.out.println("");
        System.out.println("Terminated after " + cols + " steps and the largest value was " + largestVal + ".");
    }
}
