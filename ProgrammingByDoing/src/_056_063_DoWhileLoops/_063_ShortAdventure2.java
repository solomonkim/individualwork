package _056_063_DoWhileLoops;

import java.util.Scanner;

public class _063_ShortAdventure2 {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        int nextroom = 1;
        String choice = "";

        while (nextroom != 0) {
            if (nextroom == 1) {
                System.out.println("Foyer");
                System.out.println("You're in the foyer. Nice and warm in this house. Want to go out back out the \"door\" you came in through, to the \"kitchen\", or \"upstairs\"?");
                choice = keyboard.nextLine();
                if (choice.equals("kitchen")) {
                    nextroom = 2;
                } else if (choice.equals("upstairs")) {
                    nextroom = 3;
                } else if (choice.equals("door")) {
                    nextroom = 0;
                } else {
                    System.out.println("ERROR.");
                }
            }
            if (nextroom == 2) {
                System.out.println("Kitchen");
                System.out.println("You're in the kitchen. Smells like Lysol. Want to go back to the \"foyer\", or crawl into the laundry \"chute\"?");
                choice = keyboard.nextLine();
                if (choice.equals("foyer")) {
                    nextroom = 1;
                } else if (choice.equals("chute")) {
                    nextroom = 4;
                } else {
                    System.out.println("ERROR.");
                }

            }
            if (nextroom == 3) {
                System.out.println("Upstairs");
                System.out.println("You went upstairs. Your ears popped on account of the elevation change. Want to go back down to the \"foyer\", into the \"bedroom\" or into the \"bathroom?\"");
                choice = keyboard.nextLine();
                if (choice.equals("foyer")) {
                    nextroom = 1;
                } else if (choice.equals("bedroom")) {
                    nextroom = 4;
                } else if (choice.equals("bathroom")) {
                    nextroom = 5;
                } else {
                    System.out.println("ERROR.");
                }

            }
            if (nextroom == 4) {
                System.out.println("Bedroom");
                System.out.println("Welcome to the bedroom--where the magic happens. Want to slide down the laundry \"chute\", go out on to the \"balcony\", or to the \"upstairs\" landing?");
                choice = keyboard.nextLine();
                if (choice.equals("chute")) {
                    nextroom = 2;
                } else if (choice.equals("balcony")) {
                    nextroom = 6;
                } else if (choice.equals("upstairs")) {
                    nextroom = 3;
                } else {
                    System.out.println("ERROR.");
                }
            }

            if (nextroom == 5) {
                System.out.println("Bathroom");
                System.out.println("You're in the bathroom. It has a sink and a toilet. Fascinating. Type \"upstairs\" to go back out to the landing.");
                choice = keyboard.nextLine();
                if (choice.equals("upstairs")) {
                    nextroom = 3;
                } else {
                    System.out.println("ERROR.");
                }

            }
            if (nextroom == 6) {
                System.out.println("Balcony");
                System.out.print("You're on the balcony... ");
                nextroom = 0;
            }
        }
        System.out.println("You made it outside. You've reached the pinnacle of human achievement.");
    }
}
