package _056_063_DoWhileLoops;

import java.util.Scanner;

public class _063c_Nim {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int pile1 = 3;
        int pile2 = 3;
        int pile3 = 3;
        int choice, take;
        boolean noEmptyPiles = true;
        String player1, player2;
        boolean player1Turn = true;

        System.out.println("Player 1: Enter your name.");
        player1 = sc.nextLine();
        System.out.println("Player 2: Enter your name.");
        player2 = sc.nextLine();

        while (noEmptyPiles) {
            System.out.println("pile 1: " + pile1);
            System.out.println("pile 2: " + pile2);
            System.out.println("pile 3: " + pile3);
            if (player1Turn){
                System.out.print(player1);
            } else {
                System.out.print(player2);
            }
            System.out.println(", which pile do you want to take from? 1/2/3");
            choice = sc.nextInt();
            switch (choice) {
                case 1:
                    System.out.println("How many stones do you want to take from pile 1?");
                    take = sc.nextInt();
                    pile1 -= take;
                    if (pile1 <= 0) {
                        noEmptyPiles = false;
                    }
                    break;
                case 2:
                    System.out.println("How many stones do you want to take from pile 2?");
                    take = sc.nextInt();
                    pile2 -= take;
                    if (pile2 <= 0) {
                        noEmptyPiles = false;
                    }
                    break;
                case 3:
                    System.out.println("How many stones do you want to take from pile 3?");
                    take = sc.nextInt();
                    pile3 -= take;
                    if (pile3 <= 0) {
                        noEmptyPiles = false;
                    }
                    break;
            }
            if (player1Turn) {
                player1Turn = false;
            } else {
                player1Turn = true;
            }
        }
        System.out.println("Game Over");
        if (player1Turn) {
            System.out.print(player1);
        } else {
            System.out.print(player2);
        }
        System.out.println(" wins!");
        System.out.println("pile 1: " + pile1);
        System.out.println("pile 2: " + pile2);
        System.out.println("pile 3: " + pile3);
    }
}
