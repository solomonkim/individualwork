package _056_063_DoWhileLoops;

import java.util.Random;

public class _058_ShorterDoubleDice {

    public static void main(String[] args) {
        Random r = new Random();
        int die1, die2, diceTotal;

        do {
            die1 = 1 + r.nextInt(6);
            die2 = 1 + r.nextInt(6);
            diceTotal = die1 + die2;
            System.out.println("die 1: " + die1);
            System.out.println("die 2: " + die2);
            System.out.println("total: " + diceTotal);
            System.out.println("");
        } while (die1 != die2);
    }
}
