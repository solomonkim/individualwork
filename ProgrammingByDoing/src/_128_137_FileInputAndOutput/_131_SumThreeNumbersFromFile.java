package _128_137_FileInputAndOutput;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class _131_SumThreeNumbersFromFile {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("3nums.txt")));
        int num1 = sc.nextInt(), num2 = sc.nextInt(), num3 = sc.nextInt();
        System.out.println(num1 + " + " + num2 + " + " + num3 + " = " + (num1 + num2 + num3));
    }
}
