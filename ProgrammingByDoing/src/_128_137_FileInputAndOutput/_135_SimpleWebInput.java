package _128_137_FileInputAndOutput;

import java.net.URL;
import java.util.Scanner;

public class _135_SimpleWebInput {

    public static void main(String[] args) throws Exception {
        URL mcool = new URL("https://cs.leanderisd.org/mitchellis.txt");
        Scanner webIn = new Scanner(mcool.openStream());

        while (webIn.hasNextLine()) {
            System.out.println(webIn.nextLine());
        }
    }
}
