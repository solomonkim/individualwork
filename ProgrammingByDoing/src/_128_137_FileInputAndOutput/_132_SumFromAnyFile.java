package _128_137_FileInputAndOutput;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class _132_SumFromAnyFile {

    public static void main(String[] args) throws Exception {
        Scanner consoleSc = new Scanner(System.in);
        System.out.print("Which file would you like to read numbers from? ");
        Scanner fileSc = new Scanner(new BufferedReader(new FileReader(consoleSc.nextLine())));
        int num1 = fileSc.nextInt(), num2 = fileSc.nextInt(), num3 = fileSc.nextInt();
        System.out.println(num1 + " + " + num2 + " + " + num3 + " = " + (num1 + num2 + num3));
    }
}
