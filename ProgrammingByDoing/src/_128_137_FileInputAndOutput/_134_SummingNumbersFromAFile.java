package _128_137_FileInputAndOutput;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class _134_SummingNumbersFromAFile {

    public static void main(String[] args) throws Exception {
        Scanner consoleSc = new Scanner(System.in);
        int sum = 0;
        System.out.println("Which file would you like to read numbers from: ");
        Scanner fileSc = new Scanner(new BufferedReader(new FileReader(consoleSc.nextLine())));
        while (fileSc.hasNextInt()) {
            sum += fileSc.nextInt();
        }
        System.out.println("The sum of the numbers in the file is " + sum + ".");
    }
}
