package _128_137_FileInputAndOutput;

import java.io.FileWriter;
import java.io.PrintWriter;

public class _128_LetterRevisited {

    public static void main(String[] args) throws Exception {
        PrintWriter out = new PrintWriter(new FileWriter("OutFile.txt"));
        out.println("+-----------------------------------------------+");
        out.println("|                                           ####|");
        out.println("|                                           ####|");
        out.println("|                                           ####|");
        out.println("|                                               |");
        out.println("|                                               |");
        out.println("|                SOLOMON KIM                    |");
        out.println("|                600 MARSHALL ST APT 128        |");
        out.println("|                LOUISVILLE KY 40202            |");
        out.println("|                                               |");
        out.println("|                                               |");
        out.println("+-----------------------------------------------+");
        out.close();
    }
}
