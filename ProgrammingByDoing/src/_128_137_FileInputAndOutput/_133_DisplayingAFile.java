package _128_137_FileInputAndOutput;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class _133_DisplayingAFile {

    public static void main(String[] args) throws Exception {
        Scanner consoleSc = new Scanner(System.in);

        System.out.println("Open which file: ");
        Scanner fileSc = new Scanner(new BufferedReader(new FileReader(consoleSc.nextLine())));
        while (fileSc.hasNextLine()) {
            System.out.println(fileSc.nextLine());
        }
    }

}
