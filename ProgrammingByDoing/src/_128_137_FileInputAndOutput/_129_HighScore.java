package _128_137_FileInputAndOutput;

import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Scanner;

public class _129_HighScore {

    public static void main(String[] args) throws Exception {
        PrintWriter out = new PrintWriter(new FileWriter("HighScore.txt"));
        Scanner sc = new Scanner(System.in);

        System.out.print("What's your name? ");
        String name = sc.nextLine();
        System.out.print("What's your high score? ");
        String highScore = sc.nextLine();
        out.println("name\t\tscore");
        out.println(name + "\t\t" + highScore);
        out.close();
    }
}
