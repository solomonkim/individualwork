package _128_137_FileInputAndOutput;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Scanner;

public class _130_SimpleFileInput {

    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(new BufferedReader(new FileReader("name.txt")));
        System.out.println("Your name is " + sc.nextLine() + ".");
    }
}
