package Default;

public class VariablesAndNames {

    public static void main(String[] args) {
        int cars, drivers, passengers, cars_not_driven, cars_driven;
        double space_in_a_car, carpool_capacity, average_passengers_per_car;

        //this is a variable assignment
        cars = 100;
        
        //this is a variable assignment
        space_in_a_car = 4.0; //changing this to 4 makes no difference since it
        //is declared as a double
        
        //this is a variable assignment
        drivers = 30;
        
        //this is a variable assignment
        passengers = 90;
        
        //this is a variable assignment
        cars_not_driven = cars - drivers;
        
        //this is a variable assignment
        cars_driven = drivers;
        
        //this is a variable assignment
        carpool_capacity = cars_driven * space_in_a_car;
        
        //this is a variable assignment
        average_passengers_per_car = passengers / cars_driven;

        System.out.println("There are " + cars + " cars available.");
        System.out.println("There are only " + drivers + " drivers available.");
        System.out.println("There will be " + cars_not_driven + " empty cars today.");
        System.out.println("We can transport " + carpool_capacity + " people today.");
        System.out.println("We have " + passengers + " to carpool today.");
        System.out.println("We need to put about " + average_passengers_per_car + " in each car.");
    }
}
