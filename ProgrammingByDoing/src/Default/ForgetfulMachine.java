package Default;

import java.util.Scanner;

public class ForgetfulMachine {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        
        System.out.println("Give me a word. ");
        keyboard.next();
        
        System.out.println("Give me another word. ");
        keyboard.next();
        
        System.out.println("Now give me a number. ");
        keyboard.nextDouble();
        
        System.out.println("Give me another number. ");
        keyboard.nextDouble();
        
        System.out.println("Wasn't that fun?");
    }
}
