package Default;

import java.util.Scanner;

public class MoreUserInput {

    public static void main(String[] args) {
        String firstName;
        String lastName;
        int grade;
        String studentID;
        String loginName;
        float GPA;
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Please enter the following information...");
        System.out.println("First Name: ");
        firstName = keyboard.next();
        System.out.println("Last Name: ");
        lastName = keyboard.next();
        System.out.println("Grade (9-12): ");
        grade = keyboard.nextInt();
        System.out.println("Student ID: ");
        studentID = keyboard.next();
        System.out.println("Login Name: ");
        loginName = keyboard.next();
        System.out.println("Grade Point Average: ");
        GPA = keyboard.nextFloat();
        System.out.println("Your information");
        System.out.println("First Name: " + firstName);
        System.out.println("Last Name: " + lastName);
        System.out.println("Grade: " + grade);
        System.out.println("Student ID: " + studentID);
        System.out.println("Login Name: " + loginName);
        System.out.println("Grade Point Average: " + GPA);
    }
}
