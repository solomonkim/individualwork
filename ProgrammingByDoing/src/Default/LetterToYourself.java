package Default;

public class LetterToYourself {

    public static void main(String[] args) {
        System.out.println("+-----------------------------------------------+");
        System.out.println("|                                           ####|");
        System.out.println("|                                           ####|");
        System.out.println("|                                           ####|");
        System.out.println("|                                               |");
        System.out.println("|                                               |");
        System.out.println("|                SOLOMON KIM                    |");
        System.out.println("|                600 MARSHALL ST APT 128        |");
        System.out.println("|                LOUISVILLE KY 40202            |");
        System.out.println("|                                               |");
        System.out.println("|                                               |");
        System.out.println("+-----------------------------------------------+");
    }

}
