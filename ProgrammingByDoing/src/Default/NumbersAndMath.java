package Default;

public class NumbersAndMath {

    public static void main(String[] args) {
        System.out.println("I will now count my chickens:");

        //divides 30 by 6 then adds 25
        System.out.println("Hens " + (25 + 30 / 6));

        //multiplies 25 and 3, takes the remainder when dividing the product by
        //4, then subtracts the result from 100: 75%4 = 3, 100 - 3 = 97
        System.out.println("Roosters " + (100 - 25 * 3 % 4));

        System.out.println("Now I will count the eggs:");

        //subtract 4 % 2 (0), subtract 1 / 4 (-.25), add 3 + 2 + 1 - 5 + 6 (7)
        //round up from 6.75 to 7
        System.out.println(3.0 + 2.0 + 1.0 - 5.0 + 4.0 % 2.0 - 1.0 / 4.0 + 6.0);
        //add ".0" to integers to make them floating point numbers
        
        //compare the sum of 3 and 2 to the difference of 5 and 7
        System.out.println("Is it true that 3 + 2 < 5 - 7?");
        System.out.println(3 + 2 < 5 - 7);

        //prints the question then concatenates it with the evaluation
        System.out.println("What is 3 + 2? " + (3 + 2));
        System.out.println("What is 5 - 7? " + (5 - 7));
        System.out.println("Oh, that's why it's false.");
        System.out.println("How about some more.");

        //prints the question then prints the result of "greater than," "less
        //than," "greater "than or equal to," and "less than or equal to"
        //evaluations
        System.out.println("Is it greater? " + (5 > -2));
        System.out.println("Is it greater or equal? " + (5 >= -2));
        System.out.println("Is it less or equal? " + (5 <= -2));
    }

}
