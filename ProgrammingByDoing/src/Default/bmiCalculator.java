package Default;

import java.util.Scanner;

public class bmiCalculator {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        double heightFeetImperial;
        double heightInchesImperial;
        double weightImperial;

        System.out.println("What is your height? feet: ");
        heightFeetImperial = keyboard.nextDouble();
        System.out.println("inches: ");
        heightInchesImperial = keyboard.nextDouble();
        System.out.println("What is your weight (in lbs)? ");
        weightImperial = keyboard.nextDouble();

        double heightMetric = ((heightFeetImperial * 12) + heightInchesImperial) * 0.0254;
        double weightMetric = weightImperial * 0.454;
        double bmi = weightMetric / (heightMetric * heightMetric);

        System.out.println("Your BMI is " + bmi + ".");
    }

}
