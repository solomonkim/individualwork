package Default;

public class StillUsingVariables {

    public static void main(String[] args) {
        int yearOfGraduation = 2007;
        String myName = "Solomon Kim";

        System.out.println("My name is " + myName + " and I graduated in " + yearOfGraduation + ".");
    }
}
