package Default;

import java.util.Scanner;

public class AgeInFiveYears {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String name;
        int age;
        
        System.out.println("Hi. What's your name?");
        name = keyboard.next();
        System.out.println("And what's your age?");
        age = keyboard.nextInt();
        
        int agePlusFive = age + 5;
        int ageMinusFive = age - 5;
        System.out.println("Check it, " + name + "...");
        System.out.println("Five years ago, you were " + ageMinusFive + ".");
        System.out.println("In five years, you'll be " + agePlusFive + ".");
        System.out.println("How about that?");
         
    }

}
