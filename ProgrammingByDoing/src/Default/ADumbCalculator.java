package Default;

import java.util.Scanner;

public class ADumbCalculator {

    public static void main(String[] args) {

        double num1;
        double num2;
        double num3;
        Scanner keyboard = new Scanner(System.in);

        System.out.println("What is your first number? ");
        num1 = keyboard.nextDouble();
        System.out.println("What is your second number? ");
        num2 = keyboard.nextDouble();
        System.out.println("What is your third number? ");
        num3 = keyboard.nextDouble();
        double result = (num1 + num2 + num3) / 2;
        System.out.println("(" + num1 + " + " + num2 + " + " + num3 + ")/2 = " + result);
    }
}
