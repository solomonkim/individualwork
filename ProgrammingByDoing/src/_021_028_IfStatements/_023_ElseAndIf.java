package _021_028_IfStatements;

public class _023_ElseAndIf {

    public static void main(String[] args) {
        int people = 30;
        int cars = 40;
        int buses = 15;

        if (cars > people) {
            System.out.println("We should take the cars.");
        } else if (cars < people) //"else if" triggers if the "if" statement
        //does not trigger and if the condition in parentheses is met. removing
        //"else" turns this into a new "if" statement. if the condition in the
        //first "if" statement (cars > people) doesn't trigger, nothing happens
        //and we move forward to the second "if" statement (cars < people). for
        //this specific program, there is no difference in the output.
        {
            System.out.println("We should not take the cars.");
        } else //triggers if the conditions in the "if" and "else if" statements
        //do not trigger
        {
            System.out.println("We can't decide.");
        }

        if (buses > cars) {
            System.out.println("That's too many buses.");
        } else if (buses < cars) {
            System.out.println("Maybe we could take the buses.");
        } else {
            System.out.println("We still can't decide.");
        }

        if (people > buses) {
            System.out.println("All right, let's just take the buses.");
        } else {
            System.out.println("Fine, let's stay home then.");
        }

    }
}
