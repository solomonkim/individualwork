package _021_028_IfStatements;

import java.util.Scanner;

public class _027_ALittleQuiz {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int answer;
        int numCorrect = 0;

        System.out.println("Let's take the easiest quiz in the history of quizzes!");

        System.out.println("");
        System.out.println("Question 1: Which Isotope of Uranium is used in Nuclear reactors?");
        System.out.println("1. Uranium 234");
        System.out.println("2. Uranium 235");
        System.out.println("3. Uranium 238");
        answer = sc.nextInt();
        if (answer == 2) {
            numCorrect++;
            System.out.println("That's correct!");
        } else {
            System.out.println("Sorry, the correct answer is 2: Uranium 235.");
        }

        System.out.println("");
        System.out.println("Question 2: By what name is A.L. Dupin Dudevant best known?");
        System.out.println("1. George Sand");
        System.out.println("2. Isak Denisen");
        System.out.println("3. Guillaume Apollinaire");
        answer = sc.nextInt();
        if (answer == 1) {
            numCorrect++;
            System.out.println("That's correct!");
        } else {
            System.out.println("Sorry, the correct answer is 1: George Sand.");
        }

        System.out.println("");
        System.out.println("Question 3: Which of the following is true of the Congress of Vienna?");
        System.out.println("1. Metternich refused to relinquish any conquered territory.");
        System.out.println("2. France was not represented in the negotiations.");
        System.out.println("3. It contributed to a century without generalized war.");
        answer = sc.nextInt();
        if (answer == 3) {
            numCorrect++;
            System.out.println("That's correct!");
        } else {
            System.out.println("Sorry, the correct answer is 3: It contributed to a century without generalized war.");
        }

        System.out.println("");
        System.out.println("You answered " + numCorrect + " out of 3 questions correctly.");
        System.out.println("Thanks for playing!");
    }
}
