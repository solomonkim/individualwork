package _021_028_IfStatements;

public class _021_WhatIf {

    public static void main(String[] args) {
        int people = 20;
        int cats = 20; //changed from 30 to 20 in order to prevent either
        //statement from printing
        int dogs = 15;

        if (people < cats) {
            //"if" statements only run the next block of code
            //if the condition in the parentheses is met
            System.out.println("Too many cats!  The world is doomed!");
        }

        if (people > cats) { //the curly braces mark the beginning and end of
            //the code to be run if the condition in the parentheses is met
            System.out.println("Not many cats!  The world is saved!");
        }

        if (people < dogs) {
            System.out.println("The world is drooled on!");
        }

        if (people > dogs) {
            System.out.println("The world is dry!");
        }

        dogs += 5;

        if (people >= dogs) {
            System.out.println("People are greater than or equal to dogs.");
        }

        if (people <= dogs) {
            System.out.println("People are less than or equal to dogs.");
        }

        if (people == dogs) {
            System.out.println("People are dogs.");
        }
    }
}