/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _021_028_IfStatements;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class _025_HowOldAreYouSpecifically {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("What is your name? ");
        String name = sc.nextLine();
        System.out.println("Okay, " + name + ". How old are you?");
        int age = sc.nextInt();
        if (age < 16) {
            System.out.println("You can't drive, " + name + ".");
        } else if (age < 18) {
            System.out.println("You can drive but you can't vote, " + name + ".");
        } else if (age < 25) {
            System.out.println("You can drive and vote but you can't rent a car, " + name + ".");
        } else {
            System.out.println("You can pretty much do anything, " + name + ".");
        }
    }

}
