package _021_028_IfStatements;

import java.util.Scanner;

public class _026_SpaceBoxing {

    public static void main(String[] args) {
        System.out.println("Please enter your current Earth weight in pounds. ");
        Scanner sc = new Scanner(System.in);
        double weight = sc.nextInt();
        System.out.println("Which planet will you be visiting? (choose a number)");
        System.out.println("1. Venus      2. Mars      3. Jupiter");
        System.out.println("4. Saturn     5. Uranus    6. Neptune");
        int planet = sc.nextInt();
        if (planet == 1) {
            System.out.println("Your weight on Venus would be " + weight * 0.78 + " pounds.");
        } else if (planet == 2) {
            System.out.println("Your weight on Mars would be " + weight * 0.39 + " pounds.");
        } else if (planet == 3) {
            System.out.println("Your weight on Jupiter would be " + weight * 2.65 + " pounds.");
        } else if (planet == 4) {
            System.out.println("Your weight on Saturn would be " + weight * 1.17 + " pounds.");
        } else if (planet == 5) {
            System.out.println("Your weight on Uranus would be " + weight * 1.05 + " pounds.");
        } else if (planet == 6) {
            System.out.println("Your weight on Neptune would be " + weight * 1.23 + " pounds.");
        } else {
            System.out.println("That was not a valid selection.");
        }
    }
}
