package _021_028_IfStatements;

import java.util.Scanner;

public class _022_HowOldAreYou {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("How old are you?");
        int age = sc.nextInt();

        if (age < 16) {
            System.out.println("You can't drive.");
        }

        if (age < 18) {
            System.out.println("You can't vote.");
        }

        if (age < 25) {
            System.out.println("You can't rent a car.");
        }

        if (age >= 25) {
            System.out.println("You can do anything that's legal.");
        }
    }
}
