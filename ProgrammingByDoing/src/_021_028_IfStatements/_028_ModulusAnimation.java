package _021_028_IfStatements;

public class _028_ModulusAnimation {

    public static void main(String[] args) throws Exception {

        int repeats = 5;

        for (int i = 0; i < repeats * 11; i++) {
            if (i % 11 == 0) {
                System.out.print("I <3 JAVA! \n");
            } else if (i % 11 == 1) {
                System.out.print(" <3 JAVA! I\n");
            } else if (i % 11 == 2) {
                System.out.print("<3 JAVA! I \n");
            } else if (i % 11 == 3) {
                System.out.print("3 JAVA! I <\n");
            } else if (i % 11 == 4) {
                System.out.print(" JAVA! I <3\n");
            } else if (i % 11 == 5) {
                System.out.print("JAVA! I <3 \n");
            } else if (i % 11 == 6) {
                System.out.print("AVA! I <3 J\n");
            } else if (i % 11 == 7) {
                System.out.print("VA! I <3 JA\n");
            } else if (i % 11 == 8) {
                System.out.print("A! I <3 JAV\n");
            } else if (i % 11 == 9) {
                System.out.print("! I <3 JAVA\n");
            } else if (i % 11 == 10) {
                System.out.print(" I <3 JAVA!\n");
            }

            Thread.sleep(200);
        }

    }
}
