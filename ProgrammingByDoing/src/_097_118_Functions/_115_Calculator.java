package _097_118_Functions;

import java.util.Scanner;

public class _115_Calculator {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        double a, b, c;
        String op;

        do {
            System.out.println("Calculator");
            System.out.println("Enter two numbers separated by an operand (+ - * / ^)");
            System.out.println("Enter 0 as your first operand to quit.");
            System.out.print("> ");
            a = keyboard.nextDouble();
            op = keyboard.next();
            b = keyboard.nextDouble();

            if (op.equals("+")) {
                c = a + b;
            } else if (op.equals("-")) {
                c = a - b;
            } else if (op.equals("*")) {
                c = a * b;
            } else if (op.equals("/")) {
                c = a / b;
            } else if (op.equals("^")) {
                c = Math.pow(a, b);
            } else {
                System.out.println("Undefined operator: '" + op + "'.");
                c = 0;
            }

            System.out.println(c);

        } while (a != 0);
    }
}
