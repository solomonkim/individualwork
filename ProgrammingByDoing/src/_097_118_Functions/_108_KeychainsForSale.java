package _097_118_Functions;

import java.util.Scanner;

public class _108_KeychainsForSale {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int choice;

        do {
            System.out.println("Keychain Shop");
            System.out.println("1. add keychains");
            System.out.println("2. remove keychains");
            System.out.println("3. view order");
            System.out.println("4. checkout");
            System.out.println("");
            System.out.print("Please enter your choice: ");
            choice = sc.nextInt();
            System.out.println("");

            if (choice == 1) {
                add_keychains();
            }
            if (choice == 2) {
                remove_keychains();
            }
            if (choice == 3) {
                view_order();
            }
            if (choice == 4) {
                checkout();
            }
        } while (choice != 4);
    }

    public static void add_keychains() {
        System.out.println("***add keychains***");
        System.out.println("");
    }

    public static void remove_keychains() {
        System.out.println("***remove keychains***");
        System.out.println("");
    }

    public static void view_order() {
        System.out.println("***view order***");
        System.out.println("");
    }

    public static void checkout() {
        System.out.println("***checkout***");
        System.out.println("");
    }
}
