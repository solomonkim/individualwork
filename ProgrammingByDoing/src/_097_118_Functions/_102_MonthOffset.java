package _097_118_Functions;

public class _102_MonthOffset {

    public static int offset(int month) {
        int result;
        if (month == 1) {
            result = 1;
        } else if (month == 2) {
            result = 4;
        } else if (month == 3) {
            result = 4;
        } else if (month == 4) {
            result = 0;
        } else if (month == 5) {
            result = 2;
        } else if (month == 6) {
            result = 5;
        } else if (month == 7) {
            result = 0;
        } else if (month == 8) {
            result = 3;
        } else if (month == 9) {
            result = 6;
        } else if (month == 10) {
            result = 1;
        } else if (month == 11) {
            result = 4;
        } else if (month == 12) {
            result = 6;
        } else {
            result = -1;
        }
        return result;
    }
    
    public static void main(String[] args) {
        for (int i = 1; i < 13; i++) {
            System.out.println("Offset for month " + i + ": " + offset(i));
        }
        System.out.println("Offset for month 43: " + offset(43));
    }
}