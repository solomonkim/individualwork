package _097_118_Functions;

public class _100_DistanceFormula {

    public static void main(String[] args) {
        // test the formula a bit
        double d1 = distance(-2, 1, 1, 5);
        System.out.println(" (-2,1) to (1,5) => " + d1);

        double d2 = distance(-2, -3, -4, 4);
        System.out.println(" (-2,-3) to (-4,4) => " + d2);

        System.out.println(" (2,-3) to (-1,-2) => " + distance(2, -3, -1, -2));

        System.out.println(" (4,5) to (4,5) => " + distance(4, 5, 4, 5));
    }

    public static double distance(double x1, double y1, double x2, double y2) {
        double dist;
        dist = Math.sqrt(((x2 - x1) * (x2 - x1)) + ((y2 - y1) * (y2 - y1)));
        return dist;
    }
}
