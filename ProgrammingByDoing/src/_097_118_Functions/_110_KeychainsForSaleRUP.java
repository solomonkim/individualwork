package _097_118_Functions;

import java.util.Scanner;

public class _110_KeychainsForSaleRUP {

    public static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) {
//        Scanner sc = new Scanner(System.in);
        int choice;
        int quantity = 0;
        double price = 10.00;
        double taxRate = 0.0825;
        double shipRate = 5.00;
        double addUnitSurcharge = 1.00;

        do {
            System.out.println("Keychain Shop");
            System.out.println("1. add keychains");
            System.out.println("2. remove keychains");
            System.out.println("3. view order");
            System.out.println("4. checkout");
            System.out.println("");
            System.out.print("Please enter your choice: ");
            choice = sc.nextInt();
            System.out.println("");

            if (choice == 1) {
                quantity = add_keychains(quantity);
            } else if (choice == 2) {
                quantity = remove_keychains(quantity);
            } else if (choice == 3) {
                view_order(quantity, price, taxRate, shipRate, addUnitSurcharge);
            } else if (choice == 4) {
                checkout(quantity, price, taxRate, shipRate, addUnitSurcharge);
            } else {
                System.out.println("invalid selection");
            }
        } while (choice != 4);
    }

    public static int add_keychains(int quantity) {
//        Scanner sc = new Scanner(System.in);

        System.out.println("***add keychains***");
        System.out.println("");
        System.out.print("How many keychains do you want to add to your order? ");
        int add = sc.nextInt();
        int updatedQuantity = quantity += add;
        System.out.println("You have " + updatedQuantity + " keychains in your shopping cart.");
        System.out.println("");
        return updatedQuantity;

    }

    public static int remove_keychains(int quantity) {
//        Scanner sc = new Scanner(System.in);
        int updatedQuantity;

        System.out.println("***remove keychains***");
        System.out.println("");
        System.out.print("How many keychains do you want to remove your order? ");
        int remove = sc.nextInt();
        if (remove <= quantity) {
            updatedQuantity = quantity -= remove;
        } else {
            updatedQuantity = 0;
            System.out.println("You tried to remove more keychains from your cart than you had.");
        }
        System.out.println("You have " + updatedQuantity + " keychains in your shopping cart.");
        System.out.println("");
        return updatedQuantity;
    }

    public static void view_order(int quantity, double price, double taxRate, double shipRate, double addUnitSurcharge) {
        double subTotal = quantity * price;
        double tax = subTotal * taxRate;
        double surcharge;
        if (quantity > 1) {
            surcharge = addUnitSurcharge * (quantity - 1);
        } else {
            surcharge = 0;
        }
        double shipping;
        if (quantity > 0) {
            shipping = shipRate + surcharge;
        } else {
            shipping = 0;
        }
        double grandTotal = subTotal + tax + shipping;

        System.out.println("***view order***");
        System.out.println("");
        System.out.println("quantity:\t" + quantity);
        System.out.println("price:\t\t" + price);
        System.out.println("subtotal: \t" + subTotal);
        System.out.println("tax (8.25%):\t" + tax);
        System.out.println("shipping:\t" + shipping);
        System.out.println("grand total:\t" + grandTotal);
    }

    public static void checkout(int quantity, double price, double taxRate, double shipRate, double addUnitSurcharge) {
//        Scanner sc = new Scanner(System.in);
        
        String name;
        double subTotal = quantity * price;
        double tax = subTotal * taxRate;
        double surcharge;
        if (quantity > 1) {
            surcharge = addUnitSurcharge * (quantity - 1);
        } else {
            surcharge = 0;
        }
        double shipping;
        if (quantity > 0) {
            shipping = shipRate + surcharge;
        } else {
            shipping = 0;
        }
        double grandTotal = subTotal + tax + shipping;

        System.out.println("***checkout***");
        System.out.println("");
        System.out.println("quantity:\t" + quantity);
        System.out.println("price:\t\t" + price);
        System.out.println("subtotal: \t" + subTotal);
        System.out.println("tax (8.25%):\t" + tax);
        System.out.println("shipping:\t" + shipping);
        System.out.println("grand total:\t" + grandTotal);
        System.out.print("Please enter your name: ");
        name = sc.nextLine();
        System.out.println("Your total is " + grandTotal + ". Thank you for your order, " + name + "!");
    }
}
