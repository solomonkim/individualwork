package _097_118_Functions;

import java.util.Scanner;

public class _104_AreaCalculator {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int choice;
        do {
            System.out.println("Shape Area Calculator");
            System.out.println("1) triangle");
            System.out.println("2) rectangle");
            System.out.println("3) square");
            System.out.println("4) circle");
            System.out.println("5) quit");
            System.out.println("Choose...");
            choice = sc.nextInt();

            if (choice == 1) {
                System.out.println("Enter the base and height of your triangle.");
                System.out.println("base: ");
                double base = sc.nextDouble();
                System.out.println("height: ");
                double height = sc.nextDouble();
                System.out.println("The area of your triangle is " + area_triangle(base, height) + ".");
                System.out.println("*****************************************************************");
            }

            if (choice == 2) {
                System.out.println("Enter the length and width of your rectangle.");
                System.out.println("length: ");
                double length = sc.nextDouble();
                System.out.println("width: ");
                double width = sc.nextDouble();
                System.out.println("The area of your rectangle is " + area_rectangle(length, width) + ".");
                System.out.println("*****************************************************************");
            }

            if (choice == 3) {
                System.out.println("Enter the length of one of the sides of your square.");
                System.out.println("side: ");
                double side = sc.nextDouble();
                System.out.println("The area of your square is " + area_square(side) + ".");
                System.out.println("*****************************************************************");
            }

            if (choice == 4) {
                System.out.println("Enter the radius of your circle.");
                System.out.println("radius: ");
                double radius = sc.nextDouble();
                System.out.println("The area of your circle is " + area_circle(radius) + ".");
                System.out.println("*****************************************************************");
            }

        } while (choice != 5);
    }

    public static double area_triangle(double base, double height) {
        double area = ((base / 2) * height);
        return area;
    }

    public static double area_rectangle(double length, double width) {
        double area = length * width;
        return area;
    }

    public static double area_square(double side) {
        double area = side * side;
        return area;
    }

    public static double area_circle(double radius) {
        double area = Math.PI * radius * radius;
        return area;
    }
}