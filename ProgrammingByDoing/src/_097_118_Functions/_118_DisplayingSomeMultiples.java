package _097_118_Functions;

import java.util.Scanner;

public class _118_DisplayingSomeMultiples {
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Enter a number.");
        int x = sc.nextInt();
        
        for (int i = 1; i < 13; i++) {
            System.out.println(x + " x " + i + " = " + (x * i));
        }
    }
}
