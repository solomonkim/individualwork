package _097_118_Functions;

public class _113_FindingPrimeNumbers {

    public static void main(String[] args) {
        for (int i = 2; i < 101; i++) {
            System.out.print(i);
            if (isPrime(i)) {
                System.out.print(" <");
            }
            System.out.println("");
        }
    }

    public static boolean isPrime(int n) {
        for (int i = 2; i < n; i++) {
            if (n % i == 0) {
                return false;
            }
        }
        return true;
    }
}
