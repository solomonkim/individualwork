package _097_118_Functions;

import java.util.Scanner;

public class _116_Refresher {
    
    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        int reps;
        
        System.out.println("What is your name?");
        String name = sc.nextLine();
        if (name.equals("Mitchell"))
            reps = 5;
        else
            reps = 10;
        
        System.out.println("");
        for (int i = 0; i < reps; i++) {
            System.out.println(name);
        }
    }
    
}
