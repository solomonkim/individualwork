/*
HeronsFormula and HeronsFormulaNoFunction produce the same output. HeronsFormula
is 26 lines long and would be even fewer if they were all formatted as the
second half of them are while HeronsFormulaNoFunction is 45. Fixing a function
is easier than fixing code without functions because you only have to do it in
one place. It is also easier to add new code because variables don't need to be
reassigned and formulas don't have to be entered for each set of inputs.
 */
package _097_118_Functions;

public class _099_HeronsFormula {

    public static void main(String[] args) {
        double a;

        a = triangleArea(3, 3, 3);
        System.out.println("A triangle with sides 3,3,3 has an area of " + a);

        a = triangleArea(3, 4, 5);
        System.out.println("A triangle with sides 3,4,5 has an area of " + a);

        a = triangleArea(7, 8, 9);
        System.out.println("A triangle with sides 7,8,9 has an area of " + a);

        a = triangleArea(9, 9, 9);
        System.out.println("A triangle with sides 9,9,9 has an area of " + a);

        System.out.println("A triangle with sides 5,12,13 has an area of " + triangleArea(5, 12, 13));
        System.out.println("A triangle with sides 10,9,11 has an area of " + triangleArea(10, 9, 11));
        System.out.println("A triangle with sides 8,15,17 has an area of " + triangleArea(8, 15, 17));
        System.out.println("A triangle with sides 9,9,9 has an area of " + triangleArea(9, 9, 9));
    }

    public static double triangleArea(int a, int b, int c) {
        // the code in this function computes the area of a triangle whose sides have lengths a, b, and c
        double s, A;

        s = (a + b + c) / 2.0;
        A = Math.sqrt(s * (s - a) * (s - b) * (s - c));

        return A;
        // ^ after computing the area, "return" it
    }

//    public static void main( String[] args )
//	{
//		int a, b, c;
//		double s, A;
//		
//		a = 3;
//		b = 3;
//		c = 3;
//		s = (a+b+c) / 2.0;
//		A = Math.sqrt( s*(s-a)*(s-b)*(s-c) );
//		System.out.println("A triangle with sides 3,3,3 has an area of " + A );
//
//		a = 3;
//		b = 4;
//		c = 5;
//		s = (a+b+c) / 2.0;
//		A = Math.sqrt( s*(s-a)*(s-b)*(s-c) );
//		System.out.println("A triangle with sides 3,4,5 has an area of " + A );
// 
//		a = 7;
//		b = 8;
//		c = 9;
//		s = (a+b+c) / 2.0;
//		A = Math.sqrt( s*(s-a)*(s-b)*(s-c) );
//		System.out.println("A triangle with sides 7,8,9 has an area of " + A );
//
//		a = 5;
//		b = 12;
//		c = 13;
//		s = (a+b+c) / 2.0;
//		A = Math.sqrt( s*(s-a)*(s-b)*(s-c) );
//		System.out.println("A triangle with sides 5,12,13 has an area of " + A );
//
//		a = 10;
//		b = 9;
//		c = 11;
//		s = (a+b+c) / 2.0;
//		A = Math.sqrt( s*(s-a)*(s-b)*(s-c) );
//		System.out.println("A triangle with sides 10,9,11 has an area of " + A );
//		
//		a = 8;
//		b = 15;
//		c = 17;
//		s = (a+b+c) / 2.0;
//		A = Math.sqrt( s*(s-a)*(s-b)*(s-c) );
//		System.out.println("A triangle with sides 8,15,17 has an area of " + A );
//	}
}
