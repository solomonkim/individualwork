package _163_175_Records;

import java.net.URL;
import java.util.Scanner;

public class _165_AddressToString {

    static class Address {

        String street;
        String city;
        String state;
        int zip;
/*
Renaming the toString to something else method prevents it from returning a 
String value. Whenever toString is called in the main method, it instead prints 
out memory locations.
*/
        public String toString() {
            return (this.street + ", " + this.city + ", " + this. state + ", "+ this.zip);
        }
    }

    public static void main(String[] args) throws Exception {
        URL addys = new URL("https://cs.leanderisd.org/txt/fake-addresses.txt");

        Address[] addybook = new Address[5];

        Scanner fin = new Scanner(addys.openStream());

        for (int i = 0; i < 5; i++) {
            addybook[i] = new Address();
            addybook[i].street = fin.nextLine();
            addybook[i].city = fin.nextLine();
            addybook[i].state = fin.next();
            addybook[i].zip = fin.nextInt();
            fin.skip("\n");
        }
        fin.close();

        for (int i = 0; i < 5; i++) {
            System.out.println((i + 1) + ". " + addybook[i]);
        }
    }
}
