package _163_175_Records;

import java.util.Scanner;

public class _167_LittleDatabase {

    public static class Student {

        String name;
        int grade, avg;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Student[] student = new Student[3];

        System.out.println("Enter the first student's name: ");
        student[0].name = sc.nextLine();
        System.out.println("Enter the first student's grade: ");
        student[0].grade = sc.nextInt();
        System.out.println("Enter the first student's average: ");
        student[0].avg = sc.nextInt();
        System.out.println("");
        sc.nextLine();

        System.out.println("Enter the second student's name: ");
        student[1].name = sc.nextLine();
        System.out.println("Enter the second student's grade: ");
        student[1].grade = sc.nextInt();
        System.out.println("Enter the second student's average: ");
        student[1].avg = sc.nextInt();
        System.out.println("");

        System.out.println("Enter the third student's name: ");
        student[2].name = sc.nextLine();
        System.out.println("Enter the third student's grade: ");
        student[2].grade = sc.nextInt();
        System.out.println("Enter the third student's average: ");
        student[2].avg = sc.nextInt();
        System.out.println("");
        sc.nextLine();

        System.out.println("The student names are: " + student[1].name + " " + student[2].name + " " + student[3].name);
        System.out.println("The student grades are: " + student[1].grade + " " + student[2].grade + " " + student[3].grade);
        System.out.println("The student averages are: " + student[1].avg + " " + student[2].avg + " " + student[3].avg);
    }
}
