package _163_175_Records;

import java.util.Scanner;

public class _166_BasicRecords {

    public static class Student {

        String name;
        int grade, avg;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Student student1 = new Student();
        Student student2 = new Student();
        Student student3 = new Student();

        System.out.println("Enter the first student's name: ");
        student1.name = sc.nextLine();
        System.out.println("Enter the first student's grade: ");
        student1.grade = sc.nextInt();
        System.out.println("Enter the first student's average: ");
        student1.avg = sc.nextInt();
        System.out.println("");
        sc.nextLine();

        System.out.println("Enter the second student's name: ");
        student2.name = sc.nextLine();
        System.out.println("Enter the second student's grade: ");
        student2.grade = sc.nextInt();
        System.out.println("Enter the second student's average: ");
        student2.avg = sc.nextInt();
        System.out.println("");
        sc.nextLine();

        System.out.println("Enter the third student's name: ");
        student3.name = sc.nextLine();
        System.out.println("Enter the third student's grade: ");
        student3.grade = sc.nextInt();
        System.out.println("Enter the third student's average: ");
        student3.avg = sc.nextInt();
        System.out.println("");
        sc.nextLine();

        System.out.println("The student names are: " + student1.name + " " + student2.name + " " + student3.name);
        System.out.println("The student grades are: " + student1.grade + " " + student2.grade + " " + student3.grade);
        System.out.println("The student averages are: " + student1.avg + " " + student2.avg + " " + student3.avg);

    }
}
