package _177_192_ArrayLists;

import java.util.ArrayList;
import java.util.Random;

public class _181_CopyingArrayLists {

    public static void main(String[] args) {

        Random r = new Random();
        ArrayList listOne = new ArrayList();
        ArrayList listTwo = new ArrayList();

        for (int i = 0; i < 10; i++) {
            listOne.add(1 + r.nextInt(100));
            listTwo.add(listOne.get(i));
        }

        listOne.set(9, -7);

        System.out.println("ArrayList 1: " + listOne);
        System.out.println("ArrayList 2: " + listTwo);

    }
}
