package _177_192_ArrayLists;

import java.util.ArrayList;
import java.util.Random;

public class _180_BasicArrayLists3 {
    public static void main(String[] args) {
        
        Random r = new Random();
        ArrayList myArrayList = new ArrayList();
        
        for (int i = 0; i < 1000; i++) {
            myArrayList.add(10 + r.nextInt(90));
        }
        
        for (int i = 0; i < myArrayList.size(); i++) {
            System.out.print(myArrayList.get(i) + " ");
            if((i + 1) % 20 == 0)
                System.out.println("");
        }
        
        
    }
    
    
}
