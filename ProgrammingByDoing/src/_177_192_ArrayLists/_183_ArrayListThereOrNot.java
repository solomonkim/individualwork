package _177_192_ArrayLists;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class _183_ArrayListThereOrNot {

    public static void main(String[] args) {
        
        Random r = new Random();
        ArrayList<Integer> myArrayList = new ArrayList();
        Scanner sc = new Scanner(System.in);

        for (int i = 0; i < 10; i++) {
            myArrayList.add(1 + r.nextInt(50));
        }

        System.out.println("ArrayList: " + myArrayList);
        System.out.println("Value to find:");
        int x = sc.nextInt();
        if (myArrayList.contains(x)) {
            System.out.println(x + " is in the ArrayList.");
        } else {
            System.out.println(x + " is not in the ArrayList.");
        }
        
    }
}
