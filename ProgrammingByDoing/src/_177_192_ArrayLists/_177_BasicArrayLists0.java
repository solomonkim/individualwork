package _177_192_ArrayLists;

import java.util.ArrayList;

public class _177_BasicArrayLists0 {

    public static void main(String[] args) {
        ArrayList<Integer> myArrayList = new ArrayList<>();

        myArrayList.add(-113);
        myArrayList.add(-113);
        myArrayList.add(-113);
        myArrayList.add(-113);
        myArrayList.add(-113);
        myArrayList.add(-113);
        myArrayList.add(-113);
        myArrayList.add(-113);
        myArrayList.add(-113);
        myArrayList.add(-113);

        System.out.println(myArrayList.get(0));
        System.out.println(myArrayList.get(1));
        System.out.println(myArrayList.get(2));
        System.out.println(myArrayList.get(3));
        System.out.println(myArrayList.get(4));
        System.out.println(myArrayList.get(5));
        System.out.println(myArrayList.get(6));
        System.out.println(myArrayList.get(7));
        System.out.println(myArrayList.get(8));
        System.out.println(myArrayList.get(9));

    }
}
