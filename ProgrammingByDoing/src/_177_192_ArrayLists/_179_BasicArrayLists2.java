package _177_192_ArrayLists;

import java.util.ArrayList;
import java.util.Random;

public class _179_BasicArrayLists2 {

    public static void main(String[] args) {

        ArrayList myArrayList = new ArrayList();
        Random r = new Random();

        for (int i = 0; i < 10; i++) {
            myArrayList.add(1 + r.nextInt(100));
        }

        System.out.println("Array List: " + myArrayList);

    }

}
