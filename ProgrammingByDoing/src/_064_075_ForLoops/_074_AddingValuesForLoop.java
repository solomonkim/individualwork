package _064_075_ForLoops;

import java.util.Scanner;

public class _074_AddingValuesForLoop {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int sum = 0;

        System.out.println("Enter a number:");
        int num = sc.nextInt();

        for (int i = 1; i <= num; i++) {
            System.out.print(i + " ");
            sum += i;
        }
        System.out.println("");
        System.out.println("The sum is " + sum + ".");
    }
}
