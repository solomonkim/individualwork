package _064_075_ForLoops;

import java.util.Scanner;

public class _067_CountingMachineRevisited {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Count from: ");
        int from = sc.nextInt();

        System.out.println("Count to: ");
        int to = sc.nextInt();

        System.out.println("Count by: ");
        int by = sc.nextInt();

        for (int i = from; i <= to; i = i + by) {
            System.out.print(i + " ");
        }
    }
}
