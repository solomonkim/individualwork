package _064_075_ForLoops;

import java.util.Random;

public class _075_BabyBlackjack {

    public static void main(String[] args) {

        Random r = new Random();

        int draw, pHand, dHand;

        System.out.println("Baby Blackjack");

        System.out.print("Player Hand: ");
        draw = (1 + r.nextInt(10));
        pHand = draw;
        System.out.print(draw + " + ");
        draw = (1 + r.nextInt(10));
        pHand = pHand + draw;
        System.out.println(draw + " = " + pHand);

        System.out.print("Dealer Hand: ");
        draw = (1 + r.nextInt(10));
        dHand = draw;
        System.out.print(draw + " + ");
        draw = (1 + r.nextInt(10));
        dHand = dHand + draw;
        System.out.println(draw + " = " + dHand);

        if (pHand == dHand) {
            System.out.println("It's a push!");
        } else if (pHand > dHand) {
            System.out.println("Player wins!");
        } else {
            System.out.println("Dealer wins!");
        }
    }
}
