package _064_075_ForLoops;

public class _071_FizzBuzz {

    public static void main(String[] args) {
        for (int i = 1; i <= 100; i++) {
            if (i % 3 == 0)
                System.out.print("On");
            if (i % 7 == 0)
                System.out.print("Base");
            if (i % 3 != 0 && i % 7 != 0)
                System.out.print(i);
            System.out.println();
        }
    }
}
