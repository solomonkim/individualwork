package _064_075_ForLoops;

/**
 *
 * @author apprentice
 */
public class _065_TenTimes {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(i + ". Mr. Mitchell is cool.");
        }
    }
}
