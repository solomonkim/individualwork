package _064_075_ForLoops;

public class _070_NoticingEvenNumbers {
    public static void main(String[] args) {
        for (int i = 0; i <= 20; i++) {
            System.out.print(i);
            if (i % 2 == 0) {
                System.out.println(" <");
            } else {
                System.out.println("");
            }
                
        }
    }
}
