package _064_075_ForLoops;

import java.util.Scanner;

public class _066_CountingMachine {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("How high do you want to count?");
        int target = sc.nextInt();
        for (int i = 0; i <= target; i++) {
            System.out.print(i + " ");
        }
    }
}
