package _032_040_GUIs;

import java.util.Scanner;

public class _035_TwoMoreQuestions {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String answer1, answer2;

        System.out.println("Let's play Two More Questions! Think of something and I'll guess what it is...");
        System.out.println("Question #1: Does it belong \"inside\", \"outside\", or \"both\"?");
        answer1 = sc.nextLine();
        System.out.println("Question #2: Is it alive? (y/n)");
        answer2 = sc.nextLine();
        if ("inside".equals(answer1) && "y".equals(answer2)) {
            System.out.println("I think you're thinking of a potted plant.");
        }
        if ("inside".equals(answer1) && "n".equals(answer2)) {
            System.out.println("I think you're thinking of a shower curtain.");
        }
        if ("outside".equals(answer1) && "y".equals(answer2)) {
            System.out.println("I think you're thinking of a bison.");
        }
        if ("outside".equals(answer1) && "n".equals(answer2)) {
            System.out.println("I think you're thinking of a billboard.");
        }
        if ("both".equals(answer1) && "y".equals(answer2)) {
            System.out.println("I think you're thinking of a dog.");
        }
        if ("both".equals(answer1) && "n".equals(answer2)) {
            System.out.println("I think you're thinking of a cellular telephone.");
        }
    }
}
