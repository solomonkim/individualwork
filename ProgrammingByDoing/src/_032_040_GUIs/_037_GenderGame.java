package _032_040_GUIs;

import java.util.Scanner;

public class _037_GenderGame {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String fName, lName, sex;
        int age;

        System.out.println("What is your first name? ");
        fName = sc.nextLine();
        System.out.println("...and your last name? ");
        lName = sc.nextLine();
        System.out.println("What is your sex? (m/f)");
        sex = sc.nextLine();
        System.out.println("How old are you?");
        age = sc.nextInt();
        if (age < 20) {
            System.out.println("Then I will call you " + fName + " " + lName + ".");
        }

        if (age >= 20 && "m".equals(sex)) {
            System.out.println("Then I Will call you Mr. " + fName + " " + lName + ".");
        }

        if (age >= 20 && "f".equals(sex)) {
            Scanner sc1 = new Scanner(System.in);
            System.out.println("Are you married? (y/n) ");
            String married = sc1.nextLine();
            if ("y".equals(married)) {
                System.out.println("Then I will call you Mrs. " + fName + " " + lName + ".");
            } else if ("n".equals(married)) {
                System.out.println("Then I will call you Miss " + fName + " " + lName + ".");
            }
        }
    }
}
