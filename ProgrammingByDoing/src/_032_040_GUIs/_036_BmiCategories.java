package _032_040_GUIs;

import java.util.Scanner;

public class _036_BmiCategories {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        double heightFeetImperial;
        double heightInchesImperial;
        double weightImperial;

        System.out.println("What is your height? feet: ");
        heightFeetImperial = keyboard.nextDouble();
        System.out.println("inches: ");
        heightInchesImperial = keyboard.nextDouble();
        System.out.println("What is your weight (in lbs)? ");
        weightImperial = keyboard.nextDouble();

        double heightMetric = ((heightFeetImperial * 12) + heightInchesImperial) * 0.0254;
        double weightMetric = weightImperial * 0.454;
        double bmi = weightMetric / (heightMetric * heightMetric);

        System.out.print("Your BMI is " + bmi + ", which classifies you as ");

        if (bmi < 18.5) {
            System.out.print("underweight.");
        }
        if (bmi >= 18.5 && bmi < 25) {
            System.out.print("normal weight.");
        }
        if (bmi >= 25 && bmi < 30) {
            System.out.print("overweight.");
        }
        if (bmi > 30) {
            System.out.print("obese.");
        }
    }
}
