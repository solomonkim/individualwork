package _032_040_GUIs;

import java.util.Scanner;

public class _039_AlphabeticalOrder {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("What is your name? ");
        String name = sc.nextLine();
        
        if (name.compareTo("Carswell") <= 0) {
            System.out.println("You don't have to wait long.");
        } else if (name.compareTo("Jones") <= 0) {
            System.out.println("That's not bad.");
        } else if (name.compareTo("Smith") <= 0) {
            System.out.println("Looks like a bit of a wait.");
        } else if (name.compareTo("Young") <= 0) {
            System.out.println("It's gonna be a while.");
        } else {
            System.out.println("Not going anywhere for a while?");
        }
    }
}
