package _032_040_GUIs;

import java.util.Scanner;

public class _034_AgeMessages {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.println("What is your name?");
        String name = sc.nextLine();
        System.out.println("How old are you, " + name + "? ");
        int age = sc.nextInt();
        
        if (age < 16){
            System.out.println("You can't drive a car, " + name + ".");
        }
        
        if (age >= 16 && age < 18) {
            System.out.println("You can drive a car, but you can't vote, " + name + ".");
        }
        
        if (age >= 18 && age < 25) {
            System.out.println("You can vote, but you can't drive a car, " + name + ".");
        }
        
        if (age >= 25) {
            System.out.println("You can do pretty much anything, " + name + ".");
        }
    }
}
