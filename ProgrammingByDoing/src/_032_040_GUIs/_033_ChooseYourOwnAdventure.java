package _032_040_GUIs;

import java.util.Scanner;

public class _033_ChooseYourOwnAdventure {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Choose Your Own Adventure!");
        System.out.println("You're at home. Wanna go \"here\" or \"there\"?");
        String answer = sc.nextLine();

        if ("here".equals(answer)) {
            System.out.println("You went here. Wanna do \"this\" or \"that?\"");
            answer = sc.nextLine();
            if ("this".equals(answer)) {
                System.out.println("You decide to do this. You gonna \"stay\" or \"go\"?");
                answer = sc.nextLine();
                if ("stay".equals(answer)) {
                    System.out.println("You stayed. Nothing happened. Way to go.");
                }else if ("go".equals(answer)) {
                    System.out.println("You decided to go. Why why why?");
                }
            } else if ("that".equals(answer)) {
                System.out.println("You decide to do that. Now go \"hither\" or \"thither\"?");
                answer = sc.nextLine();
                if ("hither".equals(answer)) {
                    System.out.println("You went hither. You are the winner!");
                }else if ("thither".equals(answer)) {
                    System.out.println("You went thither and died horribly. Should've gone hither.");
                }
            }
        }

        else if ("there".equals(answer)) {
            System.out.println("You went there. Now what? Go \"near\" or \"far?\"");
            answer = sc.nextLine();
            if ("near".equals(answer)) {
                System.out.println("You stayed near. Everyone think you're a punk bitch. Do you want to \"whine\" or \"moan\"?");
                answer = sc.nextLine();
                if ("whine".equals(answer)) {
                    System.out.println("You whined. Everyone STILL thinks you're a punk bitch.");
                }else if ("moan".equals(answer)) {
                    System.out.println("You moaned. Enough said.");
                }
            } else if ("far".equals(answer)) {
                System.out.println("You went far. Will you \"player-hate\" or \"player-participate\"?");
                answer = sc.nextLine();
                if ("player-hate".equals(answer)) {
                    System.out.println("You draw the ire of everyone. The end.");
                }else if ("player-participate".equals(answer)) {
                    System.out.println("Good choice. Game recognize game.");
                }
            }
        }
    }
}
