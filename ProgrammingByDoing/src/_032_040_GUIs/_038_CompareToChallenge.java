package _032_040_GUIs;

public class _038_CompareToChallenge {

    public static void main(String[] args) {
        int i;

        System.out.print("Comparing \"axe\" with \"dog\" produces ");
        i = "axe".compareTo("dog");
        System.out.println(i);

        System.out.print("Comparing \"applebee's\" with \"apple\" produces ");
        System.out.println("applebee's".compareTo("apple"));

        System.out.print("Comparing \"phone\" with \"book\" produces ");
        i = "phone".compareTo("book");
        System.out.println(i);

        System.out.print("Comparing \"tea\" with \"bag\" produces ");
        System.out.println("tea".compareTo("bag"));

        System.out.print("Comparing \"this\" with \"that\" produces ");
        i = "this".compareTo("that");
        System.out.println(i);

        System.out.print("Comparing \"bingo\" with \"bango\" produces ");
        System.out.println("bingo".compareTo("bango"));

        System.out.print("Comparing \"Bo\" with \"Dallas\" produces ");
        i = "Bo".compareTo("Dallas");
        System.out.println(i);

        System.out.print("Comparing \"hey\" with \"ho\" produces ");
        System.out.println("hey".compareTo("no"));

        System.out.print("Comparing \"Quad\" with \"Apartments\" produces ");
        i = "Quad".compareTo("Apartments");
        System.out.println(i);

        System.out.print("Comparing \"fandango\" with \"FANDANGO\" produces ");
        System.out.println("fandango".compareTo("FANDANGO"));

        System.out.print("Comparing \"compareTo challenge\" with \"compareTo challenge\" produces ");
        i = "compareTo challenge".compareTo("compareTo challenge");
        System.out.println(i);

        System.out.print("Comparing \"hobo camp\" with \"hobo camp\" produces ");
        System.out.println("hobo camp".compareTo("hobo camp"));
    }
}
