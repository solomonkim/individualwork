package _032_040_GUIs;

import java.util.Scanner;

public class _040_WorstNumberGuessingGameEver {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int secretNumber = 9;
        
        System.out.println("I'm thinking of a number between 1 and 10... What is it? ");
        if (sc.nextInt() == secretNumber) {
            System.out.println("You're right! It was " + secretNumber + ".");
        } else {
            System.out.println("ABJECT FAILURE! It was " + secretNumber + ".");
        }
    }
}
