package _032_040_GUIs;

import java.util.Scanner;

public class _032_TwoQuestions {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String answer1, answer2;

        System.out.println("Let's play Two Questions! Think of something and I'll guess what it is...");
        System.out.println("Question #1: Is it an animal, vegetable or mineral?");
        answer1 = sc.nextLine();
        System.out.println("Question #2: Is it bigger than a breadbox? (y/n)");
        answer2 = sc.nextLine();

        if ("animal".equals(answer1)) {
            if ("y".equals(answer2)) {
                System.out.println("I think you're thinking of a moose.");
            } else if ("n".equals(answer2)) {
                System.out.println("I think you're thinking of a squirrel.");
            }
        } else if ("vegetable".equals(answer1)) {
            if ("y".equals(answer2)) {
                System.out.println("I think you're thinking of a watermelon.");
            } else if ("n".equals(answer2)) {
                System.out.println("I think you're thinking of a carrot.");
            }
        } else if ("mineral".equals(answer1)) {
            if ("y".equals(answer2)) {
                System.out.println("I think you're thinking of a Camaro.");
            } else if ("n".equals(answer2)) {
                System.out.println("I think you're thinking of a paper clip.");
            }
        }
    }
}
