package _048_055_WhileLoops;

import java.util.Random;

public class _050_DiceDoubles {

    public static void main(String[] args) {
        Random r = new Random();
        int die1 = 1;
        int die2 = 2;
        int diceTotal;

        while (die1 != die2) {
            die1 = 1 + r.nextInt(6);
            die2 = 1 + r.nextInt(6);
            diceTotal = die1 + die2;
            System.out.println("die 1: " + die1);
            System.out.println("die 2: " + die2);
            System.out.println("total: " + diceTotal);
            System.out.println("");
        }
    }
}
