package _048_055_WhileLoops;

import java.util.Scanner;

public class _048_EnterYourPin {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int pin = 12345;

        System.out.println("WELCOME TO THE BANK OF MITCHELL.");
        System.out.print("ENTER YOUR PIN: ");
        int entry = keyboard.nextInt();
/*
while loops are similar to if loops in that they can be manipulated to run a
block of code over and over. they are different in that an if loop triggers IF 
the condition is true and will run the code once. a while loop will continue
executing the code until a condition is met
*/
        while (entry != pin) {
            System.out.println("\nINCORRECT PIN. TRY AGAIN.");
            System.out.print("ENTER YOUR PIN: ");
            entry = keyboard.nextInt(); //no need to declare variable "int"
            //because it is already declared globally in line 13. removing line
            //23 will cause the while loop to execute over and over if the pin
            //entry is incorrect because there is no opportunity to re-enter the
            //pin
        }

        System.out.println("\nPIN ACCEPTED. YOU NOW HAVE ACCESS TO YOUR ACCOUNT.");
    }
}
