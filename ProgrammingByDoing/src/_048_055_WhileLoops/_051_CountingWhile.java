package _048_055_WhileLoops;

import java.util.Scanner;

public class _051_CountingWhile {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        int n = 0;

        System.out.println("Type in a message, and I'll display it as many times as you want.");
        System.out.print("Message: ");
        String message = keyboard.nextLine();
        System.out.println("How many times do you want to print it?");
        int reps = keyboard.nextInt();
        while (n < reps) {
            System.out.println(((n + 1) * 10) + ". " + message);
            n++; //increments the counter
        }
    }
}
