package _048_055_WhileLoops;

import java.util.Random;
import java.util.Scanner;

public class _054_HiLoLimitedTries {

    public static void main(String[] args) {
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int number, guess;
        int guessCount = 0;

        System.out.println("I'm thinking of a number between 1 and 100. Guess!");
        number = 1 + r.nextInt(100);

        while (guessCount <= 7) {
            guess = sc.nextInt();
            guessCount++;
            if (guess == number) {
                System.out.println("That's correct! The number was " + number + ". It took you " + guessCount + " guesses.");
            } else if (guess < number) {
                System.out.println("Higher...");
            } else {
                System.out.println("Lower...");
            }
            if (number != guess && guessCount == 7) {
                System.out.println("The number was " + number + ". You didn't get it in seven guesses or less.");
            }
        }
    }
}
