package _048_055_WhileLoops;

import java.util.Scanner;

public class _055_AddingValuesLoop {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int x, total;

        System.out.println("I will add the values you give me...");
        System.out.println("number: ");
        x = sc.nextInt();
        total = x;
        while (x != 0) {
            System.out.println("total so far: " + total);
            System.out.println("number: ");
            x = sc.nextInt();
            total += x;
        }
        System.out.println("total: " + total);
    }
}
