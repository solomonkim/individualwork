package _048_055_WhileLoops;

import java.util.Random;
import java.util.Scanner;

public class _053_NumberGuessingWithCounter {

    public static void main(String[] args) {
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int num = 1 + r.nextInt(10);
        int numGuesses = 0;

        System.out.println("I'm thinking of a number between 1 and 10. Guess! ");
        int guess = sc.nextInt();
        numGuesses++;

        while (num != guess) {
            System.out.println("Nope! Try again...");
            guess = sc.nextInt();
            numGuesses++;
        }

        System.out.print("That's right! The number was " + num + ". It took you " + numGuesses + " guess");
        if (numGuesses > 1) {
            System.out.println("es.");
        } else {
            System.out.println(".");
        }
    }
}
