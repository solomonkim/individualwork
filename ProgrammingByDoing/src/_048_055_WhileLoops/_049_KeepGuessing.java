package _048_055_WhileLoops;

import java.util.Random;
import java.util.Scanner;

public class _049_KeepGuessing {

    public static void main(String[] args) {
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int num = 1 + r.nextInt(10);

        System.out.println("I'm thinking of a number between 1 and 10. Guess! ");
        int guess = sc.nextInt();

        while (num != guess) {
            System.out.println("Nope! Try again...");
            guess = sc.nextInt();
        }

        System.out.println("That's right! The number was " + num + ".");
    }
}
