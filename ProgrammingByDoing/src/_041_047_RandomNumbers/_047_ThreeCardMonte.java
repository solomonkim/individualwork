package _041_047_RandomNumbers;

import java.util.Random;
import java.util.Scanner;

public class _047_ThreeCardMonte {

    public static void main(String[] args) {

        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int blackMariah = 1 + r.nextInt(3);
        int guess;

        System.out.println("    3-Card Monte     ");
        System.out.println("   Find the Queen!   ");
        System.out.println("+---+   +---+   +---+");
        System.out.println("|###|   |###|   |###|");
        System.out.println("|###|   |###|   |###|");
        System.out.println("|###|   |###|   |###|");
        System.out.println("+---+   +---+   +---+");
        System.out.println("  1       2       3  ");
        System.out.println("  Choose: 1, 2 or 3  ");

        guess = sc.nextInt();

        if (blackMariah == 1) {
            System.out.println("+---+   +---+   +---+");
            System.out.println("|   |   |   |   |   |");
            System.out.println("| Q |   | 4 |   | 9 |");
            System.out.println("|   |   |   |   |   |");
            System.out.println("+---+   +---+   +---+");
            System.out.println("  1       2       3  ");
        }

        if (blackMariah == 2) {
            System.out.println("+---+   +---+   +---+");
            System.out.println("|   |   |   |   |   |");
            System.out.println("| 4 |   | Q |   | 9 |");
            System.out.println("|   |   |   |   |   |");
            System.out.println("+---+   +---+   +---+");
            System.out.println("  1       2       3  ");
        }

        if (blackMariah == 3) {
            System.out.println("+---+   +---+   +---+");
            System.out.println("|   |   |   |   |   |");
            System.out.println("| 9 |   | 4 |   | Q |");
            System.out.println("|   |   |   |   |   |");
            System.out.println("+---+   +---+   +---+");
            System.out.println("  1       2       3  ");
        }
        
        if (guess == blackMariah) {
            System.out.println("You win!");
        } else {
            System.out.println("You lose!");
        }
            
    }
}