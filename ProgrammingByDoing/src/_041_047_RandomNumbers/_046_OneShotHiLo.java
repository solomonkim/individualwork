package _041_047_RandomNumbers;

import java.util.Random;
import java.util.Scanner;

public class _046_OneShotHiLo {

    public static void main(String[] args) {
        Random r = new Random();
        Scanner sc = new Scanner(System.in);
        int number, guess;

        System.out.println("I'm thinking of a number between 1 and 100. Guess!");
        number = 1 + r.nextInt(100);
        guess = sc.nextInt();
        if (guess == number) {
            System.out.println("That's correct! The number was " + number + ".");
        } else if (guess < number) {
            System.out.println("Nope. Too low. The number was " + number + ".");
        } else {
            System.out.println("Nope. Too high. The number was " + number + ".");
        }

    }
}
