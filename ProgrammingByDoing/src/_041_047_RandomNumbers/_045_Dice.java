package _041_047_RandomNumbers;

import java.util.Random;

public class _045_Dice {
    public static void main(String[] args) {
        Random r = new Random();
        int die1, die2, diceTotal;
        
        die1 = 1+ r.nextInt(6);
        die2 = 1+ r.nextInt(6);
        diceTotal = die1 + die2;
        
        System.out.println("die 1: " + die1);
        System.out.println("die 2: " + die2);
        System.out.println("total: " + diceTotal);
    }
}
