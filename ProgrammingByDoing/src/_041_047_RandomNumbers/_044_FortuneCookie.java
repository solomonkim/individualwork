package _041_047_RandomNumbers;

import java.util.Random;

public class _044_FortuneCookie {

    public static void main(String[] args) {
        Random r = new Random();
        int roll;

        roll = r.nextInt(6);
        switch (roll) {
            case 0:
                System.out.println("Your fraudulence will be exposed VERY soon in a VERY humiliating way.");
                break;
            case 1:
                System.out.println("Your lack of self-esteem is the only thing that makes you attractive... like in that song by One Direction.");
                break;
            case 2:
                System.out.println("Others think you are a guarded person with walls, but deep down, you know the truth--you're just not a very interesting person.");
                break;
            case 3:
                System.out.println("You either don't know it or don't want to acknowledge it, but your insecurities are on full display to everyone. Man up and cut that shit out.");
                break;
            case 4:
                System.out.println("I can't think of any other fortunes that put on your emotional instability on front street, but it's okay because you're fully aware of your personality defects, even if it's only on a sub-conscious level.");
                break;
            default:
                System.out.println("Today is a good day.");
                break;
        }

        System.out.println("Lucky Numbers");
        for (int i = 1; i < 7; i++) {
            System.out.print((1 + r.nextInt(54)));
            if (i < 6) {
                System.out.print(" - ");
            } else {
                System.out.println("");
            }
        }
    }
}